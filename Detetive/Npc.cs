﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using edde.components.motsystem;
using edde.components;
using Detetive.action;
using MotivationalSystem;
using edde.resourcing;
using edd.motsystem;
using Microsoft.Xna.Framework;
using edde.physicalmanager;
using edde.maps;

namespace Detetive
{
    public static class Ballon
    {
        public const int CONVERSAR  = 15;
        public const int BRINCAR    = 16;
        public const int BATER      = 17;
        public const int DORMIR     = 08;
        public const int NEGAR      = 24;
        public const int INTIMIDAR  = 23;
        public const int PRESENTE   = 25;
        public const int FELIZ      = 20;
        public const int TRISTE     = 21;
        public const int MEDO       = 22;
        public const int INVALID    = TileSet.INVALID_TILESET;
    }

    public class Npc: GameObject
    {

        #region injection items
        [Injection]
        ExecuteActionTextComponent mExecuteActionTextComponent;
        [Injection]
        RectBaseComponent mRectBaseComponent;

        [Injection]
        MotSystemComponent  mMotSystem;

        [Injection]
        CharSetComponent    mCharSetPrincipal;

        [Injection]
        Mover               mMover;

        [Injection]
        TileAnimationComponent mCharSetBallon;

        [Injection]
        TileAnimationComponent mCharSetBallonEmotion;


        [Injection]
        RotationCharSet        mCharSetArrow;

        [Injection]
        Timer<Npc> mTimer;

        [Injection]
        AndarAction         mActionAndar;

        [Injection]
        ProcurarAction      mActionProcurar;

        [Injection]
        FugirAction         mActionFugir;

        [Injection]
        ConversarAction     mActionConversar;

        [Injection]
        ContarParadeiroAction mActionContarParadeiro;

       // [Injection]
       // ReputationComponent mReputationComponent;
        #endregion

        /// <summary>
        /// Sensores
        /// </summary>
        #region sensors
        ISensor mSensorAlvejado;

        ISensor mSensorOuvirTiro;

        ISensor mSensorOuvirParadeiro;

        ISensor mSensorVerDesconhecido;

        ISensor mSensorConversado;

        ISensor mSensorReceberPresente;

        ISensor mSensorReceberDinheiro;

        ISensor mSensorIntimidado;

        ISensor mSensorTocado;
        #endregion

        /// <summary>
        /// variáveis membro
        /// </summary>
        /// 

        private class ListGameObjectTime
        {
            private LinkedList<KeyValuePair<GameObject, TimeSpan>> mListPossibleMurder = new LinkedList<KeyValuePair<GameObject,TimeSpan>>(); //determina possíveis Assassinos

            public bool add(GameObject go, TimeSpan time)
            {
                LinkedListNode<KeyValuePair<GameObject, TimeSpan>> node = null;
                for(node = mListPossibleMurder.First; node != null; node = node.Next)
                {                
                    if(ReferenceEquals(node.Value.Key, go))
                    {
                        node.Value = new KeyValuePair<GameObject,TimeSpan>(go, time);
                        return true;
                    }
                }

                mListPossibleMurder.AddLast(new KeyValuePair<GameObject, TimeSpan>(go, time));
                return true;
            }

            public bool remove(GameObject go)
            {
                LinkedListNode<KeyValuePair<GameObject, TimeSpan>> node = null;
                for(node = mListPossibleMurder.First; node != null; node = node.Next)
                {
                    if(ReferenceEquals(node.Value.Key, go))
                    {
                        break;
                    }
                }

                if(node != null)
                {
                    mListPossibleMurder.Remove(node);
                    return true;
                }

                return false;
            }

            public bool hasItem() { return mListPossibleMurder.Count > 0; }

            public TimeSpan getTimeNextRemove()
            {
                return mListPossibleMurder.First.Value.Value;
            }

            public void removeNextRemove()
            {
                mListPossibleMurder.RemoveFirst();
            }

            public GameObject getRandomMurder()
            {
                if (mListPossibleMurder.Count <= 0) return null;

                int pos = MyMath.getRandom(mListPossibleMurder.Count);
                LinkedListNode<KeyValuePair<GameObject, TimeSpan>> node = null;
                for(node = mListPossibleMurder.First; node != null && pos >= 0; node = node.Next, pos--)
                {                    
                }

                if (node != null)
                    return node.Value.Key;

                return null;

            }

        }
        #region fields
        ActionComponent mActiveActionComponent = null;

        string mStrName = "noname";

        TimeSpan mTimeCurrent;

        //ListGameObjectTime mListGameObjectTime = new ListGameObjectTime();
        List<GameObject> mListBandido = new List<GameObject>();
        
        #endregion

        #region properties
        public ActionComponent activeAction
        {
            get { return mActiveActionComponent; }
        }

        /// <summary>
        /// Altera a classe do sistema motivacional usada. Isto faz resetar o sistema
        /// </summary>
        public string motivationalSystemClass
        {
            get { return mMotSystem.motSystemClass; }
            set { mMotSystem = new MotSystemComponent(value); }
        }
        #endregion

        private void desactiveAction(Timer<Npc>.TimeParam timeParam, Npc parent)
        {
            activeAction.desactiveAction();
            mActiveActionComponent = null;
        }

        public bool trySetActiveAction(ActionComponent component, TimeSpan delay/*, params Timer<Npc>.TimeHandle [] stopCallBack*/)
        {
            if (mActiveActionComponent == null)
            {
                mActiveActionComponent = component;
                //mTimer.addTimer(delay, TimeSpan.Zero, component, stopCallBack);
                mTimer.addTimer(delay, desactiveAction);
                return true;
            }
            else
                return false;
        }

        public Npc() : this("Npc") { }

        /// <summary>
        /// Construtor
        /// </summary>
        public Npc(string strMotClass)
        {
            mActionAndar        = new AndarAction();
            mMotSystem          = new MotSystemComponent(strMotClass);
           // mCharSetPrincipal   = new CharSetComponent("amanda");
            mMover              = new Mover();
            mCharSetBallon      = new TileAnimationComponent("ballon");
            mCharSetBallonEmotion = new TileAnimationComponent("ballon");
            mCharSetArrow       = new RotationCharSet("arrow", CharSet.PATTERN.PATTERN_64_4, 0f);
            mTimer              = new Timer<Npc>();
            mRectBaseComponent = new RectBaseComponent();
            mExecuteActionTextComponent = new ExecuteActionTextComponent();

           // mReputationComponent = new ReputationComponent();

            //
            mActionAndar        = new AndarAction();
            mActionProcurar     = new ProcurarAction();
            mActionFugir        = new FugirAction();
            mActionConversar    = new ConversarAction();
            mActionContarParadeiro = new ContarParadeiroAction();

            //
            mCharSetArrow.setAnimatedState(CharSetComponent.ANIMATED_STATE.FIX);
            mCharSetArrow.setRelativeFrame(0);
            mCharSetArrow.enable = false;

            {
                //muda o charset
                string [] strCharSets = new string[3];
                strCharSets[0] = "homens01";
                strCharSets[1] = "mulheres01";
                strCharSets[2] = "soldados";
                int nQualFile = MyMath.getRandom(3);
                mCharSetPrincipal = new CharSetComponent(strCharSets[nQualFile]);
                mCharSetPrincipal.setPointCharSet(MyMath.getRandom(4), MyMath.getRandom(2));
            }

         }

        /// <summary>
        /// Retorna um container contendo os processos
        /// </summary>
        public ICollection<IProcessor> ContentProcessor
        {
            get { return mMotSystem.ContentProcessor; }
        }

        public bool getRandomMurderPos(out Vector2 posMurder)
        {
            GameObject goPerto = null;//mListGameObjectTime.getRandomMurder();
            float distance = float.MaxValue;
            if(mListBandido.Count > 0)
            {
                IEnumerator<GameObject> it = mListBandido.GetEnumerator(); it.MoveNext();
                goPerto = it.Current;
                distance = (goPerto.position - this.position).Length();
                while(it.MoveNext())
                {
                    float actualDistance = (it.Current.position - goPerto.position).Length();
                    if (distance > actualDistance)
                        goPerto = it.Current;
                }
            }
            if(!ReferenceEquals(goPerto, null))
            {
                posMurder = goPerto.position;
                return true;
            }else
            {
            //{
            //    int width = 0;
            //    int height = 0;
            //    Map map = RPG.me.sceneManager.topScene as Map;
            //    if(ReferenceEquals(map, null))
            //    {
            //        width = RPG.me.screenWidth;
            //        height= RPG.me.screenHeight;
            //    }else
            //    {
            //        width = map.width;
            //        height= map.height;
            //    }
                 
            //    return MyMath.getRandomPos((float)width, (float)height);
                posMurder = Vector2.Zero;
                return false;
            }
        }

        #region send to sensors
        private void sendViu(ViuDesconhecido viu)
        {
            mSensorVerDesconhecido.sendPerception(viu);
        }

        private void sendTocado(Tocado tocado)
        {
            mSensorTocado.sendPerception(tocado);
        }

        private void sendOuvirTiro(OuvirTiro ouviuTiro)
        {
            mSensorOuvirTiro.sendPerception(ouviuTiro);
        }

        public void sendPresente(Presente presente)
        {
            mSensorReceberPresente.sendPerception(presente);

        }
        #endregion



        #region GameObject members

        public override sealed bool testVision(GameTime gameTime)
        {
            return mMotSystem.isUpdateTime(gameTime.TotalGameTime);
        }

        public override void onTouch(ICollection<edde.physicalmanager.ICollised> collObject)
        {
            foreach (edde.physicalmanager.ICollised collised in collObject)
            {
                GameObject go = collised as Npc;
                if (go == null)
                    go = collised as Detetive.DetetiveHero;

                if (go != null)
                {
                    Vector2 visionDir = (go.position - this.position);
                    Vector2 vecDist = visionDir;
                    if (visionDir != Vector2.Zero)
                        visionDir.Normalize();

                    float intensify = (float)(Collision.VISION_DISTANCE - (float)vecDist.Length()) / (float)Collision.VISION_DISTANCE;
                    if (intensify >= 0f)
                    {
                        Tocado tocado = new Tocado(go, intensify, visionDir);
                        sendTocado(tocado);
                    }
                }

                //faz percepção do tiro
                go = collised as SomTiro;
                if (go != null)
                {
                    Vector2 somDir = (go.position - this.position);
                    Vector2 vecDist = somDir;
                    if (somDir != Vector2.Zero)
                        somDir.Normalize();

                    float intensify = (float)(Collision.VISION_DISTANCE - (float)vecDist.Length()) / (float)Collision.VISION_DISTANCE;
                    if (intensify >= 0f)
                    {
                        OuvirTiro ouvirTiro = new OuvirTiro(go.position, intensify, somDir);
                        sendOuvirTiro(ouvirTiro);
                        if(!mListBandido.Contains(go))
                            mListBandido.Add(go);
                    }
                }

                //vê colisão tiro
                {
                    Shot arrow = collised as Shot;
                    if (arrow != null && arrow.source != this)
                    {
                        Lapide lapide = new Lapide(this);
                        RPG.me.sceneManager.insertSafeGameObject(lapide);
                        RPG.me.sceneManager.removeSafeGameObject(this);
                    }
                }
            }
        }

  
        public override sealed void verifyVisionContainer(ICollection<edde.physicalmanager.ICollised> collVision)
        {
            foreach (edde.physicalmanager.ICollised collised in collVision)
            {
                GameObject go = collised as Npc;

                {
                    Detetive.Bandido bandido = go as Detetive.Bandido;
                    if (!ReferenceEquals(bandido, null))
                    {
                        if(!mListBandido.Contains(bandido))
                            mListBandido.Add(bandido);
                    }
                }

                if (go == null)
                    go = collised as Detetive.DetetiveHero;

                if (!ReferenceEquals(go, null))
                {
                    Vector2 visionDir = new Vector2();
                    Vector2 vecDist = visionDir = (go.position - this.position);
                    if (visionDir != Vector2.Zero)
                        visionDir.Normalize();

                    float intensify = (float)(Collision.VISION_DISTANCE - (float)vecDist.Length()) / (float)Collision.VISION_DISTANCE;
                    if (intensify >= 0f)
                    {
                        ViuDesconhecido p = new ViuDesconhecido((GameObject)go, intensify, visionDir);
                        sendViu(p);
                    }
                }
            }
        }

        /// <summary>
        /// Inicia o NPC
        /// </summary>
        /// <param name="scene">O mapa ao qual pertence</param>
        /// <returns></returns>
        public override bool init(Scene scene)
        {
            if( base.init(scene))
            {
   
                mSensorAlvejado         = mMotSystem.getSensor("Alvejado");    
                mSensorOuvirParadeiro   = mMotSystem.getSensor("OuvirParadeiro");
                mSensorVerDesconhecido  = mMotSystem.getSensor("VerDesconhecido");
                mSensorConversado       = mMotSystem.getSensor("Conversado");
                mSensorReceberPresente  = mMotSystem.getSensor("ReceberPresente");
                mSensorReceberDinheiro  = mMotSystem.getSensor("ReceberDinheiro");
                mSensorIntimidado       = mMotSystem.getSensor("Intimidado");
                mSensorTocado           = mMotSystem.getSensor("Tocado");
                mSensorOuvirTiro        = mMotSystem.getSensor("OuvirTiro");
                return true;
            }else
            {
                return false;
            }

        }

        public override void  update(GameTime time)
        {
            mTimeCurrent = time.TotalGameTime;
 	        base.update(time);

            string emotion = mMotSystem.getVisibleEmotion();
            if (ReferenceEquals(emotion, null))
            {
                mCharSetBallonEmotion.actualTile = Ballon.INVALID;
                return;
            }
            else
            {
                if (emotion == "Triste")
                {
                    mCharSetBallonEmotion.actualTile = Ballon.TRISTE;
                }
                else if (emotion == "Medo")
                {
                    mCharSetBallonEmotion.actualTile = Ballon.MEDO;

                }
                else if(emotion == "Feliz")
                {
                    mCharSetBallonEmotion.actualTile = Ballon.FELIZ;
                }
            }

            //não precisa remover
            //if (mListGameObjectTime.hasItem())
            //{

            //    if ((mTimeCurrent - mListGameObjectTime.getTimeNextRemove()) > TimeSpan.FromSeconds(30))
            //       mListGameObjectTime.removeNextRemove();
            //}
        }

        #endregion

        //hashCode para os dicionários
        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0} | {1}", name.ToString(), ReferenceEquals(this.activeAction, null)?"":this.activeAction.behaviorName);
        }

        #region IReputable Members

        public string state
        {
            get { return name; }
        }

        public string name
        {
            get { return mStrName; }
            set { mStrName = value; }
        }

        #endregion
    }
}

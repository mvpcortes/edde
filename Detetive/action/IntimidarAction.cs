﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using MotivationalSystem;
using edde.components;
using edd.motsystem;

namespace Detetive.action
{
    class IntimidarAction: ActionComponent
    {
        #region fields
        [Injection]
        TileAnimationComponent mTileBallon;
        #endregion

        /// <summary>
        /// Sensor de toque de outro agente
        /// </summary>
        ISensor mSensorTocado = null;


        /// <summary>
        /// Construtor
        /// </summary>
        public IntimidarAction()
            : base("Intimidar")
        {
            setDelayAction = TimeSpan.FromSeconds(1);
        }

        public override bool init(GameObject parent)
        {
            if (base.init(parent))
            {
                mSensorTocado = motSystem.getSensor("Tocado");
                return mSensorTocado != null;
            }
            return false;
        }

        public override bool runAction(float intensity)
        {
            if (canActive)
            {
                List<IPerception> list = new List<IPerception>();
                mSensorTocado.getRelevancePerceptions(list);

                if (list.Count() > 0)
                {
                    Tocado tocado = (Tocado)list[0];

                    if (trySetActive())
                    {
                        parent.setDirection(tocado.data);
                        mTileBallon.actualTile = Ballon.INTIMIDAR;
                    }
                    return true;
                }
            }

            return false;
        }

        internal override void desactiveAction()
        {
            mTileBallon.actualTile = Ballon.INVALID;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using MotivationalSystem;
using edde.components;

namespace Detetive.action
{
    class NegarAction: ActionComponent
    {
        #region fields
        [Injection]
        TileAnimationComponent mTileBallon;
  
        ISensor mSensorTocado;
        #endregion

        public NegarAction()
            : base("Negar")
        {
            setDelayAction = TimeSpan.FromSeconds(1);
        }

        public override bool init(edde.GameObject parent)
        {
            if (base.init(parent))
            {
                mSensorTocado = motSystem.getSensor("Tocado");
                return mSensorTocado != null;
            }
            return false;

        }
        public override bool runAction(float intensity)
        {
            if (canActive)
            {
                List<IPerception> list = new List<IPerception>();
                mSensorTocado.getRelevancePerceptions(list);

                if (list.Count() > 0)
                {
                    trySetActive();
                    mTileBallon.actualTile = Ballon.NEGAR;
                    return true;
                }
            }

            return false;

        }

        internal override void desactiveAction()
        {
            mTileBallon.actualTile = Ballon.INVALID;
        }
    }
}

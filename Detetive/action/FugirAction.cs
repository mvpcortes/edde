﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MotivationalSystem;
using edd.motsystem;
using Microsoft.Xna.Framework;
using edde;
using util;
using edde.components;
using edde.resourcing;

namespace Detetive.action
{
    class FugirAction: ActionComponent
    {
        [Injection]
       // [InjResource]
        Mover mMover;

        private PriorityList<ViuDesconhecido> mListSeeIt = new PriorityList<ViuDesconhecido>();
        
        private ISensor mVerDesconhecido;

        private ISensor mOuvirTiro;
        public FugirAction():base("Fugir")
        {
            setDelayAction = (TimeSpan.FromSeconds(1));
        }

        public override bool init(edde.GameObject parent)
        {
            if (base.init(parent))
            {
                mVerDesconhecido = this.motSystem.getSensor("VerDesconhecido");
                mOuvirTiro = this.motSystem.getSensor("OuvirTiro");
                return true;
            }

            return false;
        }

       
        public sealed override bool runAction(float intensity)
        {
            if (!canActive)
                return false;

            List<IPerception> list = new List<IPerception>();

            Vector2 vecDestiny = Vector2.Zero;
            {
                mVerDesconhecido.getRelevancePerceptions(list);
                if (list.Count <= 0)
                {
                    foreach (ViuDesconhecido v in mListSeeIt)
                    {
                        list.Add(v);
                    }
                }

                foreach (ViuDesconhecido viu in list)
                {
                    vecDestiny += parent.position - viu.source.position;
                }

                //atualiza lista prévia de vistos (memória)
                foreach (ViuDesconhecido v in list)
                {
                    mListSeeIt.Add(v);
                }
                mListSeeIt.CutLastUntilRest(20);

            }

            list.Clear();
            {
                mOuvirTiro.getRelevancePerceptions(list);
                foreach(OuvirTiro ouvir in list)
                {
                    vecDestiny += parent.position - ouvir.source;
                }
            }

            if (vecDestiny == Vector2.Zero)
                vecDestiny = MyMath.VEC_UP;

            vecDestiny.Normalize();

            genericMove(mMover, vecDestiny, intensity);

            trySetActive();
            return true;
        }

        internal override void desactiveAction()
        {
            //faznada
        }
    }
}

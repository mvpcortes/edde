﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using MotivationalSystem;
using Microsoft.Xna.Framework;

namespace Detetive.action
{
    class AtirarAction : ActionComponent
    {

        /// <summary>
        /// Sensor de visão do GameObject
        /// </summary>
        ISensor mSensorViu = null;
        /// <summary>
        /// evita disparar vários tiros por iteração
        /// </summary>
        private bool mAtirou = false;

        /// <summary>
        /// Referência para o pai
        /// </summary>
        private GameObject mParent = null;

        /// <summary>
        /// Referência para o GameObject pai
        /// </summary>
        private GameObject parent
        {
            get { return mParent; }
            set { mParent = value; }

        }

        public AtirarAction()
            : base("Atirar")
        {
            setDelayAction = TimeSpan.FromSeconds(10);
        }


        /// <summary>
        /// Inicia o GameObject
        /// </summary>
        /// <param name="_parent"></param>
        /// <returns></returns>
        public override bool init(edde.GameObject _parent)
        {
            if (base.init(_parent))
            {
                setDelayAction = TimeSpan.FromSeconds(0.5);
                mParent = _parent;
                mSensorViu = motSystem.getSensor("VerDesconhecido");
                return mSensorViu != null && mParent != null;
            }
            return false;
        }

        public override bool runAction(float intensity)
        {
            if (!mAtirou && canActive)
            {
                List<IPerception> list = new List<IPerception>();
                mSensorViu.getRelevancePerceptions(list);

                if (list.Count > 0)
                {
                    int pos = MyMath.getRandom(list.Count);
                    GameObject otherGameObject = list[pos].objSource as GameObject;
                    if(otherGameObject != null)
                    {
                        Vector2 dir = otherGameObject.position - parent.position;
                        dir.Normalize();
                        parent.setDirection(dir);
                        Shot shot = new Shot(parent);
                        RPG.me.sceneManager.insertSafeGameObject(shot);

                        SomTiro somTiro = new SomTiro(shot);
                        RPG.me.sceneManager.insertSafeGameObject(somTiro);
                        mAtirou = true;
                        trySetActive();
                        return true;
                    }
                }
            }
            return false;
        }

        internal override void desactiveAction()
        {
            mAtirou = false;
        }
    }
}

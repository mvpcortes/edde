﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using edde.components;
using MotivationalSystem;
using edd.motsystem;
using Microsoft.Xna.Framework;

namespace Detetive.action
{
    class ConversarAction : ActionComponent
    {
        #region fields
        [Injection]
        TileAnimationComponent mTileBallon;

      //  [Injection]
     //   ReputationComponent mReputationComponent;

        #endregion

        /// <summary>
        /// Referencia ao sensor de tocado
        /// </summary>
        ISensor mSensorTocado = null;

        /// <summary>
        /// Diz qual o valor de mal-estar no início do comportamento
        /// </summary>
        float mBeingAtBegin = 0f;

        /// <summary>
        /// Construtor
        /// </summary>

        public ConversarAction()
            : base("Conversar")
        {
            setDelayAction = TimeSpan.FromSeconds(1);
        }

        public override bool init(edde.GameObject parent)
        {
            if (base.init(parent))
            {
                mSensorTocado = motSystem.getSensor("Tocado");
                return mSensorTocado != null;
            }
            return false;

        }

        public override bool runAction(float intensity)
        {
            if (canActive)
            {
                List<IPerception> list = new List<IPerception>();
                mSensorTocado.getRelevancePerceptions(list);
               // mOtherNpcSpeak = null;
                if (list.Count() > 0)
                {
 					Tocado tocado = (Tocado)list[0];

                    if (trySetActive())
                    {
                        parent.setDirection(tocado.data);
                        mTileBallon.actualTile = Ballon.CONVERSAR;
                    }
                    return true;
                    //Tocado tocado = null;

                    //IEnumerator<IPerception> it =  mReputationComponent.getTheHighReputation(list);
                    //if (!ReferenceEquals(it, null))
                    //    tocado = it.Current as Tocado;
                    //else
                    //    tocado = list[0] as Tocado;

                    //if (!ReferenceEquals(tocado, null) && trySetActive())
                    //{
                    //    parent.setDirection(tocado.data);
                    //    mTileBallon.actualTile = Ballon.CONVERSAR;
                    //    mBeingAtBegin = motSystem.wellBeing();
                    //    mOtherNpcSpeak = tocado.source as Npc;
                    //    if (ReferenceEquals(mOtherNpcSpeak, null))
                    //        mOtherNpcSpeak = tocado.source as DetetiveHero;
                    //    return true;
                    //}
                    //return false;
                }
            }
            return false;
        }

        internal override void desactiveAction()
        {
            mTileBallon.actualTile = Ballon.INVALID;
            //float actualBeing = motSystem.wellBeing();
            //float deltaBeing = actualBeing - mBeingAtBegin;
            //mReputationComponent.addReputation(mOtherNpcSpeak, motSystem.getVisibleEmotion(), deltaBeing);
        }
    }
}

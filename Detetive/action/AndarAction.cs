﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using edde.components;
using edde.resourcing;

namespace Detetive.action
{
    class AndarAction: ActionComponent
    {
        #region fields

        [Injection]
        Mover mMover;
        #endregion

        public AndarAction()
            : base("Andar")
        {
            setDelayAction = TimeSpan.FromSeconds(1);
        }

        public override bool init(edde.GameObject parent)
        {
            if (base.init(parent))
            {
                return true;
            }
            return false;

        }
        public override bool runAction(float intensity)
        {

            if (canActive)
            {
                genericMove(mMover, MyMath.getRandomDir(), intensity);
                trySetActive();
                return true;
            }

            return false;

        }

        internal override void desactiveAction()
        {
            //faznada
        }
    }
}

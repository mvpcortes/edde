﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using edde.components;
using MotivationalSystem;
using Microsoft.Xna.Framework;

namespace Detetive.action
{
    class ContarParadeiroAction: ActionComponent
    {
        #region fields
        [Injection]
        RotationCharSet mCharSetArrow;

        ISensor mSensorTocado;
        #endregion


        public ContarParadeiroAction()
            : base("ContarParadeiro")
        {
            setDelayAction = TimeSpan.FromSeconds(1);
        }

        public override bool init(edde.GameObject parent)
        {
            if (base.init(parent))
            {
                mSensorTocado = motSystem.getSensor("Tocado");
                return mSensorTocado != null;
            }
            return false;

        }
        public override bool runAction(float intensity)
        {
            if (canActive)
            {
                List<IPerception> list = new List<IPerception>();
                mSensorTocado.getRelevancePerceptions(list);

                Detetive.DetetiveHero detetive = null;
                foreach (IPerception p in list)
                {
                    detetive = p.objSource as Detetive.DetetiveHero;
                    if(!ReferenceEquals(detetive, null))
                        break;
                }
                if(!ReferenceEquals(detetive, null))
                {
                    trySetActive();
                    Vector2 murderPos;
                    if (parent.getRandomMurderPos(out murderPos))
                    {
                        Vector2 dir = murderPos - parent.position;

                        mCharSetArrow.setRotation(dir);
                        mCharSetArrow.enable = true;
                    }
                    else
                        return false;
                }else
                    return false;
            }

            return false;
        }

        internal override void desactiveAction()
        {
            mCharSetArrow.enable = false;
        }
    }
}

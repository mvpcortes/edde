﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde.components;
using edde;
using Microsoft.Xna.Framework;
using MotivationalSystem;
using edde.resourcing;

namespace Detetive.action
{
    public abstract class ActionComponent : SoftComponent, IAction
    {
        #region injection
        [Injection]
        private edde.components.motsystem.MotSystemComponent mMotSystem;
     
        #endregion

        #region fields
        private string mStrBehaviorName = "";

        /// <summary>
        /// Ponteiro para o NPC pai
        /// </summary>
        private Npc mParent = null;

        /// <summary>
        /// prioridade deste componente para acesso a recursos
        /// </summary>
        //private int mPriority = 0;

        /// <summary>
        /// Determina que a atual ação está ativa
        /// </summary>
        //private bool mActive = false;

        /// <summary>
        /// Tempo necessário para executar este Action
        /// </summary>
        TimeSpan mTimeDelay = TimeSpan.FromSeconds(1);
        #endregion

        #region properties

        public Npc parent
        {
            get { return mParent; }
        }

        public string behaviorName
        {
            get { return mStrBehaviorName; }
            set { mStrBehaviorName = value; }
        }

        protected edde.components.motsystem.MotSystemComponent motSystem
        {
            get { return mMotSystem; }
        }
 
        public TimeSpan delayAction
        {
            get{return mTimeDelay;}
        }

        protected TimeSpan setDelayAction
        {
            set { mTimeDelay = value; }
        }

        #endregion

        protected void genericMove(Mover mover, Vector2 vecDir, float intensity)
        {
            mover.setDirection(vecDir);
            mover.addMove(delayAction, 15, this);
            //mover.addMovePath(delayAction, vecDir
        }

        public ActionComponent(string behaviorName)
        {
            mStrBehaviorName = behaviorName;
        }
    
    
        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
        {
           //faznada
        }

        public virtual void update(Microsoft.Xna.Framework.GameTime time) { } //deve fazer algo


        public virtual bool init(GameObject parent)
        {
            if (!(parent is Npc)) return false;

            mParent = (Npc)parent;
            mMotSystem.setAction(mStrBehaviorName, this);
            return true;
        }

        public void ejected()
        {
           // throw new NotImplementedException();
        }

        public bool ejectMe()
        {
            return false;
        }

        public bool enable
        {
            get { return true; }
        }

        protected bool active
        {
            get { return parent.activeAction == this; }
        }

        protected bool canActive
        {
            get { return parent.activeAction == null; }
        }

        protected bool trySetActive()
        {
            return parent.trySetActiveAction(this, delayAction);
        }


         #endregion

        #region IAction Members

        public abstract bool runAction(float intensity);

        public virtual bool isRun(out float efficiency)
        {
            if (active)
            {
                efficiency = 1;
                return true;
            }
            else
            {
                efficiency = 0;
                return false;
            }
        }

        #endregion

        internal abstract void desactiveAction();
        //protected void freeResources()
        //{
        //    parentResourced.freeComponents(this, mListResources);
        //}

        //#region ISoftComponentOwner Members

        //public int resourcePriority
        //{
        //    get { return mPriority; }
        //}

        //protected void setResourcePriority(int priority)
        //{
        //    mPriority = priority;

        //}

        //public GameObjectResourced parentResourced
        //{
        //    get { return (GameObjectResourced)mParent; }
        //}

        //public virtual void onGiveBackResources()
        //{
        //    setActive(false);
        //}

        //public void addResources(ICollection<SoftComponent> collResource)
        //{
        //    foreach (SoftComponent sc in collResource)
        //    {
        //        mListResources.Add(sc);
        //    }
        //}

        //public void addResource(SoftComponent resource)
        //{
        //    mListResources.Add(resource);
        //}

        //public bool active { get { return mActive; } }
     
        //#endregion
    }
}

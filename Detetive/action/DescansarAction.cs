﻿using System;
using System.Collections.Generic;
using System.Linq;
using edde;
using edde.components;
using edde.resourcing;

namespace Detetive.action
{
    class DescansarAction: ActionComponent
    {
        [Injection]
        TileAnimationComponent mTileBallon;

        [Injection]
        Mover mMover;

        [Injection]
        Timer<Npc> mTimer;


        DescansarAction():base("Descansar")
        {
            setDelayAction = TimeSpan.FromMinutes(1);
        }

    
        public sealed override bool runAction(float intensity)
        {
            if (!canActive)
                return false;

            mTileBallon.actualTile = Ballon.DORMIR;        
            trySetActive();
            return true;
        }

        internal override void desactiveAction()
        {
            mTileBallon.actualTile = Ballon.INVALID;
        }
    }
}

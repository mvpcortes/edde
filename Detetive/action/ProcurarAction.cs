﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using edd.motsystem;
using MotivationalSystem;
using edde.components;
using edde.resourcing;
using edde.maps;
using Microsoft.Xna.Framework;

namespace Detetive.action
{
    class ProcurarAction:ActionComponent
    {
        [Injection]
        Mover mMover;

        //[Injection]
      //  ReputationComponent mReputationComponent;

        #region  fields
        private ISensor mVerDesconhecido;

        private Vector2 mPrevPosition = Vector2.Zero;
        #endregion

        public ProcurarAction()
            : base("Procurar")
        {
        }

        public sealed override bool init(GameObject parent)
        {
            if (base.init(parent))
            {
              //  mReputationComponent.setDecrescenteSortOrder();
                mVerDesconhecido = motSystem.getSensor("VerDesconhecido");
                return true;
            }

            return false;
        }

        public sealed override bool runAction(float intensity)
        {
            if (!canActive)
                return false;

            List<IPerception> list = new List<IPerception>();
            mVerDesconhecido.getRelevancePerceptions(list);
            if (list.Count > 0)
            {
                //if (list.Count > 2 && mReputationComponent.memoryCount() > 2)
                //{
                //    int i = 0;
                //}
                //list.Sort(mReputationComponent);
                list.Sort();
                if (canActive)
                {
                    trySetActive();
                    if (followThem(mMover, list, intensity))
                        return true;
                }
            }
            else
            {
                //faz ele olhar para o centro do mapa para achar alguém. Existe um fator de random também mas sempre próximo ao centro
                if(RPG.me.sceneManager.hasScene())
                {
                    Map scene = RPG.me.sceneManager.topScene as Map ;
                    if (scene != null)
                    {
                        Vector2 vec = new Vector2(scene.width, scene.height);
                        vec /= 2; //centro do mapa;

                        vec = vec - parent.position;//faz vetor apontar para o centro do mapa;
                        if (vec.Length() > 1 && MyMath.getRandom(3) == 1)
                        {
                            vec.Normalize();
                            vec = MyMath.rotate(vec, MyMath.getRandom(((float)Math.PI / 8f)) - (float)(Math.PI / 16f));
                        }
                        else
                            vec = MyMath.getRandomDir();

                        trySetActive();
                        genericMove(mMover, vec, intensity);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool followThem(Mover mover, ICollection<IPerception> coll, float intensity)
        {
            bool girou = false;
            Vector2 dir = Vector2.Zero;
            if ((parent.position - mPrevPosition).Length() > 1)
            {
                //se não tem nada, continua no mesmo caminho
                if (coll.Count > 0)
                {
                    //pega o primeiro - mais próximo
                    GameObject obj = null;
                    IEnumerator<IPerception> enumerator = coll.GetEnumerator();
                    while (obj == null && enumerator.MoveNext())
                    {
                        obj = enumerator.Current.objSource as GameObject;
                    }

                    if (!ReferenceEquals(obj, null))
                    {
                        Vector2 dif = obj.position - parent.position;
                        if (dif.Length() < ((obj.getRectBase().getSize() + parent.getRectBase().getSize()) / 2).Length())
                        {
                            mover.stopAll();
                            return false; //já encontrou alguém próximo o suficiente.
                        }
                        girou = true;
                        dir = dif;
                    }
                }
            }

            //força girar para procurar alguém
            if (!girou)
            {
                dir = MyMath.rotate(mover.direction, MyMath.getRandomFloat((float)Math.PI));
            }

            genericMove(mover, dir, intensity);

            mPrevPosition = parent.position;
            return true;
        }



        internal override void desactiveAction()
        {
            //faznada
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde.components;
using edde;
using edde.components.motsystem;
using Microsoft.Xna.Framework.Graphics;

namespace Detetive
{
    class ExecuteActionTextComponent: TextComponent
    {
        [Injection]
        MotSystemComponent mMotSystem;

        private MotSystemComponent motSystem
        {
            get { return mMotSystem; }
        }

        private Npc parentNpc
        {
            get{ return parent as Npc;}
        }
        public override void update(Microsoft.Xna.Framework.GameTime time)
        {
            base.update(time);

            if (parentNpc.activeAction != null)
            {
                this.text = parentNpc.activeAction.behaviorName;
            }
            else
            {
                this.text = "";
            }
        }

        public ExecuteActionTextComponent() : base("arial_12_b") { }

        public override bool init(edde.GameObject parent)
        {
            if (base.init(parent) && parent is Npc)
            {
                this.posCoor = new Microsoft.Xna.Framework.Vector2(6f, 6f);
                this.color = Color.Black;
                return true;
            }
            else
                return false;
        }
    }
}

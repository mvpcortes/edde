﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using edde.components;
using edde.maps;
using edde.physicalmanager;
using Microsoft.Xna.Framework;

namespace Detetive
{
    class Shot: GameObject
    {
        [Injection]
        private RotationCharSet mCharSet;

        private GameObject mSource = null;


        private float mVelocity = 2f;
        
        private Map mMyMap;

        /// <summary>
        /// Velocidade to tiro
        /// </summary>
        public float velocity
        {
            get { return mVelocity; }
            set { mVelocity = value; }
        }


        public GameObject source
        {
            get { return mSource; }
        }



        public Shot(GameObject source)
        {
            mCharSet = new RotationCharSet("shot", CharSet.PATTERN.PATTERN_FIVE_FRAME);
            mCharSet.setAnimatedState(CharSetComponent.ANIMATED_STATE.ANIMATED);
            mCharSet.setRotation(source.getDirection());
            mCharSet.setTimeFrame(CharSetComponent.SPEED.VERY_FAST);
            mCharSet.centerPoint = new Point(12, +15);
            setDirection(source.getDirection());

            position = source.position + (source.getRectBase().length() / 2) * getDirection();
            setTypeCollision(edde.physicalmanager.COLLISION_TYPE.CLOUD);

            setRectBase(new RectFloat(-3, -3, 3, 3));
            
            mSource = source;
        }

        public override bool init(Scene scene)
        {
            mMyMap = scene as Map;
            return base.init(scene);
        }
        public override void update(Microsoft.Xna.Framework.GameTime time)
        {
            base.update(time);
            incPosition(velocity * getDirection());

            RectFloat rect = new RectFloat(0, 0, mMyMap.height, mMyMap.width);
            rect.expand((float)Math.Max(mCharSet.getTileHeight(), mCharSet.getTileWidth()));
            if (!((ICollised)this).intersects(rect))
            {
                RPG.me.sceneManager.removeSafeGameObject(this);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using edde.components.motsystem;
using MotivationalSystem;
using System.IO;

namespace Detetive
{
    class LogWellBeing: SoftComponent
    {
        /// <summary>
        /// Referência ao motSystem
        /// </summary>
        [Injection()]
        MotSystemComponent mMotSystemComponent;

        ICollection<IProcessor> mCollProcessor;

        TimeSpan mLastTime = TimeSpan.Zero;
        TimeSpan mLastUpdateTime = TimeSpan.Zero;

        readonly TimeSpan DELAY_UPDATE = TimeSpan.FromSeconds(1);

        private Npc mParent = null;

          /// <summary>
        /// referencia ao GameObject dono
        /// </summary>
        public Npc parent { get { return mParent; } }


        private string mNameFile;

        public string nameFile { get { return mNameFile; } }

        public LogWellBeing(string nameFile)
        {
            mNameFile = nameFile;
        }
        ///<summary>
        ///Um sstream para gravação do log
        StreamWriter mStreamWrite;

        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
        {
            //
        }

        public void update(Microsoft.Xna.Framework.GameTime time)
        {
            mLastTime = time.TotalGameTime;

            if (mLastTime - mLastUpdateTime > DELAY_UPDATE)
            {
                mLastUpdateTime = mLastTime;
                writeData();
            }
            else
            {
            }
        }

        public bool init(GameObject _parent)
        {
            mParent = _parent as Npc;
            mCollProcessor = mMotSystemComponent.ContentProcessor;
 
            if(ReferenceEquals(parent, null)) return false;

            try
            {
                mStreamWrite = new StreamWriter("C:\\detetive\\" +mNameFile+ "_wellbeing.log");
                writeHead();
            }
            catch (Exception)
            {
                throw new edde.components.SoftComponentException("Não foi possível iniciar o stream de dados do sistema de log Wellbeing", this, eddeException.REGION_LOOP.LOAD);
            }
        
            return true;
        }

        public void ejected()
        {
            mStreamWrite.Close();
        }

        public bool ejectMe()
        {
            return false;
        }

        public bool enable
        {
            get { return true; }
        }

        #region write methods
        public void writeHead()
        {
            mStreamWrite.Write("Time\t");

            

            mStreamWrite.WriteLine("Behavior corrente\tWellBeing\t");
        }

        public void writeData()
        {
            mStreamWrite.Write(string.Format("{0:G}\t", mLastTime.TotalSeconds));

            mStreamWrite.Write(mMotSystemComponent.wellBeing()+"\t");

            if(ReferenceEquals(parent.activeAction, null))
                mStreamWrite.Write("nenhum\t");
            else
                mStreamWrite.Write(parent.activeAction.behaviorName + "\t");
        
            mStreamWrite.WriteLine("");

            mStreamWrite.Flush();
        }
        #endregion

        #endregion

    }
}

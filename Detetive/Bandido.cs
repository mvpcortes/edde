﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Detetive.action;
using edde;

namespace Detetive
{
    public class Bandido: Npc
    {
        #region injections
        [Injection]
        AtirarAction mAtirarAction = new AtirarAction();

        [Injection]
        IntimidarAction mIntimidarAction = new IntimidarAction();
        #endregion

        public Bandido():base("Murder")
        {
        }

        public override bool init(edde.Scene scene)
        {
            if (base.init(scene))
            {
                removeComponent(typeof(Detetive.action.ContarParadeiroAction));
                return true;
            }
            return false;
        }
    }
}

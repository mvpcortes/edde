﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde.components;
using Microsoft.Xna.Framework;
using edde;

namespace Detetive
{
    class ArrowNpc: CharSetComponent
    {
        public ArrowNpc()
            : base("shot", PATTERN.PATTERN_FIVE_FRAME_LOOP)
        {
            this.setPointCharSet(0, 1);
            this.setAnimatedState(ANIMATED_STATE.ANIMATED);
            this.centerPoint = new Point(this.centerPoint.X, +48);
            this.selfDirection = MyMath.VEC_UP;
            this.setTimeFrame(CharSetComponent.SPEED.VERY_FAST);
        }

        public override void draw(GameTime time, ref Rectangle rectWindow)
        {
            base.draw(time, ref rectWindow);
        }

        public override void update(GameTime time)
        {
            base.update(time);
        }
    }
}

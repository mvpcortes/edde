using System;

namespace Detetive
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (DetetiveGame game = new DetetiveGame())
            {
                game.Run();
            }
        }
    }
}


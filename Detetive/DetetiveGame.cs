using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using edde;
using edde.maps;
using MotivationalSystem;
using util;
using edde.components.motsystem;

namespace Detetive
{
    /// <summary>
    /// Classe base do jogo de Detetive
    /// </summary>
    public class DetetiveGame : edde.RPG
    {
        edde.FPS mFPS = null;

        Detetive.DetetiveHero mHero = null;

        edde.AgentMonitor mMonitor = null;

        List<Npc> mListNpc = new List<Npc>();
        int mnListNpcPos = 0;

        Texture2D mTextureTest = null;

        public DetetiveGame():base()
        {
            mWindow[0] = new Point(0, 0);
            mWindow[1] = new Point(0, -1);
            mWindow[2] = new Point(+1, -1);
            mWindow[3] = new Point(+1, 0);
            mWindow[4] = new Point(+1, +1);
            mWindow[5] = new Point(0, +1);
            mWindow[6] = new Point(-1, +1);
            mWindow[7] = new Point(-1, 0);
            mWindow[8] = new Point(-1, -1);
        }

        
        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            mFPS = new edde.FPS(this);
            mHero = null;
            mMonitor = new edde.AgentMonitor(this);
            mMonitor.Visible = false;

            Components.Add(mFPS);
            Components.Add(mMonitor);
            base.Initialize();
            mFPS.DrawOrder = 2;
            mMonitor.DrawOrder = 3;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();
            Map scene = (Map)sceneManager.enterScene("map1");
            mHero = scene.getGameObject<Detetive.DetetiveHero>();
            scene.followObject(mHero);
            scene.getGameObjects<Npc>(mListNpc);
            if (mListNpc.Count > 0)
            {
                //adiciona um log
                {
                    Npc npc1 = mListNpc[0];
                    LogMotSystemComponent log = new LogMotSystemComponent(npc1.name);
                    npc1.addAndInitComponent(log);
                    //LogWellBeing log = new LogWellBeing(npc1.name);
                   // npc1.addAndInitComponent(log);
                }
                mMonitor.setMonitored(mListNpc[mnListNpcPos].ContentProcessor, mListNpc[mnListNpcPos].name);
                ArrowNpc arrowNpc = new ArrowNpc();
                arrowNpc.init(mListNpc[mnListNpcPos]);
                mListNpc[mnListNpcPos].addComponent(arrowNpc);
            }
            else
                mMonitor.setMonitored(null, "");
        }

        readonly Point[] mWindow = new Point[9];

        public int neighbors(Matrix<Color> matrix, Point p)
        {
            int nNeir = 0;
            Color whiteColor = matrix[0, 0];
            for(int i = 1; i < mWindow.Length; i++)
            {
                Point pWindow = mWindow[i];

                int x = pWindow.X + p.X;
                int y = pWindow.Y + p.Y;
                if (matrix.valid(x, y))
                {
                    if(matrix[x, y] != whiteColor)
                        nNeir ++;
                }
            }

            return nNeir;
        }

        public int conectivity(Matrix<Color> matrix, Point p)
        {
            int conn = 0;
            Color whiteColor = matrix[0, 0];
            bool prevIsWhite = false;
            {
                int x = mWindow[8].X + p.X;
                int y = mWindow[8].Y + p.Y;

                if (matrix.valid(x, y))
                    prevIsWhite = matrix[x, y] == whiteColor;
            }

            for (int k = 1; k < mWindow.Length; k++)
            {
                Point pWindow = mWindow[k];

                int x = pWindow.X + p.X;
                int y = pWindow.Y + p.Y;
                if (matrix.valid(x, y))
                {
                    if (matrix[x, y] == whiteColor)
                    {
                        if (!prevIsWhite) conn++;
                        prevIsWhite = true;
                    }
                    else
                    {
                        if (prevIsWhite) conn++;
                        prevIsWhite = false;
                    }
                }
            }
            return conn;
        }

        private static Point sum(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        private static bool valid<T>(util.Matrix<T> m, Point p)
        {
            return m.valid(p.X, p.Y);
        }

        public void textureBorder(Texture2D texture)
        {
            int size = (texture.Width * texture.Height);
            Color[] data = new Color[size];
            texture.GetData<Color>(data, 0, size);
            Color removeColour = data[0];//first color

            Matrix<Color> matrix = new util.Matrix<Color>(texture.Width, texture.Height, data);
            LinkedList<Point> listDelete = new LinkedList<Point>();

            for(int i = 0; i < matrix.Width; i++)
                for (int j = 0; j < matrix.Height; j++)
                {
                    if(matrix[i,j] != removeColour)
                    {
                            int neir = neighbors(matrix, new Point(i,j));
                            if (neir >= 7)
                                listDelete.AddLast(new Point(i, j));
                    }
                }

            foreach (Point p in listDelete)
            {
                matrix[p.X, p.Y] = removeColour;
            }

            texture.SetData<Color>(matrix.toArray());

        }

        public void textureThinning(Texture2D texture)
        {
            int size = (texture.Width * texture.Height);
            Color[] data = new Color[size];
            texture.GetData<Color>(data, 0, size);
            Color removeColour = data[0];//first color

            Matrix<Color> matrix = new util.Matrix<Color>(texture.Width, texture.Height, data);
            LinkedList<Point> listDelete = new LinkedList<Point>();
            bool deleted = true;
            while (deleted)
            {
                deleted = false;
                for (int i = 0; i < matrix.Width; i++)
                    for (int j = 0; j < matrix.Height; j++)
                    {
                        if (matrix[i, j] == removeColour) continue;
                        Point p = new Point(i, j);
                        
                        if (conectivity(matrix, p) != 2) continue;
                        
                        int neirCount = neighbors(matrix, p);
                        if (neirCount < 2 && neirCount > 6) continue;

                        Point pA = sum(mWindow[1], p);
                        Point pB = sum(mWindow[3], p);
                        Point pC = sum(mWindow[5], p);
                        Point pD = sum(mWindow[3], p);
                        Point pE = sum(mWindow[5], p);
                        Point pF = sum(mWindow[7], p);
                        bool cicle1 = 
                            (valid(matrix, pA) && (matrix[pA.X, pA.Y] == removeColour)) || 
                            (valid(matrix, pB) && (matrix[pB.X, pB.Y] == removeColour)) || 
                            (valid(matrix, pC) && (matrix[pC.X, pC.Y] == removeColour));

                        bool cicle2 = 
                            (valid(matrix, pD) && (matrix[pD.X, pD.Y] == removeColour)) ||
                            (valid(matrix, pE) && (matrix[pE.X, pE.Y] == removeColour)) ||
                            (valid(matrix, pF) && (matrix[pF.X, pF.Y] == removeColour));

                        if(cicle1 && cicle2)
                         listDelete.AddLast(p);
                    }

                foreach (Point p in listDelete)
                {
                    matrix[p.X, p.Y] = removeColour;
                }
                deleted |= listDelete.Count > 0;
                listDelete.Clear();

                for (int i = 0; i < matrix.Width; i++)
                    for (int j = 0; j < matrix.Height; j++)
                    {
                        if (matrix[i, j] == removeColour) continue;
                        Point p = new Point(i, j);

                        if (conectivity(matrix, p) != 2) continue;

                        int neirCount = neighbors(matrix, p);
                        if (neirCount < 2 && neirCount > 6) continue;

                        Point pA = sum(mWindow[1], p);
                        Point pB = sum(mWindow[3], p);
                        Point pC = sum(mWindow[7], p);
                        Point pD = sum(mWindow[1], p);
                        Point pE = sum(mWindow[5], p);
                        Point pF = sum(mWindow[7], p);
                        bool cicle1 = 
                            (valid(matrix, pA) && (matrix[pA.X, pA.Y] == removeColour)) || 
                            (valid(matrix, pB) && (matrix[pB.X, pB.Y] == removeColour)) || 
                            (valid(matrix, pC) && (matrix[pC.X, pC.Y] == removeColour));

                        bool cicle2 = 
                            (valid(matrix, pD) && (matrix[pD.X, pD.Y] == removeColour)) ||
                            (valid(matrix, pE) && (matrix[pE.X, pE.Y] == removeColour)) ||
                            (valid(matrix, pF) && (matrix[pF.X, pF.Y] == removeColour));

                        if (cicle1 && cicle2)
                            listDelete.AddLast(p);
                    }

                foreach (Point p in listDelete)
                {
                    matrix[p.X, p.Y] = removeColour;
                }
                deleted |= listDelete.Count > 0;
                listDelete.Clear();
            }

            for (int i = 0; i < matrix.Width; i++)
                for (int j = 0; j < matrix.Height; j++)
                {
                    if (matrix[i, j] != removeColour)
                    {
                        matrix[i, j] = Color.Black;
                    }
                }

            texture.SetData<Color>(matrix.toArray());
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (inputControl.isKeyDown(InputControl.RPG_KEY.ACTION_A))
                mMonitor.Visible = !mMonitor.Visible;

            if (mListNpc.Count > 0 && mnListNpcPos >= 0 && mnListNpcPos < mListNpc.Count)
            {
                int prevNpcPos = mnListNpcPos;

                if (inputControl.isKeyDown(InputControl.RPG_KEY.ACTION_L))
                    mnListNpcPos--;
                else if (inputControl.isKeyDown(InputControl.RPG_KEY.ACTION_R))
                    mnListNpcPos++;

                if (mnListNpcPos > mListNpc.Count - 1)
                    mnListNpcPos = 0;

                if (mnListNpcPos < 0)
                    mnListNpcPos = mListNpc.Count - 1;

                if (mnListNpcPos != prevNpcPos)
                {
                    mListNpc[prevNpcPos].removeComponent(typeof(ArrowNpc));

                    ArrowNpc arrowNpc = new ArrowNpc();
                    arrowNpc.init(mListNpc[mnListNpcPos]);
                    mListNpc[mnListNpcPos].addComponent(arrowNpc);

                    if (mListNpc.Count > 0)
                        mMonitor.setMonitored(mListNpc[mnListNpcPos].ContentProcessor, mListNpc[mnListNpcPos].name);
                    else
                        mMonitor.setMonitored(null, "");
                }
            }

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            if (mTextureTest != null)
            {
                spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.None);
                spriteBatch.Draw(mTextureTest, new Vector2(0, 0), Color.White);
                spriteBatch.End();
            }
        }

        public override void exit(bool save)
        {
            Scene scene = sceneManager.topScene;
            ICollection<GameObject> list = scene.getGameObjects();
            foreach (GameObject go in list)
            {
                LogMotSystemComponent so = go.getComponent(typeof(LogMotSystemComponent)) as LogMotSystemComponent;
                if (!ReferenceEquals(so, null))
                {
                    so.ejected();
                    NormalizeLogFiles normalize = new NormalizeLogFiles(so.nameFile);
                }
            }
            base.exit(save);
        }
    }
}

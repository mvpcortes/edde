using System;
using System.Collections.Generic;
using System.Text;

namespace MotivationalSystem
{

    public enum RUN_TYPE
    {
        FLIP = 0,
        FLOP = 1
    }    
    
    public enum INPUT_MODE
    {
        UNDER_STATE = 0,
        OVER_STATE  = 1,
        EVERY_STATE   = 2
    }

    public delegate float AppraisalPerception(ICollection<IPerception> collP);

    public interface IProcessor: IComparable<IProcessor>
    {
        AppraisalPerception onAppraisalPerception
        {
            get;
        }
        string name
        {
            get;
        }

        float overThreshold
        {
            get;
        }

        float underThreshold
        {
            get;
        }

        float biasValue(RUN_TYPE type, float deltaTime);

        #region manager values

        float getValue(RUN_TYPE rt);

        /// <summary>
        /// Retorna a m�dia dos valores de flip e flor
        /// </summary>
        /// <returns>a m�dia dos valores de flip e flop.</returns>
        float getMediaValues();

        void run(RUN_TYPE rt, float deltaTime);

        #endregion

    }
}

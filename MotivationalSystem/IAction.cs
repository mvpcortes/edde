using System;
using System.Collections.Generic;
using System.Text;

namespace MotivationalSystem
{
    /// <summary>
    /// Delegate que especifica uma opera��o (efetuador) que este comportamento deve tentar executar
    /// </summary>
    /// <param name="strOp">String de configura��o do efetuador do comportamento</param>
    /// <param name="behavior">Refer�ncia ao comportamento que chamou o efetuador.</param>
    /// <returns>Valor indicando o quanto foi executada a opera��o. valores maiores que 0 indicam efetua��o da a��o. Valores iguais a 0 indicam n�o efetua��o da a��o</returns>
    public delegate bool DeleRunAction(float intensity);

    /// <summary>
    /// Especifica um delegate que verifica a execu��o de um Action
    /// </summary>
    /// <param name="efficiency">retornas a efici�ncia da execu��o atual</param>
    /// <returns>se est� executando atualmente ou n�o</returns>
    public delegate bool DeleIsRun(out float efficiency);

    public interface IAction
    {
        /// <summary>
        /// ativa a a��o e retorna se ela foi efetuada
        /// </summary>
        /// <param name="intensity">intensidade que se deseja efetuar a a��o</param>
        /// <returns>Se foi poss�vel iniciar a a��o ou n�o. OBS: N�O USE O RETORNO PARA NADA. SEMPRE RETORNE FALSE.</returns>
        bool  runAction(float intensity);

        /// <summary>
        /// Verifica se a a��o est� sendo efetuada ou n�o
        /// </summary>
        /// <param name="efficiency">Retorna o quando de efici�ncia da a��o ocorreu neste passo (o quanto foi poss�vel realiz�-la, como quantidade de comida)</param>
        /// <returns>se est� sendo executada ou n�o</returns>
        bool isRun(out float efficiency);
    }

    /// <summary>
    /// Wrap do IAction para permitir o uso de Delegates. Para a��es simples esta classe � ideal e facilita o trabalho.
    /// </summary>
    public class ActionDelegate : IAction
    {
        private readonly DeleRunAction mRunAction;

        private readonly DeleIsRun mIsRun;

        public ActionDelegate(DeleRunAction _runAction, DeleIsRun _isRun)
        {
            mRunAction = _runAction;
            mIsRun = _isRun;
        }

        #region IAction Members

        public bool runAction(float intensity)
        {
            return mRunAction(intensity);
        }

        public bool isRun(out float efficiency)
        {
            return mIsRun(out efficiency);
        }

        #endregion
    }

    /// <summary>
    /// Esta a��o nunca pode ser efetuada
    /// </summary>
    internal class NullAction : IAction
    {
        #region IAction Members

        public NullAction()
        {
        }
        public bool runAction(float intensity)
        {
            return false;
        }

        public bool isRun(out float efficiency)
        {
            efficiency = 0;
            return false;
        }
        #endregion
    }
}

using MotivationalSystem.Memory;
namespace MotivationalSystem
{
    public interface ISensor : IShortTimeMemory
    {
        void sendPerception(IPerception perception);
    }
}

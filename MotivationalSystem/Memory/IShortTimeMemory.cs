﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MotivationalSystem.Memory
{
    //Memória de curto prazo. Isto necessida de validação terórica.
    public interface IShortTimeMemory
    {
        /// <summary>
        /// Retorna todos as percepções relevantes durante o processo de motivação.
        /// </summary>
        /// <param name="coll"> container que receberá os perceptions</param>
        void getRelevancePerceptions(ICollection<IPerception> coll);


        /// <summary>
        /// Retorna todos os perceptions deste passo de interação.
        /// </summary>
        /// <param name="coll">container que receberá os perceptions</param>
        void getAllPerceptions(ICollection<IPerception> coll);
    }
}

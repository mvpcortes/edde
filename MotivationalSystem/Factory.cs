using System;
using System.Collections.Generic;
using System.Text;
using MotivationalSystem.DAO;
using System.Xml;

namespace MotivationalSystem
{
    public static class Factory
    {
        public static IMotAgentDAO getXMLDAO(XmlReader xml)
        {
            return new XMLMotAgentDAO(xml);
        }
    }
}

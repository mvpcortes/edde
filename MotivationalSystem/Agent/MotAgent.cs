using System;
using System.Text;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.ComponentModel;

namespace MotivationalSystem
{
    internal class EditableMotAgent : IMotAgent
    {
        /// <summary>
        /// Cor neutra para sistema de motAgent. (
        /// </summary>
        public static readonly float[] DEFAULT_AGENT_COLOR = new float[] { 1, 1, 1, 1 };
        
        #region fields
        private List<IProcessor>                mArrayProcessor;

        private Dictionary<string, Behavior>    mDicBehavior;
        private List<Emotion>                   mArrayEmotion;
        private List<Drive>                     mArrayDrive;

        private Dictionary<string, Sensor>      mDicSensor;

        private RUN_TYPE mRunState = RUN_TYPE.FLIP;

        private RUN_TYPE runState
        {
            get { return mRunState;}
            set { mRunState = value; }
        }

        private float[] mNeutralColor = new float[4];

        private Emotion mEmotionMax = null;

        private util.PriorityList<Behavior> m_listBehaviors;
        #endregion

        internal float[] neutralColor
        {
            get { return mNeutralColor; }
            set { mNeutralColor = value; }
        }

        private class BehaviorActionComparer : IComparer<Behavior>
        {
            #region IComparer<Behavior> Members

            public int Compare(Behavior x, Behavior y)
            {
                float temp = x.getMediaValues() - y.getMediaValues();
                if (temp > 0)
                    return -1;
                else if (temp < 0)
                    return +1;
                else
                    return 0;
            }

            #endregion
        }

        private class ComparerTemp : IComparer<int>
        {

            #region IComparer<int> Members

            public int Compare(int x, int y)
            {
                return y - x;
            }

            #endregion
        }

        public EditableMotAgent()
        {
            mArrayProcessor = new List<IProcessor>();
            mDicSensor  = new Dictionary<string, Sensor>();
            mDicBehavior    = new Dictionary<string, Behavior>();
            mArrayEmotion   = new List<Emotion>();
            mArrayDrive     = new List<Drive>();
            m_listBehaviors = new util.PriorityList<Behavior>();
            m_listBehaviors.setComparer(new BehaviorActionComparer());
            for(int i = 0; i < mNeutralColor.Length && i < DEFAULT_AGENT_COLOR.Length; i++)
                mNeutralColor[i] = DEFAULT_AGENT_COLOR[i];
        }

        public EditableMotAgent(ICollection<Processor> collProcessor):this()
        {            
            foreach(Processor p in collProcessor)
            {
               addProcessor(p);
            }
        }


        public void addProcessor(Processor p)
        {
                 if (p is Drive) AddDrive((Drive)p);
            else if (p is Emotion) AddEmotion((Emotion)p);
            else if (p is Behavior) AddBehavior((Behavior) p);
            else if (p is Sensor)   AddSensor((Sensor) p);
        }

        public void AddSensor(Sensor sensor)
        {
            mArrayProcessor.Add(sensor);
            mDicSensor.Add(sensor.name, sensor);
        }

        public void AddBehavior(Behavior behavior)
        {
            mArrayProcessor.Add(behavior);
            mDicBehavior.Add(behavior.name, behavior);
        }

        public void AddEmotion(Emotion emotion)
        {
            mArrayProcessor.Add(emotion);
            mArrayEmotion.Add(emotion);
        }

        public void AddDrive(Drive drive)
        {
            mArrayProcessor.Add(drive);
            mArrayDrive.Add(drive);
        }

        private void clearBufferSensor()
        {
            foreach (KeyValuePair<string, Sensor> key in mDicSensor)
            {
                key.Value.clearPerceptions();
            }
        }

        private void updateProcessors(float deltaTime)
        {
            {//Sensor
                foreach (KeyValuePair<string, Sensor> key in mDicSensor)
                {
                    key.Value.run(runState, deltaTime);
                }
            }
            {//behavior
                m_listBehaviors.Clear();

                //verifica a lista de ativa��o dos behaviors.
                foreach (KeyValuePair<string, Behavior> key in mDicBehavior)
                {
                    key.Value.run(runState, deltaTime);

                    if (key.Value.canAction(runState))
                        m_listBehaviors.Add(key.Value);
                }

                foreach (Behavior b in m_listBehaviors)
                {
                    if (b.runAction(runState))
                        break;
                    //appetitives
                    else
                    {
                        bool execute = false;
                        IEnumerator<Behavior> enumApptitive = b.getAppetitiveEnumerator();
                        while (enumApptitive.MoveNext())
                        {
                            if (execute = enumApptitive.Current.runAction(runState))
                                break;
                        }

                        if (execute)//executou um item
                            break;
                    }
                }
            }
            {//emotion
                int size = mArrayEmotion.Count;
                if (size > 0)
                {
                    mEmotionMax = mArrayEmotion[0];
                    for (int i = 0; i < size; i++)
                    {
                        mArrayEmotion[i].run(runState, deltaTime);
                        if (mEmotionMax.getValue(runState) < mArrayEmotion[i].getValue(runState))
                            mEmotionMax = mArrayEmotion[i];
                    }
                    if (mEmotionMax.getValue(runState) < mEmotionMax.overThreshold)
                        mEmotionMax = null;
                }
            }

            {//drive
                int size = mArrayDrive.Count;
                for (int i = 0; i < size; i++)
                {
                    mArrayDrive[i].run(runState, deltaTime);
                }
            }

        }


        #region IMotAgent Members

        void IMotAgent.setAction(string id, IAction action)
        {
            Behavior behavior;
            if (mDicBehavior.TryGetValue(id, out behavior))
            {
                behavior.setAction(action);
            }
        }

        ISensor IMotAgent.getSensor(string id)
        {
            Sensor sensor;
            if (mDicSensor.TryGetValue(id, out sensor))
            {
                return sensor;
            }

            throw new MotivationalSystemException("N�o foi poss�vel achar o sensor: " + id);
        }

        void IMotAgent.update(TimeSpan actualTime, TimeSpan deltaTime)
        {

            updateProcessors((float)deltaTime.TotalSeconds);
       
            m_listBehaviors.Clear();
            //verifica a lista de ativa��o dos behaviors.
            foreach (KeyValuePair<string, Behavior> key in mDicBehavior)
            {
                if(key.Value.canAction(runState))
                    m_listBehaviors.Add(key.Value);
            }

            //vare os comportamentos tentando acion�-los.
            foreach (Behavior b in m_listBehaviors)
            {
                //Se o comportamento n�o conseguiu ser acionado, tenta acionar os facilitadores
                if (!b.runAction(runState))
                {
                    IEnumerator<Behavior> enumerator = b.getAppetitiveEnumerator();
                    while (enumerator.MoveNext())
                    {
                        enumerator.Current.runAction(runState);
                    }
                }
                else
                    break;
            }


            //ser� a �ltima coisa a se fazer?
            runState = Processor.Not(runState);

            //limpa sensores
            clearBufferSensor();
        }

        ICollection<IProcessor> IMotAgent.ContentProcessor
        {
            get { return mArrayProcessor; }
        }


        private static float[] msVecRGBAValues = { 0f, 0f, 0f, 0f };

        float[] IMotAgent.getEmotionalColor()
        {
            for (int i = 0; i < 4; i++) msVecRGBAValues[i] = 1f;

            if (mArrayEmotion.Count == 0) return msVecRGBAValues;

            if (mEmotionMax!= null)
            {
                return mEmotionMax.color;
            }
            else
                return neutralColor;

        }

        string IMotAgent.getVisibleEmotion()
        {
            if (mEmotionMax != null)
                return mEmotionMax.name;
            else
                return null;

        }

        float[] IMotAgent.getBlendEmotionalColor()
        {
            float[] f =  {0f, 0f, 0f, 0f};
            return f;
            //if (mEmotionMax == null)
            //{
            //    return neutralColor;
            //}

            //for(int i = 0; i < 4; i++) msVecRGBAValues[i] = 0f;

            //float maxValue;
            //foreach (Emotion e in mArrayEmotion)
            //{
            //    float[] tempColor = e.color;
            //    for (int i = 0; i < 4; i++)
            //    {
            //        msVecRGBAValues[i] += tempColor[i];
            //    }
            //}


            ////esse vetor nos d� uma reta que passa pelo plano r + g + b + a = 1/2
            ////vamos considerar ele em uma reta que passa pela origem: P = t*V
            ////para achar o t do ponto desta reta que passa no plano, fazemos: t = 1/2*(r + g + b + a);

            //float sum = 0;
            //for (int i = 0; i < 4; i++)
            //{
            //   // msVecRGBAValues[i] /= mArrayEmotion.Count;
            //    sum += msVecRGBAValues[i];
            //}

            //float t = 1 / (2 * sum);

            //for (int i = 0; i < 4; i++)
            //{
            //    msVecRGBAValues[i] *= t;
            //}

            //return msVecRGBAValues;

            //if(mEmotionMax == null) return neutralColor;
            
            //for(int i = 0; i < 4; i++) msVecRGBAValues[i] = 0f;

            //float maxValue = mEmotionMax.getMediaValues();
            //float maxAcumCount = 0;
            //foreach(Emotion e in mArrayEmotion)
            //{
            //    float[] tempColor = e.color;
            //    float rateColor = e.getMediaValues()/maxValue;
            //    for(int i = 0; i < 4; i++)
            //    {
            //        msVecRGBAValues[i] += tempColor[i]*rateColor;
            //    }
            //}

            //for(int i = 0; i < 4; i++)
            //    msVecRGBAValues[i] /= (float)mArrayEmotion.Count;

            //return msVecRGBAValues;
        }

        void IMotAgent.randomizeProcessorsValues(float range)
        {
            Random random = new Random();
            foreach (Processor p in mArrayProcessor)
            {
                p.incValue((float)((random.NextDouble() * range * 2) - range));
            }
        }

        //float IMotAgent.(int x)
        //{
        //    float value = 0;
        //    foreach (IProcessor p in this.mArrayDrive)
        //    {
        //        if (p.getMediaValues() > p.overThreshold)
        //            value += Math.Abs(p.getMediaValues() - p.overThreshold);

        //        if (p.getMediaValues() < p.underThreshold)
        //            value += Math.Abs(p.getMediaValues() - p.underThreshold);
        //    }

        //    return value;
        //}

        /// <summary>
        /// Retorna um valor de wellBeing (bem-estar) do agente. Usado como m�trica.
        /// </summary>
        /// <returns></returns>
        float IMotAgent.wellBeing()
        {
            float a = badBeing();

            float ret = 1f / a;
            return ret;
        }
   
        public float badBeing()
        {
            float value = 0;
            foreach (IProcessor p in this.mArrayDrive)
            {
                if (p.getMediaValues() > p.overThreshold)
                    value += Math.Abs(p.getMediaValues() - p.overThreshold);

                if (p.getMediaValues() < p.underThreshold)
                    value += Math.Abs(p.getMediaValues() - p.underThreshold);
            }

            return value;
        }


        #endregion
    }
}

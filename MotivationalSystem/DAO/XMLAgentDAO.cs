using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Globalization;

namespace MotivationalSystem.DAO
{
    internal class XMLMotAgentDAO: IMotAgentDAO
    {
        /// <summary>
        /// Strutura temporária para representar a influência de um processo em outro
        /// </summary>
        private struct Param
        {
            public string strProcessor;
            public float weight;
            public INPUT_MODE mode;

            public Param(String _strProcessor, float _weight, INPUT_MODE _mode)
            {
                strProcessor = _strProcessor;
                weight = _weight;
                mode = _mode;
            }


            public void modeFromString(string value)
            {
                if (value == null)
                {
                    mode = INPUT_MODE.OVER_STATE;
                    return;
                }

                value.ToLower();
                if (value == XMLDefines.OVER_STATE_TAG)
                    mode = INPUT_MODE.OVER_STATE;
                else if (value == XMLDefines.UNDER_STATE_TAG)
                    mode = INPUT_MODE.UNDER_STATE;
                else if (value == XMLDefines.EVERY_STATE_TAG)
                    mode = INPUT_MODE.EVERY_STATE;
                else
                    mode = INPUT_MODE.OVER_STATE;
            }
        }

        private class AgentClass
        {

            private List<Processor> mListProcessors = new  List<Processor>();
            
            private Dictionary<string, LinkedList<Param>> mDicInfluences = new Dictionary<string,LinkedList<Param>>();

            private Dictionary<string, LinkedList<Param>> mDicConsummateInfluences = new Dictionary<string,LinkedList<Param>>();

            private Dictionary<string, LinkedList<string>> mDicAppetitiveBehavior = new Dictionary<string, LinkedList<string>>();

            private string mName = "";
            
            private AgentClass mParent = null;

            private string mNameParent = null;

            private float[] mNeutralColor = null;

            private Processor mProcessorEnergy = null;

            #region properties
            public string name
            {
                get { return mName; }
            }

            public AgentClass parent
            {
                get { return mParent; }
                set
                {
                    mParent = value;
                    mNameParent = mParent.name;
                }

            }

            public string nameParent
            {
                get
                {
                    if (parent != null)
                        return parent.name;
                    else
                        return mNameParent;
                }
            }

            public float [] neutralColor
            {
                get
                {
                    if(mNeutralColor != null)
                        return mNeutralColor;
                    else if(parent != null)
                        return parent.neutralColor;
                    else
                        return EditableMotAgent.DEFAULT_AGENT_COLOR;
                }

                set
                { mNeutralColor = value;}
            }

            public Processor processorEnergy
            {
                get
                {
                    if (mProcessorEnergy != null)
                        return mProcessorEnergy;
                    else if (parent != null)
                        return parent.processorEnergy;
                    else
                        return null;
                }

                set
                { 
                    mProcessorEnergy = value;
                }
            }
            #endregion

            public AgentClass(string name, string nameParent)
            {
                mName = name;
                mNameParent = nameParent;
                mParent = null;
            }

            public AgentClass(string name):this(name, "")
            {
            }

            public AgentClass(string name, AgentClass parent)
            {
                mName = name;
                mNameParent = parent.name;
                mParent = parent;
            }

            public void addProcessor(Processor p)
            {
               mListProcessors.Add(p);
            }

            public T getProcessor<T>(string name) where T: Processor
            {
                foreach(Processor proc in mListProcessors)
                {
                    if(proc is T && proc.name == name)
                        return proc as T;
                }
                return null;
            }

            public void addInfluence(string source, Param pDest)
            {
                LinkedList<Param> list;
                if (mDicInfluences.TryGetValue(source, out list))
                {
                    LinkedListNode<Param> node = null;
                    for (node = list.First; node != null; node = node.Next)
                    {
                        if (pDest.strProcessor == node.Value.strProcessor)
                            node.Value = pDest;
                    }
                    
                    if(node == null)
                        list.AddLast(pDest);
                }
                else
                {
                    mDicInfluences.Add(source, new LinkedList<Param>());
                    addInfluence(source, pDest);
                } 
            }

            public void addConsummateInfluence(string source, Param pDest)
            {
                LinkedList<Param> list;
                if (mDicConsummateInfluences.TryGetValue(source, out list))
                {
                    LinkedListNode<Param> node = null;
                    for (node = list.First; node != null; node = node.Next)
                    {
                        if (pDest.strProcessor == node.Value.strProcessor)
                            node.Value = pDest;
                    }

                    if (node == null)
                        list.AddLast(pDest);
                }
                else
                {
                    mDicConsummateInfluences.Add(source, new LinkedList<Param>());
                    this.addConsummateInfluence(source, pDest);
                }
            }

            public void addAppetitiveBehavior(string source, string destiny)
            {
                LinkedList<string> list;
                if (mDicAppetitiveBehavior.TryGetValue(source, out list))
                {
                    LinkedListNode<string> node = null;
                    for (node = list.First; node != null; node = node.Next)
                    {
                        if (destiny == node.Value)
                            break;
                    }

                    if (node == null)
                        list.AddLast(destiny);
                }
                else
                {
                    mDicAppetitiveBehavior.Add(source, new LinkedList<string>());
                    addAppetitiveBehavior(source, destiny);
                }

            }

            Dictionary<string, Processor> ms_dicProcessor = new Dictionary<string, Processor>();

            private void relinkItems(Dictionary<string, Processor> dicRelink)
            {
                if (this.parent != null)
                    parent.relinkItems(dicRelink);

                foreach (Processor p in mListProcessors)
                {
                    if (!dicRelink.ContainsKey(p.name))
                        dicRelink.Add(p.name, p.clone());
                }

                //relinks influences
                foreach (KeyValuePair<string, LinkedList<Param>> key in mDicInfluences)
                {
                    Processor processor;
                    if (dicRelink.TryGetValue(key.Key, out processor))
                    {
                        foreach (Param param in key.Value)
                        {
                            Processor otherProcessor;
                            if (dicRelink.TryGetValue(param.strProcessor, out otherProcessor))
                            {
                                otherProcessor.addInput(processor, param.weight, param.mode);
                            }
                        }
                    }
                }

                //relink consummate
                foreach (KeyValuePair<string, LinkedList<Param>> key in mDicConsummateInfluences)
                {
                    Processor processor;
                    if (dicRelink.TryGetValue(key.Key, out processor))
                    {
                        Behavior behavior = processor as Behavior;
                        if (!ReferenceEquals(behavior, null))
                        {
                            foreach (Param p in key.Value)
                            {
                                Processor otherProcessor;
                                if (dicRelink.TryGetValue(p.strProcessor, out otherProcessor))
                                {
                                    behavior.addConsummateProcessor(otherProcessor, p.weight);
                                }
                            }
                        }
                    }
                }

                //relink appetitive
                foreach (KeyValuePair<string, LinkedList<string>> key in mDicAppetitiveBehavior)
                {
                    Processor processor;
                    if (dicRelink.TryGetValue(key.Key, out processor))
                    {
                        Behavior behavior = processor as Behavior;
                        if (behavior != null)
                        {
                            foreach (string str in key.Value)
                            {
                                Processor other;
                                if (dicRelink.TryGetValue(str, out other))
                                {
                                    if(other is Behavior)
                                        behavior.addAppetitiveBehavior((Behavior)other);
                                }
                            }
                        }
                    }
                }
            }


            public EditableMotAgent createNewAgent()
            {
                lock (ms_dicProcessor)
                {
                    ms_dicProcessor.Clear();
                    relinkItems(ms_dicProcessor);
                    return new EditableMotAgent(ms_dicProcessor.Values);
                }
            }


            public override string ToString()
            {
                return string.Format("Nome: {0}; Parent {1};", this.mName, this.mNameParent);
            }

        }

        #region fields

        /// <summary>
        /// Container contendo os agentClass achados.
        /// </summary>
        private Dictionary<string, AgentClass> mDicAgentClass = new Dictionary<string,AgentClass>();

        /// <summary>
        /// Referência ao leitor de XML usado
        /// </summary>
        private XmlReader mXmlReader;

        private XmlReader reader
        {
            get { return mXmlReader; }
        }

        //private float[] mNeutralColor = { 1f, 1f, 1f, 1f };
        //
        #endregion


        public XMLMotAgentDAO(string filename)
            : this(new XmlTextReader(filename))
        {
        }

        public XMLMotAgentDAO(XmlReader xmlReader)
        {

            mXmlReader = xmlReader;

            while (reader.Read())
            {
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == XMLDefines.MOT_SYSTEM_TAG))
                {
                    while(reader.Read())
                    {
                        if((reader.NodeType == XmlNodeType.Element) && (reader.Name == XMLDefines.CLASS_TAG))
                        {
                            processClass();
                        }else if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == XMLDefines.MOT_SYSTEM_TAG))
                            break;
                    }
                }
            }

            //atualiza os parents das classes
            foreach (KeyValuePair<string, AgentClass> kv in mDicAgentClass)
            {
                if (kv.Value.parent == null && (kv.Value.nameParent != null || kv.Value.nameParent != ""))
                {
                    AgentClass parentClass = null;
                    if (mDicAgentClass.TryGetValue(kv.Value.nameParent, out parentClass))
                    {
                        kv.Value.parent = parentClass;
                    }
                }
            }
                 
        }

        private void processClass()
        {
            string name = reader.GetAttribute(XMLDefines.NAME_TAG);
            string nameParent = reader.GetAttribute(XMLDefines.EXTEND_TAG);

            AgentClass agent = null;
            if (nameParent == null)
                agent = new AgentClass(name);
            else
            {
                AgentClass agentClassParent = null;
                if (mDicAgentClass.TryGetValue(nameParent, out agentClassParent))
                    agent = new AgentClass(name, agentClassParent);
                else
                    agent = new AgentClass(name, nameParent);
            }

            while (reader.Read())
            {
                if (reader.Name == XMLDefines.SENSOR_TAG)
                {
                    processSensor(agent);
                }
                else if (reader.Name == XMLDefines.DRIVE_TAG)
                {
                    processDrive(agent);
                }
                else if (reader.Name == XMLDefines.EMOTION_TAG)
                {
                    processEmotion(agent);
                }
                else if (reader.Name == XMLDefines.BEHAVIOR_TAG)
                {
                    processBehavior(agent);
                }
                else if (reader.Name == XMLDefines.NEUTRAL_AVATAR)
                {
                    agent.neutralColor = processColor();
                }
                else if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == XMLDefines.CLASS_TAG))
                {
                    break;
                }
            }

            mDicAgentClass.Add(name, agent);
        }
   
        private void processParam(AgentClass agent, Processor processor)
        {
            Param param = new Param();
            string strTemp = reader.GetAttribute(XMLDefines.WEIGHT_TAG);
            param.weight = util.StrTools.toFloat(strTemp);
            param.modeFromString(reader.GetAttribute(XMLDefines.TYPE_TAG));
            param.strProcessor = reader.GetAttribute(XMLDefines.NAME_TAG);

            agent.addInfluence(processor.name, param);
        }

        private void processAppetitive(AgentClass agent, Behavior behavior)
        {
            string strOtherBehavior = reader.GetAttribute(XMLDefines.NAME_TAG);
            agent.addAppetitiveBehavior(behavior.name, strOtherBehavior);
        }

        private void processConsummate(AgentClass agent, Behavior behavior)
        {
            Param param = new Param();
            string strTemp = reader.GetAttribute(XMLDefines.WEIGHT_TAG);
            param.weight = util.StrTools.toFloat(strTemp);
            param.mode = INPUT_MODE.OVER_STATE;
            param.strProcessor = reader.GetAttribute(XMLDefines.NAME_TAG);

            agent.addConsummateInfluence(behavior.name, param);
        }

        private void processThreshold(Processor p)
        {
            string str = reader.GetAttribute(XMLDefines.THESHOLD_OVER_TAG);
            if (!ReferenceEquals(str, null))
                p.overThreshold = util.StrTools.toFloat(str);

            str = reader.GetAttribute(XMLDefines.THESHOLD_UNDER_TAG);
            if (!ReferenceEquals(str, null))
                p.underThreshold = util.StrTools.toFloat(str);
        }

        string processName()
        {
            string temp = reader.GetAttribute(XMLDefines.NAME_TAG);
            if (ReferenceEquals(temp, null))
                return "sem nome";
            else
                return temp;

        }

        T processNameParent<T>(AgentClass agent) where T : Processor, new()
        {
            T processor = null;
            string name = processName();

            if (agent.parent != null)
            {
                processor = agent.parent.getProcessor<T>(name);
                if (processor != null)
                    processor = (T)processor.clone();
            }

            if (processor == null)
            {
                processor = new T();
                processor.name = name;
            }

            return processor;
        }
            

        //private void processName(Processor p)
        //{
        //    string temp = reader.GetAttribute(XMLDefines.NAME_TAG);
        //    if (ReferenceEquals(temp, null))
        //        p.name = "sem nome";
        //    else
        //        p.name = temp;
        //}

        private void processSensor(AgentClass agent)
        {
            Sensor sensor = processNameParent<Sensor>(agent);
            agent.addProcessor(sensor);

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    if (reader.Name == XMLDefines.INFLUENCE_TAG)
                        processParam(agent, sensor);
                    else if (reader.Name == XMLDefines.THESHOLD_TAG)
                        processThreshold(sensor);
                }
                else if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == XMLDefines.SENSOR_TAG))
                    return;
            }
        }

        private void processDrive(AgentClass agent)
        {
            Drive drive = processNameParent<Drive>(agent);
            agent.addProcessor(drive);

            string str = reader.GetAttribute(XMLDefines.INC_RATE_TAG);
            if(str != null)
                drive.incRate = util.StrTools.toFloat(str);

            while (reader.Read())
            {
                if(reader.NodeType == XmlNodeType.Element)
                {
                    if (reader.Name == XMLDefines.INFLUENCE_TAG)
                        processParam(agent, drive);
                    else if(reader.Name == XMLDefines.THESHOLD_TAG)
                        processThreshold(drive);
                    else if(reader.Name == XMLDefines.ENERGY_TAG)
                        agent.processorEnergy = drive;
                }
                else if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == XMLDefines.DRIVE_TAG))
                    return;
            }
        }

        private void processEmotion(AgentClass agent)
        {
            Emotion emotion = processNameParent<Emotion>(agent);
            agent.addProcessor(emotion);

            string temp = reader.GetAttribute(XMLDefines.DEC_RATE_TAG);
            if(temp != null)
                emotion.decRate = util.StrTools.toFloat(temp);

            temp = reader.GetAttribute(XMLDefines.GOOD_TAG);
            if (temp != null)
                emotion.goodValue = util.StrTools.toFloat(temp);

            temp = reader.GetAttribute(XMLDefines.POLE_TAG);
            if (temp != null)
                emotion.poleValue = util.StrTools.toFloat(temp);

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    if (reader.Name == XMLDefines.INFLUENCE_TAG)
                        processParam(agent, emotion);
                    else if (reader.Name == XMLDefines.THESHOLD_TAG)
                        processThreshold( emotion);
                    else if (reader.Name == XMLDefines.AVATAR)
                        emotion.color = processColor();
                }
                else if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == XMLDefines.EMOTION_TAG))
                    return;
            }
        }

        private void processBehavior(AgentClass agent)
        {
            Behavior behavior = processNameParent<Behavior>(agent);
            agent.addProcessor(behavior);

            string temp = reader.GetAttribute(XMLDefines.SPEND_TAG);
            if (temp != null)
                behavior.spendValue = util.StrTools.toFloat(temp);

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    if (reader.Name == XMLDefines.INFLUENCE_TAG)
                        processParam(agent, behavior);
                    else if (reader.Name == XMLDefines.THESHOLD_TAG)
                        processThreshold(behavior);
                    else if (reader.Name == XMLDefines.CONSUMMATE_INF_TAG)
                        processConsummate(agent, behavior);
                    else if (reader.Name == XMLDefines.APPETITIVE_TAG)
                        processAppetitive(agent, behavior);
                }
                else if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == XMLDefines.BEHAVIOR_TAG))
                    return;
            }

            return;
        }

        
        private float[] processColor()
        {
            float[] colorRet = new float[4]; for (int i = 0; i < 4; i++) colorRet[i] = EditableMotAgent.DEFAULT_AGENT_COLOR[i];
            try
            {
                string tempStr = reader.GetAttribute(XMLDefines.AVATAR_COLOR);
                tempStr = tempStr.Substring(1);
                for(int i = 0; i < 4; i++)
                {
                    if(i*2+1 > tempStr.Length) break;
                    colorRet[i] = int.Parse( tempStr.Substring(i * 2, 2), NumberStyles.HexNumber);
                    colorRet[i] /= 255f;
                }
            }catch(ArgumentException)
            {
            }
            return colorRet;
        }

        #region IMotAgentDAO Members
        
        /// <param name="strClass">Nome da classe de sistema motivacional</param>
        /// <returns>O MotAgent</returns>
        public IMotAgent getMotAgent(string strClass)
        {
            AgentClass tempAgentClass;
            lock (this.mDicAgentClass)
            {
                if (mDicAgentClass.TryGetValue(strClass, out tempAgentClass))
                {
                    return tempAgentClass.createNewAgent();

                }
            }
            return null;
        }

        #endregion
    }
}

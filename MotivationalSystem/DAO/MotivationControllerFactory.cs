//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Xml;

//namespace MotivationalSystem
//{
//    public class MotivationControllerFactory
//    {
//        private struct Param
//        {
//            public Processor processor;
//            public float weight;
//            public INPUT_MODE mode;
//            public void modeFromString(string value)
//            {
//                if (value == null) return;

//                value.ToLower();
//                if (value == XMLDefines.OVER_STATE_TAG)
//                    mode = INPUT_MODE.OVER_STATE;
//                else if (value == XMLDefines.UNDER_STATE_TAG)
//                    mode = INPUT_MODE.UNDER_STATE;
//                else if (value == XMLDefines.EVERYTIME_TAG)
//                    mode = INPUT_MODE.EVERYTIME;
//            }
//        }


//        private Dictionary<string, LinkedList<Param>> mInfluences;

//        private LinkedList<Processor> mListProcessors;

//        private Drive mEnergyDrive;

//        private XmlReader mXmlReader;

//        private static MotivationControllerFactory msSingletons = null;

//        private MotivationControllerFactory(string filename)
//            : this(new XmlTextReader(filename))
//        {
//        }

//        private MotivationControllerFactory(XmlReader xmlReader)
//        {
//            mListProcessors = new LinkedList<Processor>();

//            mInfluences = new Dictionary<string, LinkedList<Param>>();
//            XmlReader x;
//            mXmlReader = xmlReader;

//            while (mXmlReader.Read())
//            {
//                if ((mXmlReader.NodeType == XmlNodeType.Element) && (mXmlReader.Name == XMLDefines.MOT_CONTROLLER_TAG))
//                    processElement(ref mXmlReader);
//            }

//        }

//        //Faz a relinkagem de um grafo depois dele ser copiado
//        private void relinkItems(ICollection<Processor> coll)
//        {
//            //faz os inputs
//            foreach (Processor processor in mListProcessors)
//            {
//                LinkedList<Param> list;
//                if (mInfluences.TryGetValue(processor.name, out list))
//                {
//                    foreach (Param param in list)
//                    {
//                        processor.addInput(param.processor, param.weight, param.mode);
//                    }
//                }
//            }

//        }


//        public static void resetMe(string filename)
//        {
//            if (!ReferenceEquals(msSingletons, null))
//                msSingletons.mXmlReader.Close();
//            msSingletons = new MotivationControllerFactory(filename);
//        }

//        public static void resetMe(XmlReader xmlReader)
//        {
//            if (!ReferenceEquals(msSingletons, null))
//                msSingletons.mXmlReader.Close();
//            msSingletons = new MotivationControllerFactory(xmlReader);
//        }

//        public static MotivationControllerFactory me
//        {
//            get { return msSingletons; }
//        }

//        private void insertParam(string name, Param param)
//        {
//            LinkedList<Param> list;
//            if (mInfluences.TryGetValue(name, out list))
//            {
//                list.AddLast(param);
//            }
//            else
//            {
//                mInfluences.Add(name, new LinkedList<Param>());
//                insertParam(name, param);
//            }
//        }

//        private Param processParam(Processor proc, ref XmlReader XmlReader)
//        {
//            Param p = new Param();
//            p.processor = proc;
//            p.weight = float.Parse(XmlReader.GetAttribute(XMLDefines.WEIGHT_TAG));
//            p.modeFromString(XmlReader.GetAttribute(XMLDefines.TYPE_TAG));
//            string name = XmlReader.GetAttribute(XMLDefines.NAME_TAG);

//            insertParam(name, p);
//            return p;
//        }

//        private Drive processDrive(ref XmlReader XmlReader)
//        {
//            Drive drive = new Drive(XmlReader.GetAttribute(XMLDefines.NAME_TAG));
//            drive.incRate = float.Parse(XmlReader.GetAttribute(XMLDefines.INC_RATE_TAG));

//            while (XmlReader.Read())
//            {
//                if ((XmlReader.NodeType == XmlNodeType.Element) && (XmlReader.Name == XMLDefines.INFLUENCE_TAG))
//                {
//                    processParam(drive, ref XmlReader);
//                }
//                else if ((XmlReader.NodeType == XmlNodeType.Element) && (XmlReader.Name == XMLDefines.ENERGY_TAG))
//                {
//                    mEnergyDrive = drive;
//                }
//                else if ((XmlReader.NodeType == XmlNodeType.EndElement) && (XmlReader.Name == XMLDefines.DRIVE_TAG))
//                    return drive;
//            }

//            OnDriveLoad(drive);
//            return drive;
//        }

//        private Emotion processEmotion(ref XmlReader XmlReader)
//        {
//            Emotion emotion = new Emotion(XmlReader.GetAttribute(XMLDefines.NAME_TAG));
//            emotion.decRate = float.Parse(XmlReader.GetAttribute(XMLDefines.DEC_RATE_TAG));
//            emotion.goodValue = float.Parse(XmlReader.GetAttribute(XMLDefines.GOOD_TAG));
//            emotion.poleValue = float.Parse(XmlReader.GetAttribute(XMLDefines.POLE_TAG));

//            while (XmlReader.Read())
//            {
//                if ((XmlReader.NodeType == XmlNodeType.Element) && (XmlReader.Name == XMLDefines.INFLUENCE_TAG))
//                {
//                    processParam(emotion, ref XmlReader);
//                }
//                else if ((XmlReader.NodeType == XmlNodeType.EndElement) && (XmlReader.Name == XMLDefines.EMOTION_TAG))
//                    return emotion;
//            }

//            OnEmotionLoad(emotion);
//            return emotion;
//        }

//        private Sensor processSensor(ref XmlReader XmlReader)
//        {
//            Sensor sensor = new Sensor(XmlReader.GetAttribute(XMLDefines.NAME_TAG));

//            sensor.operation = XmlReader.GetAttribute(XMLDefines.OPERATION_TAG);

//            while (XmlReader.Read())
//            {
//                if ((XmlReader.NodeType == XmlNodeType.Element) && (XmlReader.Name == XMLDefines.INFLUENCE_TAG))
//                {
//                    processParam(sensor, ref XmlReader);
//                }
//                else if ((XmlReader.NodeType == XmlNodeType.EndElement) && (XmlReader.Name == XMLDefines.SENSOR_TAG))
//                    return sensor;
//            }

//            OnSensorLoad(sensor);
//            return sensor;
//        }

//        private Behavior processBehavior(ref XmlReader XmlReader)
//        {
//            Behavior behavior = new Behavior(XmlReader.GetAttribute(XMLDefines.NAME_TAG));
//            behavior.spendValue = float.Parse(XmlReader.GetAttribute(XMLDefines.SPEND_TAG));

//            while (XmlReader.Read())
//            {
//                if ((XmlReader.NodeType == XmlNodeType.Element) && (XmlReader.Name == XMLDefines.INFLUENCE_TAG))
//                {
//                    processParam(behavior, ref XmlReader);
//                }
//                else if ((XmlReader.NodeType == XmlNodeType.EndElement) && (XmlReader.Name == XMLDefines.BEHAVIOR_TAG))
//                    return behavior;
//            }

//            OnBehaviorLoad(behavior);
//            return behavior;
//        }

//        private void processElement(ref XmlReader XmlReader)
//        {
//            while (XmlReader.Read())
//            {
//                if (XmlReader.Name == XMLDefines.SENSOR_TAG)
//                {
//                    Sensor proc = processSensor(ref XmlReader);
//                    mListProcessors.AddLast(proc);
//                }
//                else if (XmlReader.Name == XMLDefines.DRIVE_TAG)
//                {
//                    Processor proc = processDrive(ref XmlReader);
//                    mListProcessors.AddLast(proc);
//                }
//                else if (XmlReader.Name == XMLDefines.EMOTION_TAG)
//                {
//                    Processor proc = processEmotion(ref XmlReader);
//                    mListProcessors.AddLast(proc);

//                }
//                else if (XmlReader.Name == XMLDefines.BEHAVIOR_TAG)
//                {
//                    Processor proc = processBehavior(ref XmlReader);
//                    mListProcessors.AddLast(proc);
//                }
//                else if ((XmlReader.NodeType == XmlNodeType.EndElement) && (XmlReader.Name == XMLDefines.MOT_CONTROLLER_TAG))
//                {
//                    break;
//                }
//            }

//        }

//        public MotAgent newMotivationController()
//        {
//            MotAgent temp = new MotAgent(mListProcessors);
//            relinkItems(temp.ContentProcessor);
//            return temp;
//        }


//        #region Handles for pos load items
//        public virtual void OnSensorLoad(Sensor s) { }

//        public virtual void OnBehaviorLoad(Behavior b) { }

//        public virtual void OnDriveLoad(Drive d) { }

//        public virtual void OnEmotionLoad(Emotion e) { }
//        #endregion
//    }
//}

using System;
using System.Collections.Generic;
using System.Text;

namespace MotivationalSystem.DAO
{
    public interface IMotAgentDAO
    {
        IMotAgent getMotAgent(string strClass);
    }

    public class DAOException : MotivationalSystemException
    {
        public DAOException(string msg)
            : base(msg)
        {
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MotivationalSystem
{
    public class MotivationalSystemException: Exception
    {

        public MotivationalSystemException(string problem)
            : base(problem)
        {
        }
    }
}

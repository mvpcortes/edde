using System;
using System.Collections.Generic;
using System.Text;

namespace MotivationalSystem
{
    public interface IPerception: IComparable<IPerception>
    {
        string strSource
        {
            get ;
        }

        object objSource
        {
            get;
        }

        float intensity
        {
            get ;
        }

        string strData
        {
            get ;
        }

        object objData
        {
            get ;
        }

        void copyFrom(IPerception p);
    }
    
    public class Perception<T, D>:IPerception
    {
        private T mSource;
        private float mIntensity;
        private D mData;
        private string mStrData;

        #region implements IPerception
        public string strSource
        {
            get { return mSource.ToString(); }
        }

        public object objSource
        {
            get { return mSource; }
        }

        public float intensity
        {
            get { return mIntensity; }
        }

        public string strData
        {
            get
            {
                  return mStrData;
            }
        }

        public object objData
        {
            get { return mData; }
        }

        public Perception(T _source, float _intensity, D _objData)
        {
            if (_intensity < 0 || _intensity > 1) throw new MotivationalSystemException("Intensidade deveria estar entre [0, 1]");
            mSource = _source;
            mIntensity = _intensity;
            mData = _objData;
        }

        public void copyFrom(IPerception p)
        {
            mIntensity = p.intensity;

            if (p.objData is D)
                this.mData = (D)p.objData;

            if (p.objSource is T)
                this.mSource = (T)p.objSource;
            
            this.mStrData = p.strData;
        }

        #endregion

        public T source
        {
            get { return mSource; }
        }

        public D data
        {
            get { return mData; }
        }

        public override string ToString()
        {
            return string.Format("({0}, {1}, {2})", source, data, intensity);
        }

        #region IComparable<IPerception> Members

        public int CompareTo(IPerception other)
        {
            if (intensity < other.intensity) return -1;
            else if (intensity > other.intensity) return 1;
            else return 0;
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MotivationalSystem
{
    internal class Drive: Processor
    {
        #region Fields
        /// <summary>
        /// Fator de incremento usado para incrementar um processo
        /// </summary>
        private float mIncRate = 1;
        #endregion
        

        public float incRate
        {
            get{return mIncRate;}
            set { mIncRate = value; }
        }

        public Drive()
            : this("")
        {
        }

        public Drive(string name):base(name)
        {
        }

        public Drive(Drive drive):base(drive.name)
        {
            drive.copyTo(this);
        }

        public Drive copyTo(Drive drive)
        {
            base.copyTo(drive);
            drive.mIncRate = this.mIncRate;

            return drive;
        }

        public sealed override Processor clone()
        {
           Drive d = new Drive(this);
           return d;
        }

        public override float biasValue(RUN_TYPE type, float deltaTime)
        {
           return +incRate*deltaTime;
        }

        internal void decValue(float value)
        {
            incValue(-value);
        }
    }
}

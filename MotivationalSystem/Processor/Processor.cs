using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace MotivationalSystem
{
    public class Processor : MotivationalSystem.IProcessor
    {
        public static RUN_TYPE Not(RUN_TYPE rt)
        {
            if (rt == RUN_TYPE.FLIP) return RUN_TYPE.FLOP;
            else return RUN_TYPE.FLIP;
        }

        protected struct Param
        {
            public float mWeight;
            public IProcessor mProcessor;
            public INPUT_MODE mInputMode;

            public Param(IProcessor proc, float weight, INPUT_MODE mode)
            {
                mProcessor = proc;
                mWeight = weight;
                mInputMode = mode;
            }

            public float computer(RUN_TYPE rt)
            {
                   float value = mProcessor.getValue(rt);
                    if( (mInputMode == INPUT_MODE.EVERY_STATE) ||
                        ((mInputMode == INPUT_MODE.OVER_STATE) && (value >= mProcessor.overThreshold)) ||
                        ((mInputMode == INPUT_MODE.UNDER_STATE) && (value <= mProcessor.underThreshold))
                      )
                    {
                        return value*mWeight;
                    }else
                        return 0f;
            }

            public bool isActive(RUN_TYPE rt)
            {
                float value = mProcessor.getValue(rt);
                   return
                        (mInputMode == INPUT_MODE.OVER_STATE) ||
                        ((mInputMode == INPUT_MODE.OVER_STATE) && (value >= mProcessor.overThreshold)) ||
                        ((mInputMode == INPUT_MODE.OVER_STATE) && (value <= mProcessor.underThreshold));
            }
        }

        #region field region

        string mName = "";

        private float mOverThreshold  = 1f;

        private float mUnderThreshold = -1f;

        private float mFlipValue = 0f;

        private float mFlopValue = 0f;

        List<Param> mListParam = new List<Param>();
        #endregion

        protected ICollection<Param> listParam
        {
            get { return mListParam; }
        }

        public override string ToString()
        {
            const string STR_PATTERN = "({0}, {1:E3}, {2:E3})";
            return string.Format(STR_PATTERN, name, this.flipValue, this.flopValue);
        }


        #region IProcessor Members

        public Processor():this("")
        {
        }
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_name">O nome do processo</param>
        public Processor(string _name)
        {
            name = _name;
            mOnAppraisalPerception = NeutralAppaisalPerception;
        }

        protected Processor copyTo(Processor p)
        {
            p.mName                     = this.mName;
            p.mFlipValue                = this.mFlipValue;
            p.mFlopValue                = this.mFlopValue;
            p.mOnAppraisalPerception    = this.mOnAppraisalPerception;
            p.mOverThreshold            = this.mOverThreshold;
            p.mUnderThreshold           = this.mUnderThreshold;
            //n�o � poss�vel fazer c�pia dos par�metros pq s�o refer�ncias a outros processos
            p.mListParam.Clear();

            return p;
        }

        public Processor(Processor p)
        {
            p.copyTo(this);
        }

        public virtual Processor clone()
        {
            Processor p = new Processor(this);
            return p;
        }

        private static float NeutralAppaisalPerception(ICollection<IPerception> collP)
        {
            return collP.Count;//uma avalia��o sempre vai de 0 at� o n�mero de itens percebidos
        }
        //
        private AppraisalPerception mOnAppraisalPerception = null;

        public AppraisalPerception onAppraisalPerception
        {
            get { return mOnAppraisalPerception; }
            set 
            {
                if (ReferenceEquals(value, null))
                    throw new ProcessorException("N�o se pode atribuir um AppraisalPerception nulo", this);
                mOnAppraisalPerception = value; }
        }

        public string name
        {
            get { return mName; }
            set { mName = value; }
        }

        public virtual float overThreshold
        {
            get { return mOverThreshold; }
            set { mOverThreshold = value; }
        }

        public virtual float underThreshold
        {
            get { return mUnderThreshold; }
            set { mUnderThreshold = value; }
        }

        protected float flipValue
        {
            get { return mFlipValue; }
            set { mFlipValue = value; }
        }

        protected float flopValue
        {
            get { return mFlopValue; }
            set { mFlopValue = value; }
        }

        internal void addInput(IProcessor proc, float weight, INPUT_MODE mode)
        {
            int i = 0;
            for (i = 0; i < mListParam.Count; i++)
            {
                if (mListParam[i].mProcessor.name == proc.name)
                {
                    mListParam[i] = new Param(proc, weight, mode);
                }
            }

            if(i == mListParam.Count)
                mListParam.Add(new Param(proc, weight, mode));
        }

        internal void removeInput(IProcessor proc)
        {
            List<int> listRemove  = new List<int>();
            for(int i = 0; i <mListParam.Count; i++)
            {
                if (proc == mListParam[i].mProcessor)
                    listRemove.Add(i);
            }

            foreach(int i in listRemove)
                mListParam.RemoveAt(i);
        }

        internal void removeInput(string nameProc)
        {
            List<int> listRemove = new List<int>();
            for(int i = 0; i <mListParam.Count; i++)
            {
                if (nameProc == mListParam[i].mProcessor.name)
                    listRemove.Add(i);
            }

            foreach(int i in listRemove)
                mListParam.RemoveAt(i);
        }

        public float getValue(RUN_TYPE run_type)
        {
            switch (run_type)
            { 
                case RUN_TYPE.FLIP:
                    return mFlipValue;
                case RUN_TYPE.FLOP:
                    return mFlopValue;
                default:
                    throw new ProcessorException("RunType inv�lido", this);

            }
        }

        public float getMediaValues()
        {
            return (mFlipValue + mFlopValue) / 2f;
        }
        protected void setValue(RUN_TYPE run_type, float value)
        {
            switch (run_type)
            {
                case RUN_TYPE.FLIP:
                    mFlipValue = value;
                    break;
                case RUN_TYPE.FLOP:
                    mFlopValue = value;
                    break;
                default:
                    throw new ProcessorException("RunType inv�lido", this);
            }
        }

        internal void incValue(float value)
        {
            mFlipValue += value;
            mFlopValue += value;
        }

        public virtual void run(RUN_TYPE rt, float deltaTime)
        {
            float valueTemp = 0f;
            foreach (Param p in mListParam)
            {
                float temp = p.computer(Not(rt));
                valueTemp += temp;
            }
      
            valueTemp += biasValue(rt, deltaTime);

           
            //O valor atual � igual ao valor anterior (inverso) Mais o somat�rio
            if (rt == RUN_TYPE.FLIP)
                mFlipValue = mFlopValue + valueTemp;
            else
                mFlopValue = mFlipValue + valueTemp;
        }

        
        public virtual float biasValue(RUN_TYPE type, float deltaTime)
        {
            return 0;
        }

        /// <summary>
        /// avalia uma percep��o
        /// </summary>
     

        #endregion

        #region IComparable<IProcessor> Members

        public int CompareTo(IProcessor other)
        {
            return name.CompareTo(other.name);
        }
        #endregion
    }
}

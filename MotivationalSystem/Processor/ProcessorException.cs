using System;
using System.Collections.Generic;
using System.Text;

namespace MotivationalSystem
{
    public class ProcessorException : MotivationalSystemException
    {
        private IProcessor mProcessor;

        public ProcessorException(string msg, IProcessor processor): base(msg)
        {
            mProcessor = processor;
        }

        public IProcessor processorThrow{
            get
            {
                return mProcessor;
            }
    
        }

        public override string ToString()
        {
            return "Processor: " + mProcessor.name +  " | TextoProcessor"+ mProcessor.ToString() +" | " + "Mensagem: " + this.Message;
        }
    }
}

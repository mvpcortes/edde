using System;
using System.Collections.Generic;
using System.Text;

namespace MotivationalSystem
{
    internal class Emotion:Processor
    {
        #region delegate and events
        public delegate void EmotionEvent(Emotion emo);

        public event EmotionEvent OnBeginEmotion;
        public event EmotionEvent OnEfetiveEmotion;
        public event EmotionEvent OnEndEmotion;
        #endregion


        #region fields
        private float mDecRate = 1;
        private bool mPrevActive = false;
        private float mGoodValue = 1;
        private float mPoleValue = 1;

        private float[] mColor = new float[4];
        #endregion

        public float[] color
        {
            get { return mColor; }
            set { mColor = value; }
        }
        public float decRate
        {
            get { return mDecRate; }
            set { mDecRate = value; }
        }

        public float goodValue
        {
            get { return mGoodValue; }
            set { mGoodValue = value; }
        }

        public float poleValue
        {
            get { return mPoleValue; }
            set { mPoleValue = value; }
        }

        public Emotion()
            : this("")
        {
        }
        public Emotion(Emotion emotion):base(emotion.name)
        {
            emotion.copyTo(this);
        }

        public Emotion copyTo(Emotion e)
        {
            base.copyTo(e);
            e.mDecRate      = mDecRate;
            e.mPrevActive   = mPrevActive;
            e.mGoodValue    = mGoodValue;
            e.mPoleValue    = mPoleValue;
            e.color         = mColor;

            return e;
        }

        public sealed override Processor clone()
        {
           Emotion e = new Emotion(this);
           return e;
        }

        public sealed override float underThreshold
        {
            get { return base.underThreshold; }
            set { throw new ProcessorException("N�o � poss�vel especificar o UnderThreshold para Emotion", this); }
        }

        public Emotion(string name):base(name)
        { 
        }

        public sealed override float biasValue(RUN_TYPE type, float deltaTime)
        {
            return -decRate * deltaTime;
        }

        public sealed override void run(RUN_TYPE rt, float deltaTime)
        {
            base.run(rt, deltaTime);
            setValue(RUN_TYPE.FLIP, Math.Max(0, getValue(RUN_TYPE.FLIP)));
            setValue(RUN_TYPE.FLOP, Math.Max(0, getValue(RUN_TYPE.FLOP)));

            bool active = (getValue(rt) > overThreshold);
            if (active)
            {
                if(mPrevActive)
                {
                    if (OnEfetiveEmotion != null) OnEfetiveEmotion(this);
                }else
                {
                    if (OnBeginEmotion != null) OnBeginEmotion(this);
                }
            }
            else
            {
                if (mPrevActive)
                {
                    if (OnEndEmotion != null) OnEndEmotion(this);
                }
            }

        }

    }
}

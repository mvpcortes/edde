using System;
using System.Collections.Generic;
using System.Text;

namespace MotivationalSystem
{
    internal class Behavior: Processor, IComparable<Behavior>
    {
        /// <summary>
        /// Struct interna para representar os processos que s�o consumados por este elemento
        /// </summary>
        private struct Consummate
        {
            private Processor mProcessor;
            private float mWeight;

            public Processor processor
            {
                get { return mProcessor; }
            }

            public float weight
            {
                get { return mWeight; }
            }

            public Consummate(Processor p, float weight)
            {
                mProcessor = p;
                mWeight = (weight);
            }

        };

        #region fields

        //A��o sendo efetuada
        private IAction mAction = new NullAction();
        
        /// <summary>
        /// Lista de processos consumados por este comportamento.
        /// </summary>
        private List<Consummate> mListConsummatorProcessor  = new List<Consummate>();


        /// <summary>
        /// Lista de comportamentos facilitadores deste comportamento
        /// </summary>
        private List<Behavior> mListAppetitiveBehavior = new List<Behavior>();

        //valor de gasto de execu��o
        private float mSpendValue = 1;

        private bool mWasRun = false;

        #endregion


        private bool wasRunning
        {
            set { mWasRun = value; }
            get { return mWasRun; }
        }
        public float spendValue
        {
            get { return mSpendValue; }
            set { mSpendValue = value; }
        }

        public Behavior() : this("") { }

        public Behavior(string name)
            : base(name)
        {
        }

        public Behavior(Behavior b):
            base(b.name)
        {
            b.copyTo(this);
        }

        public Behavior copyTo(Behavior b)
        {
            base.copyTo(b);

            b.mAction = this.mAction;

            b.mSpendValue = this.mSpendValue;

            b.mListConsummatorProcessor.Clear();

            return b;
        }

        public sealed override Processor clone()
        {
            Processor p = new Behavior(this);
            return p;
        }

        public override sealed float underThreshold
        {
            get { return base.underThreshold; }
            set { throw new ProcessorException("N�o � poss�vel especificar o UnderThreshold para Behavior", this); }
        }

        public sealed override float biasValue(RUN_TYPE type, float deltaTime)
        {
            return -getValue(type) / 2;//a m�dia dividida por 2;
        }

        public sealed override void run(RUN_TYPE rt, float deltaTime)
        {

            base.run(rt, deltaTime);

            //bloqueando o negativo
            if (getMediaValues() < 0.00001f)
            {
                setValue(rt, 0f);
                setValue(Not(rt), 0f);
            }

            if (mAction != null)
            {
                float eficiencia;
                if (wasRunning = mAction.isRun(out eficiencia))
                {
                    foreach (Consummate con in mListConsummatorProcessor)
                    {
                        float temp = (float)(deltaTime/TimeSpan.FromMilliseconds(20).TotalSeconds);
                        con.processor.incValue(con.weight * eficiencia * temp);
                    }
                }
            }

            //Caso ele se auto relacione, testa de novo
            if (getMediaValues() < 0.00001f)
            {
                setValue(rt, 0f);
                setValue(Not(rt), 0f);
            }
         
        }

        /// <summary>
        /// determina se para determinado runtime um comportamento pode tentar ativa��o ou n�o.
        /// </summary>
        /// <param name="rt">qual runtime ser� consultado</param>
        /// <returns>Se pode sim ou n�o</returns>
        public bool canAction(RUN_TYPE rt){ return (getValue(rt) > overThreshold) && !wasRunning ;}

        //em tese n�o ser� necess�rio de comportamentos consumadores em separado, � apenas necess�rio inserir este comportamento como
        //influ�ncia de outros comportamentos.
        internal void setAction(IAction action)
        {
            mAction = action;
        }

        internal bool runAction(RUN_TYPE rt)
        {
            if(mAction == null) return false;

            {
                return mAction.runAction(getValue(rt));
            }
        }

        internal void addConsummateProcessor(Processor p, float weight)
        {
            mListConsummatorProcessor.Add(new Consummate(p, weight));
        }

        internal void addAppetitiveBehavior(Behavior b)
        {
            mListAppetitiveBehavior.Add(b);
        }

        internal IEnumerator<Behavior> getAppetitiveEnumerator()
        {
            return mListAppetitiveBehavior.GetEnumerator();
        }

        #region IComparable<Behavior> Members

        public int CompareTo(Behavior other)
        {
            float temp = getMediaValues() - other.getMediaValues();
            if (temp > 0) return 1;
            else if (temp < 0) return -1;
            else
                return 0;
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using MotivationalSystem.Memory;

namespace MotivationalSystem
{
    /// <summary>
    /// Este sensor ler as percep��es do mundo e os envia para o sistema motivacional. � algo pr�ximo aos releasers do Framework de breazeal
    /// Tamb�m existem os "avaliadores emocionais" (Affective Appraisal) que avaliam a percep��o de acordo com o estado dos drivers e comportamentos.
    /// Nosso sensor carrega em si as tr�s camadas de Breazeal: Sensor, Releasers e Appraisal (avalia��o), o que o torna um Processo completo, contradizendo a quest�o do "sub-processo" do modelo anterior.
    /// </summary>
    internal class Sensor: Processor, ISensor
    {
        static int MAX_PERCEPTION_RECEIVE_PER_ITERATION = 20;
        static float PROB_SELECT_RELEVANTE_PERCEPTION = 0.5f;
        #region Fields

        /// <summary>
        /// string contendo o id da fun��o de opera��o
        /// </summary>
        string mStrOperation;

        List<IPerception> mListPerception = new List<IPerception>(MAX_PERCEPTION_RECEIVE_PER_ITERATION);
        #endregion


        /// <summary>
        /// limpa as percep��es deste Sensor.
        /// </summary>
        /// <remarks>
        /// Por enquanto este m�todo � privado pois est� sendo realizado no update do sensor. N�o foi notado problema at� agora.
        /// </remarks>
        internal void clearPerceptions()
        {
            mListPerception.Clear();
        }
     
        //  public delegate void AppraisalPerception(ref Perception p);

    
        //  public event AppraisalPerception OnAppraisalPerception;//avalia as percep��es

        public Sensor()
            : this("")
        {

        }

        public Sensor(string name):base(name)
        {
            operation = name;
        }

        public Sensor(Sensor sensor):base(sensor.name)
        {
            sensor.copyTo(this);
        }

        public Sensor copyTo(Sensor s)
        {
            base.copyTo(s);
            s.mStrOperation = this.mStrOperation;
         
            //n�o � poss�vel clonar a lista de percep��es            
            s.mListPerception.Clear();
            return s;

        }

        public sealed override Processor clone()
        {
           Sensor d = new Sensor(this);
           return d;
        }

        //public delegate void WacthScene(ICollection<Perception> collPercep, string strOp, int maxItems);
        //        public event WacthScene OnWacthScene;

       /// <summary>
       /// depreciate
       /// </summary>
        public string operation
        {
            set { mStrOperation = value;}
            get { return mStrOperation; }
        }

        public override void run(RUN_TYPE rt, float deltaTime)
        {

            float acum = 0f;

            acum += onAppraisalPerception(mListPerception);

            //avalia
            foreach (Param param in listParam)
            {
                if (param.isActive(rt))
                {
                    if (param.mProcessor.onAppraisalPerception != null)
                    {
                        float appraisal = param.mProcessor.onAppraisalPerception(mListPerception);
                        if (appraisal < 0 || appraisal > mListPerception.Count) throw new ProcessorException("Avalia��o dada por um processo inv�lida", this);
                        acum += appraisal * param.mWeight;
                    }

                }
            }

            setValue(rt, acum);
        }



        #region ISensor Members

        public void sendPerception(IPerception perception)
        {
            if(mListPerception.Count == MAX_PERCEPTION_RECEIVE_PER_ITERATION)
            {
                int posRemove = 0;
                for(int i = 1; i < mListPerception.Count; i++)
                {
                    if(mListPerception[i].intensity < mListPerception[posRemove].intensity)
                        posRemove = i;
                }

                if(mListPerception[posRemove].intensity < perception.intensity)
                    mListPerception[posRemove] = perception;
            }else
            {
                mListPerception.Add(perception);
            }
        }

        #endregion

        #region IShortTimeMemory Members

        /// <summary>
        /// Esta fun��o retorna os itens relevantes da percep��o.
        /// Por def. Os itens relevantes s�o todos os itens pr�ximos da m�dia e com maior intensidade (intensidade tente a 1).
        /// A equa��o que especifica isso �: Peso(p) = (p.intensity + (1 - |p.intensity - media|))/2;
        /// 
        /// </summary>
        /// <param name="coll">o container que receber� os itens.</param>
        public void getRelevancePerceptions(ICollection<IPerception> coll)
        {
            coll.Clear();
            float media = 0;
            for (int i = 0; i < mListPerception.Count; i++)
            {
                media = (media + mListPerception[i].intensity)/2f;
            }

            foreach (IPerception p in mListPerception)
            {
                float prob = (p.intensity + (1 - Math.Abs(p.intensity - media))) / 2f;
                if (prob > PROB_SELECT_RELEVANTE_PERCEPTION)
                    coll.Add(p);
            }
        }

        public void getAllPerceptions(ICollection<IPerception> coll)
        {
            coll.Clear();
            foreach (IPerception p in mListPerception)
            {
               coll.Add(p);
            }
        }

        #endregion
    }
}

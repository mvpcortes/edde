using System;
using System.Collections.Generic;
using System.Text;
using edde.components;
using edde;
using Microsoft.Xna.Framework;
namespace NaveRPG
    
{
    public class Nave: GameObject
    {

    [Injection()]
    //public CharSetComponent mSprite;
    public RotationCharSet mSprite;


    /// <summary>
    /// Componente que permite o controle do objeto
    /// </summary>
    InputControl mInputControl;

    public Nave()
    {
        mSprite = new RotationCharSet("amanda");
        setDirection(Vector2.One);
    }

        public override bool init(Scene scene)
        {
            mInputControl = RPG.me.inputControl;
            mSprite.setPattern(edde.CharSet.PATTERN.PATTERN_RPG_2K);
            return base.init(scene);
            
        }


    public override void  update(Microsoft.Xna.Framework.GameTime time)
    {
        int up = 0;
        if (mInputControl.isKeyPress(InputControl.RPG_KEY.LEFT))
        {
            mSprite.rotation -= 0.01f;
        }

        if (mInputControl.isKeyPress(InputControl.RPG_KEY.RIGHT))
        {
            mSprite.rotation += 0.01f;
        }

        if (mInputControl.isKeyPress(InputControl.RPG_KEY.UP))
        {
            up = +1;
        }

        if (mInputControl.isKeyPress(InputControl.RPG_KEY.DOWN))
        {
            up = -1;
        }

        float dif = 0;// mSprite.mPattern.ANGLE_SOURCE;

        Vector2 vec = new Vector2((float)Math.Cos(mSprite.rotation+dif), (float)Math.Sin(mSprite.rotation+dif));
        vec = up*vec/2;

        incPosition(vec);
        vec.Normalize();

        moving = up != 0;
 	    base.update(time);
    }

    }
}

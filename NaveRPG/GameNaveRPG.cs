using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using edde;
using edde.components;
using edde.maps;
namespace NaveRPG
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class GameNaveRPG : edde.RPG
    {
        Map mMap;
        Nave mNave;
        FPS mFPS;
        public GameNaveRPG()
        {
            mFPS = new edde.FPS(this);
            Components.Add(mFPS);
        }


        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            mMap = new edde.maps.Map(this, "audia");
            mNave = new Nave();
            Random random = new Random();

            mMap.floorLayer = new PanoramaLayer("tree1", Point.Zero);

            mMap.setSize(screenWidth * 2, screenHeight * 2);
            mMap.addObject(mNave);
            mMap.followObject(mNave);
            sceneManager.enterScene(mMap);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

            base.Draw(gameTime);
        }
    }
}

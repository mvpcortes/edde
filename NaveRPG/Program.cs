using System;

namespace NaveRPG
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (GameNaveRPG game = new GameNaveRPG())
            {
                game.Run();
            }
        }
    }
}


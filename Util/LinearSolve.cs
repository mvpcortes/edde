using System;
using System.Collections.Generic;
using System.Text;

namespace util
{
    public static class LinearSolve
    {
       private static void generateLU(ref IMatrix<float> mat, ref IMatrix<float> matL, ref IMatrix<float> matU)
        {
            matL.setSize(mat.Width, mat.Height);
            matU.setSize(mat.Width, mat.Height);

            for(int i = 0; i < mat.Width; i++)
                for (int j = 0; j < mat.Height; j++)
            {
                matU[i,j] = mat[i, j];

                if (i == j)
                    matL[i,j] = 1f;
                else
                    matL[i,j] = 0f;
            }

            
            for(int k = 0; k < mat.Width; k++)
            {		
			    for(int i = k + 1; i < mat.Width-1; i++)
			    {
				    float m = matU[ i, k ] / matU[ k, k ];

				    matL[ i, k ] = m;
				    matU[ i, k ] = 0.0f;

				    for(int j = k + 1; j <  mat.Width; j++)
				    {
					    matU[ i, j ] = matU[ i, j ] - ( m * matU[ k, j ] );
				    }
			    }
		    }
        }

        static bool solveDownDiagonalMatrix(ref IMatrix<float> mat, ref float[] vecB, ref float[] vecX)
        {
            for (int k = 0; k < mat.Width; k++)
            {
                vecX[k] = vecB[k];
                for (int i = k - 1; i >= 0; i--)
                {
                    vecX[k] -= mat[k, i] * mat[i,k];
                }

                vecX[k] = vecX[k] / mat[k, k];
            }
            return true;
        }

        static bool solveUpDiagonalMatrix(ref IMatrix<float> mat, ref float[] vecB, ref float[] vecX)
        {
            for (int k = 0; k < mat.Width; k++)
            {
                vecX[k] = vecB[k];
                for (int i =0; i <k-1; i++)
                {
                    vecX[k] -= mat[k, i] * mat[i, k];
                }

                vecX[k] = vecX[k] / mat[k, k];
            }
            return true;
        }

        public struct DataLU
        {
            IMatrix<float> mMatL;
            IMatrix<float> mMatU;

            public DataLU(ref IMatrix<float> mat)
            {
                mMatL = new Matrix<float>(mat.Width, mat.Height);
                mMatU = new Matrix<float>(mat.Width, mat.Height);

                LinearSolve.generateLU(ref mat, ref mMatL, ref mMatU);
            }

            private void generateLU(ref IMatrix<float> mat)
            {
                
                LinearSolve.generateLU(ref mat, ref mMatL, ref mMatU);
            }
            public void solveLU(ref float[] vecX, ref float[] vecB)
            {
                float[] vecY = new float[vecB.Length];
                solveDownDiagonalMatrix(ref mMatL,ref vecB, ref vecY);
                solveUpDiagonalMatrix(ref mMatU,ref vecY, ref vecX); 
            }
        }
    }
}

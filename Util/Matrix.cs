using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace util
{
    public interface IMatrix<T>
    {
        T this[int i, int j]
        {
            get;
            set;
        }

        /// <summary>
        /// propriedade para manipular a largura
        /// </summary>
        int Width
        {
            get;
        }

        /// <summary>
        /// propriedade para manipular a altura
        /// </summary>
        int Height
        {
            get;
        }

        void setSize(int width, int height);
    }

    /// <summary>
    /// Matriz gen�rica usada pelo sistema
    /// </summary>
    /// <typeparam name="T">o tipo dos elementos da matriz</typeparam>
    public class Matrix<T>: IMatrix<T>
    {
        public interface CreateIt
        {
            T DoCreateIt();
        }
        /// <summary>
        /// Vetor contendo os dados da Matrix. Se houver problema de espa�o, por favor substituir por uma List<>
        /// </summary>
        protected T[,] mData;

        /// <summary>
        /// Largura da matriz
        /// </summary>
        protected int mWidth;

        /// <summary>
        /// Altura da matriz
        /// </summary>
        protected int mHeight;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="width">Largura</param>
        /// <param name="height">Altura</param>
        public Matrix(int _width, int _height)
        {
            mData = new T[_width, _height];
            mWidth = _width;
            mHeight = _height;
        }

        public Matrix(int _width, int _height, T[] vecData):
            this(_width, _height)
        {
            int k = 0;
            for (int i = 0; i < Width; i++)
                for (int  j = 0; j < Height; j++)
            {
                mData[i,j] = vecData[k];
                k++;
            }
        }


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="width">Largura</param>
        /// <param name="height">Altura</param>
        public Matrix(int width, int height, T defaultValue)
        {
            mData = new T[width, height];
            mWidth = width;
            mHeight = height;

            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
            {
                mData[i, j] = defaultValue;
            }
        }

        public Matrix(int width, int height, CreateIt createIt)
        {
            mData = new T[width, height];
            mWidth = width;
            mHeight = height;

            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    mData[i, j] = createIt.DoCreateIt();
                }
        }

        /// <summary>
        /// Verifica se dada posi��o x, y � v�lida (existente) dentro da matriz
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>true se for v�lida</returns>
        public bool valid(int x, int y)
        {
           return   (x < mWidth) &&
                    (y < mHeight) &&
                    (x >= 0) &&
                    (y >= 0);
        }

        /// <summary>
        /// Especifica um novo tamanho para a matriz. Tenta salvar elementos da matriz
        /// </summary>
        /// <param name="w">Nova largura</param>
        /// <param name="h">Nova altura</param>
        /// <param name="defaultValue">valor default a ser atribu�do nos novos elementos da lista caso seja aumentada</param>
        public void setSize(int w, int h, T defaultValue)
        {
            T[,] data = new T[w, h];

            int minW = Math.Min(w, Width);
            int minH = Math.Min(h, Height);
            //int maxW = Math.Max(w, Width);
            //int maxH = Math.Max(h, Height);

            int i = 0;
            int j = 0;
            for (i = 0; i < minW; i++)
                for (j = 0; j < minH; j++)
            {
                data[i, j] = mData[i, j];
            }


            for (i = 0; i < minW; i++)
                for (j = minH; j < h; j++)
                    data[i, j] = defaultValue;

            for (j = 0; j < minH; j++)
                for (i = minW; i < w; i++)
                    data[i, j] = defaultValue;

            for (j = minH; j < h; j++)
                for (i = minW; i < w; i++)
                    data[i, j] = defaultValue;
            

            mData = data;
            mWidth = w;
            mHeight = h;
        }

        public void setSize(int w, int h, CreateIt createIt)
        {
            T[,] data = new T[w, h];

            int minW = Math.Min(w, Width);
            int minH = Math.Min(h, Height);
            //int maxW = Math.Max(w, Width);
            //int maxH = Math.Max(h, Height);

            int i = 0;
            int j = 0;
            for (i = 0; i < minW; i++)
                for (j = 0; j < minH; j++)
                {
                    data[i, j] = mData[i, j];
                    if(ReferenceEquals(data[i,j], null))
                        data[i,j] = createIt.DoCreateIt();
                }


            for (i = 0; i < minW; i++)
                for (j = minH; j < h; j++)
                    data[i, j] = createIt.DoCreateIt();

            for (j = 0; j < minH; j++)
                for (i = minW; i < w; i++)
                    data[i, j] = createIt.DoCreateIt();

            for (j = minH; j < h; j++)
                for (i = minW; i < w; i++)
                    data[i, j] = createIt.DoCreateIt();


            mData = data;
            mWidth = w;
            mHeight = h;
        }

        public T[] toArray()
        {
            T[] data = new T[Width * Height];
            int k = 0;
            for(int i = 0; i < Width; i++)
                for (int j = 0; j < Height; j++)
            {
                data[k] = mData[i, j];
                k++;
            }
            return data;
        }


        #region IMatrix<T> Members

        /// <summary>
        /// propriedade para manipular a largura
        /// </summary>
        public int Width
        {
            get { return mWidth; }
            set { setSize(value, Height, default(T)); }
        }

        /// <summary>
        /// propriedade para manipular a altura
        /// </summary>
        public int Height
        {
            get { return mHeight; }
            set { setSize(Width, value, default(T)); }

        }

        /// <summary>
        /// operador [] para retornar uma posi��o na matriz
        /// </summary>
        /// <param name="i">posi��o x</param>
        /// <param name="j">posi��o y</param>
        /// <returns></returns>
        public T this[int i, int j]
        {
            get
            {
                return mData[i, j];
            }

            set
            {
                mData[i, j] = value;
            }
        }

        public void setSize(int w, int h)
        {
            setSize(w, h, default(T));
        }

        #endregion

    }
}

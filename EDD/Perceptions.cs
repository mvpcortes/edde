using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using edde;

namespace edd.motsystem
{
    public class Batida : MotivationalSystem.Perception<GameObject, float>
    {
        public Batida(GameObject origem, float forca, float avaliacao)
            : base(origem, forca, avaliacao)
        {
        }
    }

    public class Conversa: MotivationalSystem.Perception<GameObject, string>
    {
        public Conversa(GameObject origem, float distancia, string texto):base(origem, distancia, texto)
        {
        }
    }

    public class Brincadeira : MotivationalSystem.Perception<GameObject, float>
    {
        public Brincadeira(GameObject origem, float forca, float avaliacao)
            : base(origem, forca, avaliacao)
        {
        }
    }

    public class ViuDesconhecido : MotivationalSystem.Perception<GameObject, Vector2>
    {
        public ViuDesconhecido(GameObject origem, float distancia, Vector2 direcao):
        base(origem, distancia, direcao)
        {
        }
    }

    public class ViuBrinquedo : MotivationalSystem.Perception<Toy, Vector2>
    {
        public ViuBrinquedo(Toy origem, float distancia, Vector2 direcao) :
        base(origem, distancia, direcao)
        {
        }
    }
}

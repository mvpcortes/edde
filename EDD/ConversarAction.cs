﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using MotivationalSystem;

//namespace edd
//{
//    /// <summary>
//    /// Implementa um action só para a ação de conversa.
//    /// É importante fazer isso para tirar peso da classe MotivationalObject.
//    /// </summary>
//    class ConversarAction:ActionComponent
//    {
//        private readonly int BALLON_CONVERSAR = 15;

//        private readonly TimeSpan TIME_CONVERSAR = TimeSpan.FromSeconds(1);

//        public motsystem.MotivationalObject parent
//        {
//            get { return mMotParent; }
//        }
//        #region IAction Members

//        public sealed bool runAction(float intensity)
//        {
//            if (base.runAction(intensity))
//            {
//                mMoverComponent.stopAll();
//                float value = (getPosition() - otherTouch.getRectBase().getCenter()).Length();
//                value = Math.Min(value / CHAR_SIDE, 1);
//                otherTouch.sendConversa(new Conversa(this, value, "ola"));
//                this.mExecutingConversar = true;
//                mTimer.addTimer(TIME_CONVERSAR, OnTimeStopConversar);
//                mTileAnimation.actualTile = BALLON_CONVERSAR;
//                return true;
//            }
//        }

//        public bool isRun(out float efficiency)
//        {
//            if (parent.hasOtherTouch() && !mExecutingConversar)
//            {
//                parent.stopAll();
//                float value = (getPosition() - otherTouch.getRectBase().getCenter()).Length();
//                value = Math.Min(value / CHAR_SIDE, 1);
//                otherTouch.sendConversa(new Conversa(this, value, "ola"));
//                this.mExecutingConversar = true;
//                mTimer.addTimer(TIME_CONVERSAR, OnTimeStopConversar);
//                mTileAnimation.actualTile = BALLON_CONVERSAR;
//                return true;
//            }
//            else
//                return false;
//        }

//        #endregion
//    }
//}

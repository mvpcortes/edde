using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using edde;
using System.Xml;
using edde.maps;
using edde;
using Microsoft.Xna.Framework.Input;
using edd.motsystem;
namespace edd
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class EDDGame : edde.RPG
    {
        #region fields
        private edde.maps.Map mMap;
        private edde.FPS mFPS;
        private AgentMonitor mAgentMonitor;

        private List<MotivationalObject> mListAgents;

        private int mSelectedListAgent = -1;
        private edde.objects.Hero mHero;

        private TimeSpan mLastTime = TimeSpan.Zero;
        #endregion

        #region contants
        private int NPC_COUNT = 2;
        #endregion 

        /// <summary>
        /// Construtor
        /// </summary>
        public EDDGame()
        {
            mFPS = new edde.FPS(this);
            mAgentMonitor = new AgentMonitor(this);
            Components.Add(mFPS);
            Components.Add(mAgentMonitor);

            mListAgents = new List<MotivationalObject>();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        private void loadMap()
        {
            Map mapTemp = new Map(this, "nome_do_mapa");
            ReadXMLTileSetLayer reader = new ReadXMLTileSetLayer("map2");
            mapTemp.floorLayer      = reader.getTileSetLayer(LAYER_ID.FLOOR);
            mapTemp.roomLayer       = reader.getTileSetLayer(LAYER_ID.ROOM);
            mapTemp.upRoomLayer     = reader.getTileSetLayer(LAYER_ID.UPROOM);

            reader.insertObjectsAtScene(mapTemp);
            mMap = mapTemp;

        }
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();

            //mMap = new edde.maps.Map(this, "audia");
            loadMap();
            mHero = new EDDHero(CharSet.PATTERN.PATTERN_DEFAULT);
            Random random = new Random();

            TileSetLayer tl = mMap.floorLayer as TileSetLayer;
            if(tl != null)
                mMap.setSize(tl.completeWidth, tl.completeHeight);
            else
                mMap.setSize(100*16, 100*16);
        
            mHero.position = new Vector2(mMap.width/ 2, mMap.height / 2);
     

            insertRandomNPCS(NPC_COUNT);
            mSelectedListAgent = 0;

            mAgentMonitor.setMonitored(mListAgents[mSelectedListAgent].ContentProcessor, mListAgents[mSelectedListAgent].name);

            //
            edde.objects.DrawCellColision drawCell = new edde.objects.DrawCellColision();
            edde.physicalmanager.CellCollisionManager cellCollision = mMap.collisionManager as edde.physicalmanager.CellCollisionManager;
            
            /*if (!ReferenceEquals(cellCollision, null))
                cellCollision.addVisionObserver(drawCell, mHero);

            mMap.addObject(drawCell);
            */
            mMap.addObject(mHero);
            mMap.followObject(mHero);
            mMap.init();
            sceneManager.enterScene(mMap);
        }



     
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            ////aumenta o n�mero de npcs
            //if (this.getInputControl.isKeyPress(InputControl.RPG_KEY.ACTION_B))
            //{
            //    if (gameTime.TotalGameTime - mLastTime > new TimeSpan(0, 0, 3))
            //    {
            //        mLastTime = gameTime.TotalGameTime;
            //        insertRandomNPCS(50);
            //    }
            //}


            //VerificaControles
            if (inputControl.isKeyUp(InputControl.RPG_KEY.ACTION_L))
            {
                mSelectedListAgent = Math.Max(mSelectedListAgent - 1, 0);
                mAgentMonitor.setMonitored(mListAgents[mSelectedListAgent].ContentProcessor, mListAgents[mSelectedListAgent].name);
            }
            if (inputControl.isKeyUp(InputControl.RPG_KEY.ACTION_R))
            {
                mSelectedListAgent = Math.Min(mSelectedListAgent + 1, mListAgents.Count-1);
                mAgentMonitor.setMonitored(mListAgents[mSelectedListAgent].ContentProcessor, mListAgents[mSelectedListAgent].name);
            }


            base.Update(gameTime);
        }

        //distribui os NPCs de tal forma que eles fiquem sempre no sentro do mapa em posi��es adequadas.
        private void insertFixNPCs(Map map, int qtd)
        {
            int mapSize = (int)Math.Ceiling(Math.Sqrt((double)qtd));

            Vector2 vecProportion = new Vector2();
            vecProportion.X = map.width /mapSize;
            vecProportion.Y = map.height /mapSize;

            for (int i = 0; i < qtd; i++)
            {
                MotivationalObject atual = new MotivationalObject();
                //atual.setRectBase(rectBase);

                atual.setDirection(MyMath.getRandomDir());

                int xMap = i % mapSize;
                int yMap = i / mapSize;
                atual.setPosition(new Vector2(xMap * vecProportion.X + vecProportion.X / 2f, yMap * vecProportion.Y + vecProportion.Y / 2f));
                atual.name = "Agente_" + i.ToString();
                mMap.addObject(atual);
                mListAgents.Add(atual);
            }
        }

        private void insertRandomNPCS(int qtd)
        { 
            RectFloat rectBase = new RectFloat(-2, -2, 2, 2);
            for (int i = 0; i < qtd; i++)
            {
               MotivationalObject atual = new MotivationalObject();
               atual.setRectBase(rectBase);

               atual.setDirection(MyMath.getRandomDir());

               bool inter = false;
               do
               {
                   inter = false;
                   atual.setPosition(MyMath.getRandomPos(mMap.width - 16f, mMap.height - 16f) + new Vector2(16, 16));
                   atual.incPosition(10, 10);
                   foreach (GameObject go in mMap.getGameObjects())
                   {
                       inter |= edde.physicalmanager.Collision.intersects(go.getRectBase(), atual.getRectBase());
                       if (inter) break;
                   }
               } while (inter);

               atual.name = "Agente_" + i.ToString();
               atual.textID = i.ToString();

               mMap.addObject(atual);
               mListAgents.Add(atual);
            }
        }
 
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edd.motsystem;

namespace edd.motsystem
{
    interface IMotivationalObject: edde.physicalmanager.ICollised
    {
        /// <summary>
        /// Envia para este agente uma percepção de conversa. Normalmente outro agente realiza isto.
        /// </summary>
        /// <param name="conversa">Perception sobre uma conversa</param>
        void sendConversa(Conversa conversa);

        /// <summary>
        /// Envia para este agente uma percepção de brincadeira. Normalmente outro agente realiza isto.
        /// </summary>
        /// <param name="brinca">Um Perception representando a brincadeira</param>
        void sendBrincadeira(Brincadeira brinca);

        /// <summary>
        /// Envia uma batida (bate) em outro agente
        /// </summary>
        /// <param name="batida">A percepção de batida</param>
        void sendBatida(Batida batida);
    }
}

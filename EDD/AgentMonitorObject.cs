using System;
using System.Collections.Generic;
using edde.components;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using MotivationalSystem;
using edde;
using edd.motsystem;
namespace edd
{
    class AgentMonitorObject:GameObject
    {
        [Injection]
        private GameGridComponent mGrid;

        #region fields
        private MotivationalObject mObjectMonitored;

        private static TimeSpan DELAY_UPDATE = TimeSpan.FromSeconds(0.5);

        private TimeSpan mLastTime = TimeSpan.Zero;
        #endregion

        public MotivationalObject monitored
        {
            get { return mObjectMonitored; }
            set 
            {
                mObjectMonitored = value;
                ///Atualiza os valores da grid pela primeira vez.
                if (hasMonitored())
                {
                    int rowCount = mObjectMonitored.ContentProcessor.Count + 1;

                    mGrid.setSize(rowCount, 3);

                    mGrid.setColWidth(0, 100);
                    mGrid.setColWidth(1, 80);
                    mGrid.setColWidth(2, 80);

                    int i = 1;

                    foreach(IProcessor p in mObjectMonitored.ContentProcessor)
                    {
                        mGrid[i, 0].mText = p.name;
                        i++;
                    }
                }
                else
                {
                    mGrid.setSize(1, 3);

                    mGrid.setColWidth(0, 100);
                    mGrid.setColWidth(1, 80);
                    mGrid.setColWidth(2, 80);
                }

                updateGrid();
            }
        }

        public AgentMonitorObject()
        {
            mGrid = new GameGridComponent(1, 1);
            mGrid.fixScreen = true;
            mGrid.pointReference = Point.Zero;
        }

        public override bool init(Scene scene)
        {
            if(base.init(scene))
            {
                return true;
            }else
                return false;
        }

        bool hasMonitored() { return !ReferenceEquals(mObjectMonitored, null); }

        void updateGrid()
        {
            if (!hasMonitored())
            {
                mGrid[0, 0].text = "sem nome";
                for (int i = 1; i < mGrid.height; i++ )
                {
                    mGrid[i, 1].mText = "---";
                    mGrid[i, 2].mText = "---";
                }
            }
            else
            {
                mGrid[0, 0].mText = monitored.name;
                int i = 1;
                foreach (IProcessor p in monitored.ContentProcessor)
                {
                    mGrid[i, 1].mText = string.Format("{0:E3}", p.getValue(RUN_TYPE.FLIP));
                    mGrid[i, 2].mText = string.Format("{0:E3}", p.getValue(RUN_TYPE.FLOP));
                    i++;
                }
            }
        }

        public override void update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            TimeSpan time = gameTime.TotalGameTime;
            base.update(gameTime);

            if (time - mLastTime > DELAY_UPDATE)
            {
                updateGrid();
                mLastTime = time;
            }
        }
    }
}

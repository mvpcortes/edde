﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde.components;
using edde;

namespace edd
{
    class EDDHero: edde.objects.Hero, edd.motsystem.IMotivationalObject
    {
        [Injection]
        private TextComponent mTextComponent;

        public EDDHero(edde.CharSet.PATTERN pattern):base(pattern)
        {
            mTextComponent = new TextComponent("arial_8");
            mTextComponent.text = "Hero";
        }

        public override void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
        {
            base.draw(time, ref rectWindow);
        }
        #region IMotivationalObject Members

        public void sendConversa(edd.motsystem.Conversa conversa)
        {
            //nothing
        }

        public void sendBrincadeira(edd.motsystem.Brincadeira brinca)
        {
            //nothing
        }

        #endregion

        #region IMotivationalObject Members


        public void sendBatida(edd.motsystem.Batida batida)
        {
            //nothing
        }

        #endregion
    }
}

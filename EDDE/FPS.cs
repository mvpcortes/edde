using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;

namespace edde
{
    /// <summary>
    /// This is a FPS manager.
    /// </summary>
    public class FPS : Microsoft.Xna.Framework.DrawableGameComponent
    {
        /// <summary>
        /// FPS atual
        /// </summary>
        private float mFPS = 60;

        /// <summary>
        /// FPS m�dio
        /// </summary>
        private float mMediaFPS = 60; 

        /// <summary>
        /// �ltimo tempo passado do c�lculo do FPS
        /// </summary>
        private TimeSpan mTimePast = TimeSpan.Zero;

        /// <summary>
        /// N�mero de Frames exibidos desde o �ltimo FPS calculado
        /// </summary>
        private float mFrameCount = 0;

        /// <summary>
        /// Especifica se deve-se exibir (pintar) o fps ou n�o.
        /// </summary>
        private bool mDrawFPS = true;

        /// <summary>
        /// Spriteset da fonte usada
        /// </summary>
        private SpriteFont mFont;


        /// <summary>
        /// Refer�ncia ao SpriteBatch necess�rio para resgatar a fonte.
        /// </summary>
        private SpriteBatch mSpriteBatch;



        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="game"></param>
        public FPS(RPG game)
            : base(game)
        {
        }

        /// <summary>
        /// Retorna se est� pintando o FPS ou n�o.
        /// </summary>
        /// <returns></returns>
        public bool drawFPS
        {
            get { return mDrawFPS; }

            set { mDrawFPS = value; }
        }

        public sealed override void Draw(GameTime gameTime)
        {
            TimeSpan elapsed = gameTime.ElapsedRealTime;
            mFrameCount++;

            mTimePast += elapsed;

            if (mTimePast.Seconds > 1)
            {
                mFPS = mFrameCount / mTimePast.Seconds;
               
                if(Game.IsActive)
                    mMediaFPS = 0.65f * mMediaFPS + 0.35f * mFPS;

                mTimePast = TimeSpan.Zero;
                mFrameCount = 0;
            }

            if (drawFPS)
            {
                mSpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.None);
                string tempStr = string.Format("Frame: {0:000} Media: {1:000}", mFPS, mMediaFPS);
                 mSpriteBatch.DrawString(mFont, tempStr, new Vector2(0f, RPG.me.screenHeight - 50f), Color.Green, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                 mSpriteBatch.End();
             }

            base.Draw(gameTime);
        }
      
        protected override void LoadContent()
        {
            base.LoadContent();
            mSpriteBatch = ((RPG)(Game)).spriteBatch;
            mFont = Resource.getFont("sys_font");
        }

        public override void Initialize()
        {
            base.Initialize();
        }
    }
}
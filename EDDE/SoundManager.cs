using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace edde
{
    /// <summary>
    /// Um componente de XNA para gerar sons.
    /// </summary>
    public class SoundManager: Microsoft.Xna.Framework.GameComponent
    {
        public bool RepeatPlayList = true;
        private AudioEngine mEngine;
        private WaveBank mWaveBank;
        private SoundBank mSoundBank;

        private Dictionary<string, Cue> mCues = new Dictionary<string, Cue>();

        private Dictionary<string, AudioCategory> mCategories = new Dictionary<string, AudioCategory>();
        private string[] mPlayList;
        private int mCurrentSong;
        private Cue mCurrentlyPlaying;

        public SoundManager(Game game, string xactProjectName)
        : this(game, xactProjectName, xactProjectName)
        { 
        }

        public SoundManager(Game game, string xactProjectName, string xactFileName)
        : this(game, xactProjectName, xactFileName, @"Content\sounds\")
        { }

        public SoundManager(Game game, string xactProjectName, string xactFileName, string contentPath)
        : base(game)
        {
            xactFileName = xactFileName.Replace(".xap", "");
            mEngine = new AudioEngine(contentPath + xactFileName + ".xgs");
            mWaveBank = new WaveBank(mEngine, contentPath + "Wave Bank.xwb");
            mSoundBank = new SoundBank(mEngine, contentPath + "Sound Bank.xsb");
        }
        
        /*public override void Initialize()
        {
            base.Initialize();
        }*/

        public override void Update(GameTime gameTime)
        {
            mEngine.Update();
            if (mCurrentlyPlaying != null) //are we playing a list?
            {
                //check current cue to see if it is playing
                //if not, go to next cue in list
                if (!mCurrentlyPlaying.IsPlaying)
                {
                    mCurrentSong++;
                    if (mCurrentSong == mPlayList.Length)
                    {
                        if (RepeatPlayList)
                            mCurrentSong = 0;
                        else
                            StopPlayList();
                    }
                    //may have been set to null, if we finished our list
                    if (mCurrentlyPlaying != null)
                    {
                        mCurrentlyPlaying = mSoundBank.GetCue(
                        mPlayList[mCurrentSong]);
                        mCurrentlyPlaying.Play();
                    }
                }
            }
            base.Update(gameTime);
        }

        protected override void Dispose(bool disposing)
        {
            mSoundBank.Dispose();
            mWaveBank.Dispose();
            mEngine.Dispose();
            mPlayList = null;
            mCurrentlyPlaying = null;
            mCues = null;
            mSoundBank = null;
            mWaveBank = null;
            mEngine = null;
            base.Dispose(disposing);
        }

        public void SetGlobalVariable(string name, float amount)
        {
            mEngine.SetGlobalVariable(name, amount);
        }
        private void CheckCategory(string categoryName)
        {
            if (!mCategories.ContainsKey(categoryName))
                mCategories.Add(categoryName, mEngine.GetCategory(categoryName));
        }

        public void SetVolume(string categoryName, float volumeAmount)
        {
            CheckCategory(categoryName);
            mCategories[categoryName].SetVolume(volumeAmount);
        }

        public void PauseCategory(string categoryName)
        {
            CheckCategory(categoryName);
            mCategories[categoryName].Pause();
        }

        public void ResumeCategory(string categoryName)
        {
            CheckCategory(categoryName);
            mCategories[categoryName].Resume();
        }

        public bool IsPlaying(string cueName)
        {
            if (mCues.ContainsKey(cueName))
                return (mCues[cueName].IsPlaying);

            return (false);
        }

        public void Play(string cueName)
        {
            Cue prevCue = null;
            if (!mCues.ContainsKey(cueName))
                mCues.Add(cueName, mSoundBank.GetCue(cueName));
            else
            {
                //store our cue if we were playing
                if (mCues[cueName].IsPlaying)
                    prevCue = mCues[cueName];
                mCues[cueName] = mSoundBank.GetCue(cueName);
            }

            //if we weren�t playing, set previous to our current cue name
            if (prevCue == null) prevCue = mCues[cueName];
            //  try
            //   {
            mCues[cueName].Play();
            //}
            // catch (InstancePlayLimitException e)
            /*  {
                  Microsoft.Xna.Framework.Audio e;
                  //hit limit exception, set our cue to the previous
                  //and let�s stop it and then start it up again ...
                  mCues[cueName] = prevCue;
                  if (mCues[cueName].IsPlaying)
                  mCues[cueName].Stop(AudioStopOptions.AsAuthored);
                  Toggle(cueName);
              }*/
        }

        public void Pause(string cueName)
        {
            if (mCues.ContainsKey(cueName))
            mCues[cueName].Pause();
        }

        public void Resume(string cueName)
        {
            if (mCues.ContainsKey(cueName))
            mCues[cueName].Resume();
        }
        public void Toggle(string cueName)
        {
            if (mCues.ContainsKey(cueName))
            {
                Cue cue = mCues[cueName];
                if (cue.IsPaused)
                    cue.Resume();
                else if (cue.IsPlaying)
                    cue.Pause();
                else //played but stopped
                {
                    //need to reget cue if stopped
                    Play(cueName);
                }
            }
            else //never played, need to reget cue
                Play(cueName);
        }

        public void StopAll()
        {
            foreach (Cue cue in mCues.Values)
                cue.Stop(AudioStopOptions.Immediate);
        }

        public void Stop(string cueName)
        {
            if (mCues.ContainsKey(cueName))
                mCues[cueName].Stop(AudioStopOptions.Immediate);
               
            mCues.Remove(cueName);
        }

        public void StartPlayList(string[] playList)
        {
            StartPlayList(playList, 0);
        }

        public void StartPlayList(string[] playList, int startIndex)
        {
            if (playList.Length == 0)
                    return;

            mPlayList = playList;

            if (startIndex > playList.Length)
                startIndex = 0;

            StartPlayList(startIndex);
        }
         
        public void StartPlayList(int startIndex)
        {
            if (mPlayList.Length == 0)
                return;

            mCurrentSong = startIndex;
            
            mCurrentlyPlaying = mSoundBank.GetCue(mPlayList[mCurrentSong]);
            
            mCurrentlyPlaying.Play();
        }

        public void StopPlayList()
        {
            if (mCurrentlyPlaying != null)
            {
                mCurrentlyPlaying.Stop(AudioStopOptions.Immediate);
                mCurrentlyPlaying = null;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace edde
{
    /// <summary>
    /// Exce��o b�sica da engine.
    /// </summary>
    public class eddeException : Exception
    {
        /// <summary>
        /// Determina o tipo da exce��o
        /// </summary>
        public enum REGION_LOOP
        {
            LOAD,
            DRAW,
            UPDATE,
            OTHER
        }


        /// <summary>
        /// Armazena qual foi a regi�o que ocorreu o loop
        /// </summary>
        private REGION_LOOP mRegionLoop;


        /// <summary>
        /// Get/Set da regi�o onde ocorreu o loop
        /// </summary>
        public REGION_LOOP regionLoop
        {
            get { return mRegionLoop; }
            set { mRegionLoop = value; }
        }


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="problem">Descreve qual foi o problema</param>
        /// <param name="source">Determina a exce��o de origem. Caso n�o haja deve ser setado NULL</param>
        /// <param name="region">Determina a regi�o onde ocorreu a exce��o dentro do GameLoop</param>
        public eddeException(string problem, Exception source, REGION_LOOP region)
            : base(problem, source)
        {
            mRegionLoop = region;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="problem">Descreve qual foi o problema</param>
        /// <param name="region">Determina a regi�o onde ocorreu a exce��o dentro do GameLoop</param>
        public eddeException(string problem, REGION_LOOP region)
            : base(problem)
        {
            mRegionLoop = region;
        }


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="problem">Descreve qual foi o problema</param>
        public eddeException(string problem)
            : this(problem, REGION_LOOP.OTHER)
        {
        }

        public override string ToString()
        {
            string ret = "";
            switch (regionLoop)
            {
                case REGION_LOOP.OTHER:
                    ret =  "Exce��o edde em regi�o desconhecida.";
                    break;
                case REGION_LOOP.DRAW:
                    ret = "Exce��o no loop de Draw.";
                    break;

                case REGION_LOOP.UPDATE:
                    ret = "Exce��o no loop de Update.";
                    break;
            }

            return ret + "n" + this.Message;
        }
    }
}

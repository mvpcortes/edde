using System;
using System.Collections.Generic;
using System.Text;
using util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using edde.physicalmanager;

namespace edde.maps
{
    public class TileSetLayer : ILayer, physicalmanager.ICollised
    {
        private TileSet     mTileSet;
        private Matrix<int> mMatrixData;

        private struct IntRange
        {
            private int mMin, mMax;

            static public readonly IntRange NOTHING = new IntRange(int.MaxValue, int.MinValue);
            static public readonly IntRange EVERY = new IntRange(int.MinValue, int.MaxValue);

            public int min
            {
                get{return mMin;}
            }

            public int max
            {
                get{return mMax;}
            }

            public IntRange(int value) : this(value, value) { }
            public IntRange(int min, int max)
            {
                if (max < min && max != int.MinValue) throw new ArgumentException("valor m�nimo � maior que o m�ximo.", "min");

                mMin = min;
                mMax = max;
            }

            public bool inRange(int value)
            {
                return (value <= mMax) && (value >= mMin);
            }

            public bool isLess(IntRange other)
            {
                return this.max < other.min;
            }

            public bool isIntercession(IntRange other)
            {
                return (this.min >= other.max) || (this.max <= other.min);
            }

            public void concat(IntRange other)
            {
                mMin = Math.Min(this.mMin, other.min);
                mMax = Math.Max(this.mMax, other.max);
            }
        }
        private ICollection<IntRange>   mCollisedTileSet;

        public TileSetLayer(string nameTileSet):this(nameTileSet, 1,1)
        {
        }

        public TileSetLayer(int w, int h)
        {
            mTileSet = null;
            mMatrixData = new Matrix<int>(w, h);
            mCollisedTileSet = new List<IntRange>();
        }

        public TileSetLayer(string nameTileSet, int w, int h):this(w, h)
        {
            mTileSet = new TileSet(nameTileSet);
        }

        public int Width
        {
            get { return mMatrixData.Width; }
        }
       
        public int Height
        {
            get { return mMatrixData.Height; }
        }

        public int tileWidth
        {
            get { return mTileSet.getTileWidth(); }
        }

        private int tileHeight
        {
            get { return mTileSet.getTileHeight(); }
        }

        public int completeWidth
        {
            get { return Width * mTileSet.getTileWidth(); }
        }

        public int completeHeight
        {
            get { return Height * mTileSet.getTileHeight(); }
        }

        public void setTileSetSize(int w, int h)
        {
            mTileSet.setTileWidth(w);
            mTileSet.setTileHeight(h);
        }

        public void setLayerSize(int w, int h)
        {
            mMatrixData.setSize(w, h, TileSet.INVALID_TILESET);
        }

        #region Collision methods
        public void addCollisionTile(int value)
        {
            mCollisedTileSet.Add(new IntRange(value));
        }

        public void addCollisionRangeTile(int minValue, int maxValue)
        {
            mCollisedTileSet.Add(new IntRange(minValue, maxValue));
        }

        public void addCollisionTileAll()
        {
            mCollisedTileSet.Clear();
            mCollisedTileSet.Add(IntRange.EVERY);
        }

        public void clearCollisionTile()
        {
            mCollisedTileSet.Clear();
        }
        #endregion

        public void setTileSet(string nameTileSet)
        {
            mTileSet = new TileSet(nameTileSet);
        }
      
        public void importStringData(string data, int maxW, int maxH)
        {
            setLayerSize(maxW, maxH);
            importStringData(data);
        }

        public int getTile(int x, int y)
        {
            return mMatrixData[x, y]; 
        }

        public void setTile(int x, int y, int value)
        {
            mMatrixData[x, y] = value;
        }
        public void importStringData(string data)
        {
            string[] lines = data.Split('\n');
            int y = 0;
            foreach (string str in lines)
            {
                if (y >= mMatrixData.Height) break;
                string[] param = str.Split(',');
                int x = 0;
                foreach (string value in param)
                {
                    if (x >= mMatrixData.Width) break;
                    try
                    {
                        mMatrixData[x, y] = Int32.Parse(value);
                    }
                    catch (FormatException)
                    {
                        mMatrixData[x, y] = TileSet.INVALID_TILESET;
                    }

                    x++;
                }
                y++;
            }
        }

        #region Layer Members

        public void draw(Rectangle rect, ICollection<GameObject> coll, GameTime gameTime)
        {
            if (ReferenceEquals(mTileSet, null)) return;

            SpriteBatch sb = RPG.me.spriteBatch;
            Point initCell, endCell;

            {
                initCell.X = Math.Max(0, rect.Left / Width);
                initCell.Y = Math.Max(0, rect.Top / Height);

                endCell.X = Math.Min(Width - 1, (rect.Right / mTileSet.getTileWidth()) + 1);
                endCell.Y = Math.Min(Height - 1, (rect.Bottom / mTileSet.getTileHeight()) + 1);
            }

            for (int i = initCell.X; i <= endCell.X; i++)
                for (int j = initCell.Y; j <= endCell.Y; j++)
                {
                    if (mMatrixData[i, j] != TileSet.INVALID_TILESET)
                    {

                        Rectangle rectDest = new Rectangle(
                            i * mTileSet.getTileWidth() - rect.Left,
                            j * mTileSet.getTileHeight() - rect.Top,
                            mTileSet.getTileWidth(),
                            mTileSet.getTileHeight()
                            );

                        Rectangle rectSource = mTileSet.getRect(mMatrixData[i, j]);

                        sb.Draw(mTileSet.getTexture(), rectDest, rectSource, Color.White);
                    }
                }
            //
            foreach (GameObject go in coll)
            {
                go.draw(gameTime,ref rect);
            }
        }
        public void draw(Rectangle rect, GameTime gameTime)
        {
            if(ReferenceEquals(mTileSet, null)) return;

            SpriteBatch sb = RPG.me.spriteBatch;
            Point initCell, endCell;

            {
                initCell.X = Math.Max(0, rect.Left / Width);
                initCell.Y = Math.Max(0, rect.Top / Height);

                endCell.X = Math.Min(Width - 1, (rect.Right / mTileSet.getTileWidth()) + 1);
                endCell.Y = Math.Min(Height - 1, (rect.Bottom / mTileSet.getTileHeight()) + 1);
            }

            for (int i = initCell.X; i <= endCell.X; i++)
                for (int j = initCell.Y; j <= endCell.Y; j++)
                {
                    if (mMatrixData[i, j] != TileSet.INVALID_TILESET)
                    {

                        Rectangle rectDest = new Rectangle(
                            i * mTileSet.getTileWidth() - rect.Left,
                            j * mTileSet.getTileHeight() - rect.Top,
                            mTileSet.getTileWidth(),
                            mTileSet.getTileHeight()
                            );

                        Rectangle rectSource = mTileSet.getRect(mMatrixData[i, j]);

                        sb.Draw(mTileSet.getTexture(), rectDest, rectSource, Color.White);
                    }
                }
        }

        #endregion

        #region ICollised Members

        public RectFloat getRectBase()
        {
            return new RectFloat(Vector2.Zero, new Vector2(completeWidth, completeHeight));
        }

        public COLLISION_TYPE getTypeCollision()
        {
            return COLLISION_TYPE.SOLID;
        }

        public bool intersects(RectFloat rect)
        {
            Point initCell, endCell;

            {
                initCell.X = Math.Max(
                    0,
                    (int)rect.left / tileWidth
                    );
                initCell.Y = Math.Max(
                    0,
                    (int)rect.top / tileHeight
                    );

                endCell.X = Math.Min(Width - 1, ((int)rect.right / tileWidth));
                endCell.Y = Math.Min(Height - 1, ((int)rect.bottom / tileHeight));
            }

             for (int i = initCell.X; i <= endCell.X; i++)
                 for (int j = initCell.Y; j <= endCell.Y; j++)
                 {
                     int tileID = mMatrixData[i, j];
                     foreach (IntRange intRange in mCollisedTileSet)
                     {
                         if (intRange.inRange(tileID))
                             return true;
                     }
                 }

             return false;

        }
        #endregion
    }
}

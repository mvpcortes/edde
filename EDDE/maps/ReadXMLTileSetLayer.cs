﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using util;
using edde.objects;

namespace edde.maps
{
    public class ReadXMLTileSetLayer
    {
        #region fields
        private XmlReader mXmlReader;

        private Matrix<int> mMatrixData;

        private int mWidth, mHeight, mTileWidth, mTileHeight, mOffSetID;

        private string mFileName;

        private string mStrMapName;

        private static int msIntCountFileOpen;

        private Dictionary<string, TileSetLayer> mDicTileSetLayer;

        //usado na compilação de objetos, suas propriedades
        private List<GameObjectFactory> mListGameObjectFactories;
        #endregion

        public void insertObjectsAtScene(Scene scene)
        {
            foreach (GameObjectFactory gof in mListGameObjectFactories)
                scene.addObject(gof.createGameObject());
        }

        private TileSetLayer addTileSetLayer(string name, string path, int width, int height, int tileWidth, int tileHeight)
        {
            TileSetLayer tile = new TileSetLayer(path, width, height);
            tile.setTileSetSize(tileWidth, tileHeight);
            mDicTileSetLayer[name] = tile;
            return tile;
        }

        public TileSetLayer getTileSetLayer(string name)
        {
            TileSetLayer tileSetLayer = null;
            if (mDicTileSetLayer.TryGetValue(name, out tileSetLayer))
                return tileSetLayer;
            else
                return null;
        }

        public Map generateNewMap()
        {
            Map map = new Map(RPG.me, mStrMapName);

            int maxWidth = int.MinValue;
            int maxheight = int.MinValue;
            
            //o loop deve ser separado para poder setar o tamanho do mapa antes de atribuir os layers
            for (LAYER_ID id = LAYER_ID.FIRST; id < LAYER_ID.LENGTH; id++)
            {
                TileSetLayer layer = getTileSetLayer(id) as TileSetLayer;
                if (layer != null)
                {
                    maxWidth = Math.Max(maxWidth, layer.completeWidth);
                    maxheight = Math.Max(maxheight, layer.completeHeight);
                }

            }
            
            map.setSize(maxWidth, maxheight);

            for(LAYER_ID id = LAYER_ID.FIRST; id < LAYER_ID.LENGTH; id++)
            {
                TileSetLayer layer = getTileSetLayer(id) as TileSetLayer;
                if(layer != null)
                    map.setLayer(id, layer);
            }

            foreach (GameObjectFactory gof in mListGameObjectFactories)
                map.addObject(gof.createGameObject());

            return map;
        }

        public ILayer getTileSetLayer(LAYER_ID id)
        {
            string name = "";
            switch (id)
            {
                case LAYER_ID.FLOOR:
                    name = "floor";
                    break;

                case LAYER_ID.ROOM:
                    name = "room";
                    break;
                case LAYER_ID.UPROOM:
                    name = "up_room";
                    break;
            }

            ILayer layer = getTileSetLayer(name);
            if (layer != null)
                return layer;
            else
                return new VoidLayer();
        }



        private XmlReader reader
        {
            get { return mXmlReader; }
        }

        public ReadXMLTileSetLayer(string name)
            : this(Resource.getXMLMap(name))
        {
            this.mStrMapName = name;
        }

        public ReadXMLTileSetLayer(XmlReader xmlReader)
        {
            mListGameObjectFactories = new List<GameObjectFactory>();
            mDicTileSetLayer = new Dictionary<string, TileSetLayer>();
            mXmlReader = xmlReader;
            mMatrixData = new Matrix<int>(0, 0, 0);
            mWidth = mHeight = mTileWidth = mTileHeight = 0;
            mOffSetID = 0;
            mFileName = "";
            mStrMapName = "noname_" + msIntCountFileOpen;
            msIntCountFileOpen++;
            processXml();
        }

        private void processTileSet()
        {
            mOffSetID = int.Parse(reader.GetAttribute(XMLTileSetConstants.OFFSET_TAG));
            //mName = reader.GetAttribute(XMLTileSetConstants.NAME_TAG);
            while (reader.Read())
            {
                if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == XMLTileSetConstants.TILE_SET_TAG))
                    return;
                else if (reader.NodeType == XmlNodeType.Element)
                {
                    if (reader.Name == XMLTileSetConstants.IMAGE_TAG)
                    {
                        mFileName = reader.GetAttribute(XMLTileSetConstants.SOURCER_TAG);
                        int pos = mFileName.LastIndexOf('.');
                        if (pos > 0)
                            mFileName = mFileName.Substring(0, pos);
                    }
                }
            }

        }

        private void processCollision(TileSetLayer layer)
        {
            while (reader.Read())
            {
                if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == XMLTileSetConstants.COLLISION_TAG))
                    return;
                else if (reader.NodeType == XmlNodeType.Element)
                {
                    if ((reader.Name == XMLTileSetConstants.TILE_TAG))
                    {
                        string value = reader.GetAttribute(XMLTileSetConstants.GID_TAG);
                        if (value == XMLTileSetConstants.EVERY_TAG)
                        {
                            layer.clearCollisionTile();
                            layer.addCollisionRangeTile(0, int.MaxValue);
                        }
                        else if (value == XMLTileSetConstants.NOTHING_TAG)
                            layer.clearCollisionTile();
                        else
                        {

                            int f = 0;
                            if (int.TryParse(value, out f))
                            {

                                int id = f - mOffSetID;
                                if (id >= 0)
                                {
                                    layer.addCollisionTile(f);
                                }
                            }
                        }
                    }
                    else if (reader.Name == XMLTileSetConstants.TILE_RANGE_TAG)
                    {
                        int minValue = getIntAttribute(XMLTileSetConstants.MIN_ATTRIB);
                        int maxValue = getIntAttribute(XMLTileSetConstants.MAX_ATTRIB);
                        layer.addCollisionRangeTile(minValue, maxValue);
                    }
                }
            }
        }
        private void processXml()
        {
            while (reader.Read())
            {
                if ((mXmlReader.NodeType == XmlNodeType.Element) && (mXmlReader.Name == XMLTileSetConstants.MAP_TAG))
                {
                    mWidth = getIntAttribute(XMLTileSetConstants.WIDTH_TAG);
                    mHeight = getIntAttribute(XMLTileSetConstants.HEIGHT_TAG);
                    mTileWidth = getIntAttribute(XMLTileSetConstants.TILE_WIDTH_TAG);
                    mTileHeight = getIntAttribute(XMLTileSetConstants.TILE_HEIGHT_TAG);
                }
                else if (mXmlReader.NodeType == XmlNodeType.Element)
                {
                    if (mXmlReader.Name == XMLTileSetConstants.TILE_SET_TAG)
                    {
                        processTileSet();
                    }
                    else if (mXmlReader.Name == XMLTileSetConstants.LAYER_TAG)
                    {
                        processLayer();
                    }
                    else if (mXmlReader.Name == XMLTileSetConstants.OBJECT_GROUP_TAG)
                    {
                        processObjects();
                    }

                }

            }
        }

        private void processLayer()
        {
            string nameLayer = reader.GetAttribute(XMLTileSetConstants.NAME_TAG);
            int gridItem = 0;
            int gridItemCount = mWidth * mHeight;
            TileSetLayer tileSetLayer = addTileSetLayer(nameLayer, mFileName, mWidth, mHeight, mTileWidth, mTileHeight);
            while (reader.Read())
            {
                if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == XMLTileSetConstants.LAYER_TAG))
                    return;
                else if (reader.NodeType == XmlNodeType.Element)
                {
                    if ((reader.Name == XMLTileSetConstants.TILE_TAG) && (gridItem < gridItemCount))
                    {
                        int f = getIntAttribute(XMLTileSetConstants.GID_TAG);

                        //(0,0) (1,0) (2,0) (3,0) (0,1) (1,1) (2,1) (3,1) (0,2) (1,2) (2,2) (3,2) 
                        // 0     1     2      3     4
                        int id = f - mOffSetID;
                        if(id <= 0)
                            id = TileSet.INVALID_TILESET;
                        tileSetLayer.setTile(gridItem % mWidth, gridItem / mWidth, id );
                        gridItem++;
                    }
                    else if (reader.Name == XMLTileSetConstants.COLLISION_TAG)
                    {
                        processCollision(tileSetLayer);
                    }
                }
            }
        }

        private void processObjects()
        {
            while (reader.Read())
            {
                if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == XMLTileSetConstants.OBJECT_GROUP_TAG))
                    return;
                else if (reader.NodeType == XmlNodeType.Element)
                {
                    if ((reader.Name == XMLTileSetConstants.OBJECT_TAG))
                    {
                        GameObjectFactory factory = new GameObjectFactory();
                        factory.gameObjectName = reader.GetAttribute(XMLTileSetConstants.NAME_TAG);
                        factory.gameObjectType = reader.GetAttribute(XMLTileSetConstants.TYPE_TAG);
                        factory.setPosAndRectByRegion(processRegion());
                        while (reader.Read())
                        {
                            if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == XMLTileSetConstants.OBJECT_TAG))
                                break;
                            else if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == XMLTileSetConstants.PROPERTY_TAG))
                            {
                                string strPropName  = reader.GetAttribute(XMLTileSetConstants.NAME_TAG);
                                string strPropValue = reader.GetAttribute(XMLTileSetConstants.VALUE_TAG);
                                factory.addProperty(strPropName, strPropValue);
                            }
                        }
                        mListGameObjectFactories.Add(factory);
                    }
                }
            }
        }
        private RectFloat processRegion()
        {
            float top = getFloatAttribute(XMLTileSetConstants.Y_ATTRIB);
            float left = getFloatAttribute(XMLTileSetConstants.X_ATTRIB);
            float width = getFloatAttribute(XMLTileSetConstants.WIDTH_ATTRIB);
            float height= getFloatAttribute(XMLTileSetConstants.HEIGHT_ATTRIB);

            return new RectFloat(top, left, left + width, top + height);          
        }

        private float getFloatAttribute(string key)
        {
            try
            {
                return  util.StrTools.toFloat(reader.GetAttribute(key));
            }
            catch (FormatException)
            {
                return 0;
            }
        }

        private int getIntAttribute(string key)
        {
            int f = -1;
            int.TryParse(reader.GetAttribute(key), out f);
            return f;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace edde.maps
{
    internal static class XMLTileSetConstants
    {
        public static readonly string MAP_TAG = "map";
        public static readonly string TILE_SET_TAG = "tileset";

        public static readonly string OFFSET_TAG  = "firstgid";

        public static readonly string WIDTH_TAG = "width";
        public static readonly string HEIGHT_TAG = "height";

        public static readonly string TILE_WIDTH_TAG= "tilewidth";
        public static readonly string TILE_HEIGHT_TAG= "tileheight";

        public static readonly string NAME_TAG = "name";
        public static readonly string TYPE_TAG = "type";



        public static readonly string IMAGE_TAG = "image";

        public static readonly string SOURCER_TAG = "source";

        public static readonly string LAYER_TAG = "layer";

        public static readonly string OBJECT_GROUP_TAG = "objectgroup";

        public static readonly string OBJECT_TAG        = "object";
        public static readonly string PROPERTY_TAG      = "property";
        public static readonly string VALUE_TAG         = "value";



        public static readonly string DATA_TAG = "data";
        public static readonly string TILE_TAG = "tile";
        public static readonly string GID_TAG = "gid";

        public static readonly string TILE_RANGE_TAG = "tile_range";
        public static readonly string COLLISION_TAG = "collision";

        public static readonly string EVERY_TAG     = "every";
        public static readonly string NOTHING_TAG   = "nothing";

        public static readonly string MIN_ATTRIB = "min";
        public static readonly string MAX_ATTRIB = "max";

        public static readonly string X_ATTRIB = "x";
        public static readonly string Y_ATTRIB = "y";
        public static readonly string WIDTH_ATTRIB = "width";
        public static readonly string HEIGHT_ATTRIB = "height";
        


    }
}

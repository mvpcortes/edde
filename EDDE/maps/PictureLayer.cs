using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace edde.maps
{
    public class PictureLayer:ILayer
    {
        internal struct Picture
        {
            public Texture2D    mTexture;
            public Rectangle    mRect;
            public float        mZoom;

            public Picture(Texture2D tex, Point pos, REFERENCE reference, float zoom)
            {
                mTexture = tex;
                Point p = Point.Zero;
                switch (reference)
                {
                    case REFERENCE.TOP_LEFT:
                    case REFERENCE.TOP_CENTER:
                    case REFERENCE.TOP_RIGHT:
                        p.Y = 0;
                        break;

                    case REFERENCE.CENTER_LEFT:
                    case REFERENCE.CENTER_CENTER:
                    case REFERENCE.CENTER_RIGHT:
                        p.Y = -mTexture.Height / 2;
                        break;
                    case REFERENCE.BOTTOM_LEFT:
                    case REFERENCE.BOTTOM_CENTER:
                    case REFERENCE.BOTTOM_RIGHT:
                        p.Y = -mTexture.Height;
                        break;
                }

                switch (reference)
                {
                    case REFERENCE.TOP_LEFT:
                    case REFERENCE.CENTER_LEFT:
                    case REFERENCE.BOTTOM_LEFT:
                        p.X = 0;
                        break;

                    case REFERENCE.TOP_CENTER:
                    case REFERENCE.CENTER_CENTER:
                    case REFERENCE.BOTTOM_CENTER:
                        p.X = -mTexture.Width / 2;
                        break;
                    case REFERENCE.TOP_RIGHT:
                    case REFERENCE.CENTER_RIGHT:
                    case REFERENCE.BOTTOM_RIGHT:
                        p.X = -mTexture.Width;
                        break;
                }

                mRect.X = p.X + pos.X;
                mRect.Y = p.Y + pos.Y;
                mRect.Width = mTexture.Width;
                mRect.Height = mTexture.Height;

                mZoom = zoom;
            }

        }

        public enum REFERENCE
        {
            TOP_LEFT        = 0,
            TOP_CENTER      ,
            TOP_RIGHT       ,
            CENTER_LEFT     ,
            CENTER_CENTER   ,
            CENTER_RIGHT    ,
            BOTTOM_LEFT     ,
            BOTTOM_CENTER   ,
            BOTTOM_RIGHT    
        }

        private List<Picture> m_listPicture;

        public PictureLayer()
        {
            m_listPicture = new List<Picture>();
        }
        

        public void addPicture(string name, Point pos)
        {
            addPicture(name, pos, 1, REFERENCE.TOP_LEFT);
        }
        public void addPicture(string name, Point pos, float zoom, REFERENCE reference)
        {
            Texture2D tex = Resource.getTexture2DFirstColorTransparent(name);
            if (ReferenceEquals(tex, null)) return;

            Picture pic = new Picture(tex, pos, reference, zoom);
            m_listPicture.Add(pic);
        }

        public void removePicture(int pos)
        {
            m_listPicture.RemoveAt(pos);
        }



        #region ILayer Members
        void ILayer.draw(Rectangle rect, ICollection<GameObject> coll, GameTime gameTime)
        {
            SpriteBatch sb = RPG.me.spriteBatch;

            IEnumerator<GameObject> igo = coll.GetEnumerator(); igo.MoveNext();
            IEnumerator<Picture> ipic = m_listPicture.GetEnumerator(); ipic.MoveNext();

            bool hasGoNext = coll.Count > 0;
            bool hasPicNext = m_listPicture.Count > 0;

           // int intGo = 0, intPic= 0;
            while (hasGoNext && hasPicNext)
            {
             //   intGo++;
            //    intPic++;
                if (igo.Current.position.Y < ipic.Current.mRect.Bottom)
                {
                    igo.Current.draw(gameTime, ref rect);
                    hasGoNext = igo.MoveNext();
                }
                else
                {
                    drawPic( sb, rect, ipic.Current);
                    hasPicNext = ipic.MoveNext();                    
                }
            }

            while (hasGoNext)
            {
             //   intGo++;
                igo.Current.draw(gameTime, ref rect);
                hasGoNext = igo.MoveNext();
            }

            while (hasPicNext)
            {
             //   intPic++;
                drawPic(sb, rect, ipic.Current);
                hasPicNext = ipic.MoveNext();
            }

        }

        private void drawPic(SpriteBatch sb, Rectangle rect, Picture pic)
        {
            if (pic.mRect.Intersects(rect))
            {
            
                Vector2 pos = new Vector2(pic.mRect.Left - rect.Left, pic.mRect.Top - rect.Top);
                sb.Draw(pic.mTexture, pos, null, Color.White, 0, Vector2.Zero, pic.mZoom, SpriteEffects.None, 0);
            }
        }

        public void draw(Rectangle rect, GameTime gameTime)
        {
            SpriteBatch sb = RPG.me.spriteBatch;
            foreach (Picture pic in m_listPicture)
            {
                drawPic(sb, rect, pic);
            }
        }
        #endregion
    }
}

using System;
using System.Text;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace edde.maps
{
    /// <summary>
    /// Enumera��o que especifica as camadas de tileset exibidas no mapa
    /// </summary>
    public enum LAYER_ID
    {
        NONE = -1,   //its devil! ho ho ho! N�o grava nada
        FLOOR,            //posi��o do solo. O ch�o.
        ROOM,            //posi��o dos personagens. Normalmente nesta posi��o h� a colis�o
        UPROOM,         //posi��o de cima dentro de um c�modo. Como o topo de arm�rios
        //  ROOF,            //telhado, tudo que est� no telhado
        LENGTH,           //
        FIRST  = FLOOR
    }

    /// <summary>
    /// Interface para uma camada de Map
    /// </summary>
    public interface ILayer
    {
        void draw(Rectangle rect, GameTime gameTime);
        void draw(Rectangle rect, ICollection<GameObject> coll, GameTime gameTime);
    }

}

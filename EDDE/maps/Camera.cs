using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using edde;
using edde.maps;

namespace edde
{

    /// <summary>
    /// Implementa uma c�mera para um cen�rio do tipo Map.
    /// </summary>
    internal class Camera
    {
        /// <summary>
        /// O map que usa a C�mera
        /// </summary>
        private Map mMap;

        /// <summary>
        /// Posi��o relativa ao canto do mapa ou ao objeto em foco. Depende se mFocus est� especificado.
        /// </summary>
        private Vector2 mPos;

        /// <summary>
        /// rectVirtual indicando qual a �rea a ser plotada na tela
        /// </summary>
        private Rectangle mVirtualRect;

        /// <summary>
        /// O GameObject que est� focado pela c�mera. Caso n�o exista um GameObject focado, ele usa a posi��o 0,0 do mapa
        /// </summary>
        private GameObject mFocus;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="map">O mapa sendo observado</param>
        public Camera(Map map)
        {
            mMap = map;
            mFocus = null;
            setVirtualRect(RPG.me.screenWidth, RPG.me.screenHeight);
        }

        /// <summary>
        /// Gera um Ret�ngulo de visualiza��o. Isto � usado para determinar a janela de visualiza��o do Map.
        /// </summary>
        /// <returns></returns>
        public Rectangle processRect()
        {
            Rectangle rect = mVirtualRect;// new Rectangle(0, 0, RPG.me.screenWidth(), RPG.me.screenHeight());

            bool bigCameraX = rect.Width > mMap.width;
            bool bigCameraY = rect.Height > mMap.height;

            Vector2 vecFocus = mFocus == null ? Vector2.Zero : mFocus.position;

            if (bigCameraX)
            {
            }
            else
            {
                rect.X = MyMath.round(vecFocus.X - (rect.Width / 2));
                rect.X   = Math.Max(0, rect.Left);
                if (rect.Right > mMap.width)
                    rect.X = mMap.width - rect.Width;
            }

            if (bigCameraY)
            {
            }
            else
            {
                rect.Y   = MyMath.round(vecFocus.Y - (rect.Height/2));
                rect.Y   = Math.Max(0, rect.Top);
                if (rect.Bottom > mMap.height)
                    rect.Y = mMap.height - rect.Height;
            }            
      
            return rect;
        }

        /// <summary>
        /// Coloca foco em um GameObject
        /// </summary>
        /// <param name="focus"></param>
        public void followIt(GameObject focus)
        {
            mFocus = focus;
        }

        /// <summary>
        /// Retira o foco do GameObject focado atualmente e torna null. Isto �, ele passa a focar a posi��o 0,0 do mapa.
        /// </summary>
        /// <returns>O GameObject anteriormente focado</returns>
        public GameObject unFollowIt()
        {
            GameObject focus = mFocus;
            mFocus = null;
            return focus;
        }

        /// <summary>
        /// Atribui uma posi��o relativa ao GameObjectFocado.
        /// <remarks>
        /// O sistema usa esta posi��o para deslocar a visualiza��o em rela��o ao GameObjectFocado.
        /// </remarks>
        /// </summary>
        /// <param name="pos"></param>
        public void setRelativePos(Vector2 pos)
        {
            mPos = pos;
        }

        /// <summary>
        /// Retorna a posi��o de deslocamento da c�mera
        /// </summary>
        /// <returns></returns>
        public Vector2 getPos()
        {
            return mPos;
        }

        /// <summary>
        /// Atribui o tamanho da c�mera
        /// </summary>
        public void setVirtualRect(int width, int height)
        {
            mVirtualRect = new Rectangle(0,0,width, height);
        }
    }
}

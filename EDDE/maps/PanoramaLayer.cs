using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace edde.maps
{
    public class PanoramaLayer: ILayer
    {
        PictureLayer.Picture mPicture;

        public PanoramaLayer(string name, Point pos):this(name, pos, PictureLayer.REFERENCE.TOP_LEFT){}

        public PanoramaLayer(string name, Point pos, PictureLayer.REFERENCE reference)
        {
            Texture2D mTexture = Resource.getTexture2DFirstColorTransparent(name);
            mPicture = new PictureLayer.Picture(mTexture, pos, reference, 1);
        }

        #region ILayer Members

        public void draw(Microsoft.Xna.Framework.Rectangle rect, Microsoft.Xna.Framework.GameTime gameTime)
        {
            SpriteBatch sb = RPG.me.spriteBatch;

            Point cellInit, cellFinal;
            cellInit.X = rect.Left/mPicture.mRect.Width;
            cellInit.Y = rect.Top/mPicture.mRect.Height;

            cellFinal.X = rect.Right/mPicture.mRect.Width;
            cellFinal.Y = rect.Bottom/mPicture.mRect.Height;

            cellFinal.X += 1;
            cellFinal.Y += 1;

            for(int i = cellInit.X; i< cellFinal.X; i++)
                for(int j = cellInit.Y; j< cellFinal.Y; j++)
                {
                    Rectangle rectPic = new Rectangle(cellInit.X * mPicture.mRect.Left* i, cellInit.Y * mPicture.mRect.Top * j, mPicture.mRect.Width, mPicture.mRect.Height);
                    rect.Intersects(rectPic);
                    sb.Draw(mPicture.mTexture, rectPic, Color.White);
                }
        }

        public void draw(Microsoft.Xna.Framework.Rectangle rect, ICollection<GameObject> coll, Microsoft.Xna.Framework.GameTime gameTime)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace edde
{

    /// <summary>
    /// Determina uma exce��o do sistema de Inje��o
    /// </summary>
    class InjectionException: eddeException
    {
        /// <summary>
        /// O GameObject de origem da exce��o
        /// </summary>
        private GameObject mSource;

        /// <summary>
        /// Retorna o GameObject de origem da exce��o
        /// </summary>
        public GameObject source
        {
            get { return mSource; }
        }

        public InjectionException(GameObject source, string msg):base(msg)
        {
            mSource = source;
        }

        public override string ToString()
        {
            return "Exce��o no processo de inje��o do GameObject: " + source.ToString() + "\n" + base.ToString();
        }
    }
}

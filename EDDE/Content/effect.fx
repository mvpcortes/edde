float4x4 World;
float4x4 View;
float4x4 Projection;

sampler2D textureSampler;

texture scaleTexture;

sampler scale = sampler_state
{
   Texture = (scaleTexture);
   ADDRESSU = MIRROR;
   ADDRESSV = MIRROR;
   MAGFILTER = LINEAR;
   MINFILTER = LINEAR;
   MIPFILTER = LINEAR;
};

struct VertexInput
{
    float4 Position : POSITION0;

    // TODO: add input channels such as texture
    // coordinates and vertex colors here.
};

struct VertexOutput
{
    float4 Position : POSITION0;

    // TODO: add vertex shader outputs such as colors and texture
    // coordinates here. These values will automatically be interpolated
    // over the triangle, and provided as input to your pixel shader.
};

struct PixelInput // Rasterization Data
{
	float4 Color : COLOR0;
	float4 coord : TEXCOORD0;
};

struct PixelOutput // Pixel Output Data
{
	float4 Color : COLOR0;
	float Depth : DEPTH;
};


VertexOutput VertexShaderFunction(VertexInput input)
{
    VertexOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);

    // TODO: add your vertex shader code here.

    return output;
}

#define incDistance 0.001
#define epsilon_zero 0.0001
   
float4 PixelFunction(PixelInput input) : COLOR0
{
    float4 color = 0;
    
    //color += tex2D(textureSampler, input.coord-incDistance);
    //color += tex2D(textureSampler, input.coord+incDistance);
	color += tex2D(textureSampler,  float2(input.coord.x-incDistance, input.coord.y-incDistance));
	color += tex2D(textureSampler, float2(input.coord.x-incDistance, input.coord.y));
	color += tex2D(textureSampler, float2(input.coord.x-incDistance, input.coord.y+incDistance));
	
	
	color += tex2D(textureSampler, float2(input.coord.x, input.coord.y-incDistance));
	//color += tex2D(textureSampler, float2(input.coord.x, input.coord.y))/8;
	color += tex2D(textureSampler, float2(input.coord.x, input.coord.y+incDistance));
	
	color += tex2D(textureSampler, float2(input.coord.x+incDistance, input.coord.y-incDistance));
	color += tex2D(textureSampler, float2(input.coord.x+incDistance, input.coord.y));
	color += tex2D(textureSampler, float2(input.coord.x+incDistance, input.coord.y+incDistance));
	
	color = color/8;
    return color;
}

float4 RGBToHSV(float4 color)
{
	float r, g, b, delta;
	float colorMax, colorMin;
	float h=0, s=0, v=0;
	float4 hsv=0;
	colorMax = max (color.r,max(color.g,color.b));
	colorMin = min (color.r,max(color.g, color.b));
	v = colorMax; // this is value
	
	if (abs(colorMax) < epsilon_zero) 
	{ 
		s = (colorMax - colorMin) / colorMax;
	}
	
	if (abs(s) < epsilon_zero) // if not achromatic
	{
		delta = colorMax - colorMin;
		if (color.r == colorMax)
			h = (color.g-color.b)/delta;
		else if (color.g == colorMax)
			h = 2.0 + (color.b-color.r) / delta;
		else
			h = 4.0 + (color.r-color.g)/delta;
			
	
		h *= 60;
		if( h < 0)
		{ 
			h +=360;
		} 
		
		hsv[0] = h / 360.0; // moving h to be between 0 and 1.
		hsv[1] = s;
		hsv[2] = v;
		hsv[3] = color.a;
	}
	
	return hsv;
}
 
float2 sampleOffsets[8] : register (c10);

float4 comic(PixelInput input) : COLOR0
{
	float4 original = tex2D(textureSampler, input.coord);
	float alfa = original.a;
	float4 result;
	float4 nova;
	result = 1 - original;
	for(int i = 0; i < 3; i++)
	{
		nova = tex2D(textureSampler, sampleOffsets[i] + input.coord);
		{
			result += nova;
		}
	}	
	
	result = result/8;
	//result = round(result*255)/255;
	result.a = alfa;
	return result;
	
	/*
	nova = tex2D(textureSampler,  float2(input.coord.x-incDistance, input.coord.y-incDistance));
	if(distance(nova, original) < distance(result, original)) result = nova;
	
	nova = tex2D(textureSampler, float2(input.coord.x-incDistance, input.coord.y));
	if(distance(nova, original) < distance(result, original)) result = nova;
	nova = tex2D(textureSampler, float2(input.coord.x-incDistance, input.coord.y+incDistance));
	if(distance(nova, original) < distance(result, original)) result = nova;
	
	nova = tex2D(textureSampler, float2(input.coord.x, input.coord.y-incDistance));
	if(distance(nova, original) < distance(result, original)) result = nova;
	nova = tex2D(textureSampler, float2(input.coord.x, input.coord.y+incDistance));
	if(distance(nova, original) < distance(result, original)) result = nova;
	
	nova = tex2D(textureSampler, float2(input.coord.x+incDistance, input.coord.y-incDistance));
	if(distance(nova, original) < distance(result, original)) result = nova;
	
	nova = tex2D(textureSampler, float2(input.coord.x+incDistance, input.coord.y));
	if(distance(nova, original) < distance(result, original)) result = nova;
	
	nova = tex2D(textureSampler, float2(input.coord.x+incDistance, input.coord.y+incDistance));
	if(distance(nova, original) < distance(result, original)) result = nova;
	*/
	
/*
	float4 color = tex2D(textureSampler, input.coord);
	float4 menor = float4(1,1,1,1) - color;
	float menorDistance = 1;
	float inc = 0.1;
	for(float i = 0; i <= 1; i+= inc)
	{
		float4 temp = tex2D(scale, float2(i, 1));
		float dTemp = distance(temp, color);
		if(dTemp < menorDistance)
		{
			menorDistance = dTemp;
			menor = temp;
		}
	};
	
	
	return menor;*/
/*
	float4 color = tex2D(textureSampler, input.coord);
	float pos = RGBToHSV(color).x;
	float a = color.a;
	color = color/2;
	color += tex2D(scale, float2(pos, 1))/2;
	color.a = a;
	return color; 
	*/
	
    /*float4 color = 0;//tex2D(textureSampler, input.coord);
    color += tex2D(textureSampler,  float2(input.coord.x-incDistance, input.coord.y-incDistance));
    color += tex2D(textureSampler, float2(input.coord.x-incDistance, input.coord.y+incDistance));
    color += tex2D(textureSampler, float2(input.coord.x+incDistance, input.coord.y-incDistance));
    color += tex2D(textureSampler, float2(input.coord.x+incDistance, input.coord.y+incDistance));
    color = sin(color/4);
    return color;
	
	color[0] = tex2D(textureSampler,  float2(input.coord.x-incDistance, input.coord.y-incDistance));
	color[1] = tex2D(textureSampler, float2(input.coord.x-incDistance, input.coord.y));
	color[2] = tex2D(textureSampler, float2(input.coord.x-incDistance, input.coord.y+incDistance));
	
	
	color[3] = tex2D(textureSampler, float2(input.coord.x, input.coord.y-incDistance));
	//color += tex2D(textureSampler, float2(input.coord.x, input.coord.y))/8;
	color[4] = tex2D(textureSampler, float2(input.coord.x, input.coord.y+incDistance));
	
	color[5] = tex2D(textureSampler, float2(input.coord.x+incDistance, input.coord.y-incDistance));
	color[6] = tex2D(textureSampler, float2(input.coord.x+incDistance, input.coord.y));
	color[7] = tex2D(textureSampler, float2(input.coord.x+incDistance, input.coord.y+incDistance));
	*/
	
   //return color;
}

technique Default
{
    pass Pass1
    {
        // TODO: set renderstates here.

        //VertexShader = compile vs_1_1 VertexShaderFunction();
        PixelShader = compile ps_2_0 comic();
    }
}

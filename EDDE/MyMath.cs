using Microsoft.Xna.Framework;
using System;

namespace edde
{
    /// <summary>
    /// classe abstrata com algumas facilidades matem�ticas
    /// </summary>
    public static class MyMath
    {
        /// <summary>
        /// Vetor apontando para cima.
        /// </summary>
        static public Vector2 VEC_UP    = new Vector2( 0f, -1f);

        /// <summary>
        /// Vetor apontando para esquerda.
        /// </summary>
        static public Vector2 VEC_LEFT  = new Vector2( 1f,  0f);

        /// <summary>
        /// Vetor apontando para direita.
        /// </summary>
        static public Vector2 VEC_RIGHT = new Vector2(-1f,  0f);

        /// <summary>
        /// Vetor apontando para baixo.
        /// </summary>
        static public Vector2 VEC_DOWN  = new Vector2( 0f,  1f);


        /// <summary>
        /// Arredonda um ponto flutuante para um int
        /// </summary>
        /// <param name="value">ponto flutuante a ser arredondado</param>
        /// <returns>Valor inteiro arredondado</returns>
        static public int round(float value)
        {
            return (int)Math.Round(value);
        }

        /// <summary>
        /// arredonda um vetor em ponto flutuante (Vector2) para um vetor em inteiro (Point)
        /// </summary>
        /// <param name="vec">Vetor em ponto flutuante</param>
        /// <returns>Vetor em inteiro</returns>
        static public Point toPoint(Vector2 vec)
        {
            return new Point(round(vec.X), round(vec.Y));
        }


        /// <summary>
        /// retorna o �ngulo entre dois vetores.
        /// </summary>
        /// <param name="source">Vetor de origem</param>
        /// <param name="vec">Vetor de destino</param>
        /// <returns>�ngulo achado</returns>
        static public float angle(Vector2 source, Vector2 vec)
        {
            float p;
            Vector2.Dot(ref source, ref vec, out p);
            float length1 = source.Length();
            float length2 = vec.Length();
            p = p / (length1 * length2);
            return (float)System.Math.Acos(p);
        }

        static public Vector2 rotate(Vector2 source, float angle)
        {
            return new Vector2(source.X * (float)Math.Cos(angle) - source.Y * (float)Math.Sin(angle), +source.X * (float)Math.Sin(angle) + source.Y * (float)Math.Cos(angle));
        }


        /// <summary>
        /// inst�ncia de sistema de randomiza��o
        /// </summary>
        private static Random mRandom = new Random();

        public static int getRandom()
        {
            return mRandom.Next();
        }

        /// <summary>
        /// Retorna um n�mero entre [0,max)
        /// </summary>
        /// <param name="max">Valor m�ximo que pode gerar o random (exclusive)</param>
        /// <returns>O n�mero gerado aleatoriamente</returns>
        public static int getRandom(int max)
        {
            return mRandom.Next() % max;
        }

        /// <summary>
        /// Retorna um n�mero randomico entre [0,max) (em float)
        /// </summary>
        /// <param name="max">O valor m�ximo</param>
        /// <returns>O valor rand�mico</returns>
        public static float getRandom(float max)
        {
            return (float)mRandom.NextDouble() % max;
        }

        /// <summary>
        /// Retorna um n�mero randomico em float.
        /// </summary>
        /// <returns></returns>
        public static float getRandomFloat()
        {
            return (float)mRandom.NextDouble() * float.MaxValue;
        }

        //public static float getRandomFloat(float max)
        //{
        //    return (float)(mRandom.NextDouble()) * max;
        //}

        /// <summary>
        /// Gera uma dire��o r�ndomica. O vetor � normalizado.
        /// </summary>
        /// <returns>A dire��o rand�mica</returns>
        public static Vector2 getRandomDir()
        { 
            return new Vector2((float)(mRandom.NextDouble() * 2.0) - 1f, (float)(mRandom.NextDouble() * 2.0) - 1f);
        }

        /// <summary>
        /// Retorna uma posi��o rand�mica no mapa, com raio de [0,w) e [0,h) (em ponto flutuante)
        /// </summary>
        /// <param name="w">A largura m�xima</param>
        /// <param name="h">A altura m�xima</param>
        /// <returns></returns>
        public static Vector2 getRandomPos(float w, float h)
        {
            return new Vector2((float)(mRandom.NextDouble() * w), (float)(mRandom.NextDouble() *h));
        }

        /// <summary>
        /// Retorna uma posi��o rand�mica no mapa, com raio de [0,w) e [0,h) (Em inteiro)
        /// </summary>
        /// <param name="w">A largura m�xima</param>
        /// <param name="h">A altura m�xima</param>
        /// <returns></returns>
        public static Point getRandomPos(int w, int h)
        {
            return new Point(mRandom.Next() % w, mRandom.Next() % h);
        }

        /// <summary>
        /// Retorna um valor true ou false randomico
        /// </summary>
        /// <returns></returns>
        public static bool getRandomBool()
        {
            return (mRandom.Next(0, 2) == 1) ? true : false;
        }


        public static bool equal(Vector2 vecA, Vector2 vecB, float epsilon)
        {
            return (vecA - vecB).Length() < epsilon;
        }

        public static bool equal(Vector2 vecA, Vector2 vecB)
        {
            return MyMath.equal(vecA, vecB, 1e-8f);
        }
    }
}

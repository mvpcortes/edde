using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;
using edde.physicalmanager;

namespace edde
{

    /// <summary>
    /// Representa um cen�rio no jogo
    /// </summary>
    public abstract class Scene
    {
     	 
        /// <summary>
        /// Nome do cen�rio
        /// </summary>
	    private string mName;
     	 
        /// <summary>
        /// Refer�ncia a inst�ncia do RPG
        /// </summary>
	    private RPG mRPG;


        /// <summary>
        /// Lista de objetos inseridos no cen�rio
        /// </summary>
        private List<GameObject> mListObjects;


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="rPG">inst�ncia do RPG</param>
        /// <param name="name">Nome do cen�rio</param>
        public Scene(RPG rPG, string name)
        {
            mRPG = rPG;
            mName = name;
            mListObjects    = new List<GameObject>();
        }

        /// <summary>
        /// Retorna o nome do cen�rio
        /// </summary>
        /// <returns></returns>
        public string getName() { return mName; }

        /// <summary>
        /// Atribui o nome ao cen�rio
        /// </summary>
        /// <param name="name"></param>
        public void setName(string name) { mName = name; }



        /// <summary>
        /// M�todo de updade do GameComponent. Apenas chama os updates dos GammeObjects
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Update(GameTime gameTime)
        {
            foreach (GameObject go in mListObjects)
                go.update(gameTime);
        }

        /// <summary>
        /// Apenas chama os draws dos GameObjects baseado no tamanho da tela.
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Draw(GameTime gameTime)
        {
            Rectangle rect = new Rectangle(0, 0, RPG.me.screenWidth, RPG.me.screenHeight);

            foreach (GameObject go in mListObjects)
                go.draw(gameTime, ref rect);
        }


        /// <summary>
        /// Inicializa somente os GameObjects
        /// </summary>
        public void init()
        {
            foreach (GameObject go in mListObjects)
                go.init(this);
        }

        //==================================================================
        /// <summary>
        /// Retorna se a cena contabiliza tempo de jogo. O default � false.
        /// </summary>
        /// <returns>Se contabiliza ou n�o tempo de jogo.</returns>
        internal bool countPlayTime(){return false;}

        //==================================================================
        //m�todos para manipula��o de GameObjects

        /// <summary>
        /// Adiciona um GameObject
        /// </summary>
        /// <param name="obj"></param>
        public virtual void addObject(GameObject obj)
        {
            if (!ReferenceEquals(obj, null))
            {
                mListObjects.Add(obj);
            }
        }

        /// <summary>
        /// retorna uma lista com os GameObjects
        /// </summary>
        /// <returns>Lista de GameObjects</returns>
        public List<GameObject> getGameObjects()
        {
            return mListObjects;
        }

        /// <summary>
        /// Retorna todos os GameObjects da classe T
        /// </summary>
        /// <typeparam name="T">A classe buscada</typeparam>
        /// <param name="coll">o container que receber� os GameObjects</param>
        public void getGameObjects<T>(ICollection<T> coll) where T : GameObject
        {
            foreach (GameObject g in mListObjects)
            {
                if (g is T)
                    coll.Add((T)g);
            }
        }

        /// <summary>
        /// Retorna o primeiro GameObject do tipo type (type � uma classe derivada de GameObject
        /// </summary>
        /// <param name="type">O tipo a ser buscado</param>
        /// <returns>O GameObject</returns>
        public GameObject getGameObject(Type type)
        {
            foreach (GameObject g in mListObjects)
            {
                if (g.GetType() == type)
                    return g;
            }
            return null;
        }


        /// <summary>
        /// Retorna o primeiro GameObject do tipo T
        /// </summary>
        /// <typeparam name="T">O tipo do GameObject buscado</typeparam>
        /// <returns>O GameOBject encontrado ou null se n�o encontrar</returns>
        public T getGameObject<T>() where T: GameObject
        {
            foreach (GameObject g in mListObjects)
            {
                if (g.GetType() == typeof(T))
                    return (T)g;
            }
            return null;
        }

        /// <summary>
        /// Verifica se o cen�rio cont�m o GameObject buscado
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public bool containsGameObject(GameObject go)
        {
            return mListObjects.Contains(go);
        }

        /// <summary>
        /// Remove um GameObject
        /// </summary>
        /// <param name="go"></param>
        public virtual void removeGameObject(GameObject go)
        {
            mListObjects.Remove(go);
        }

        /// <summary>
        /// Termina a execu��o de um cen�rio sem guardas as informa��es atuais. Finaliza os GameObjects
        /// </summary>
        public virtual void finish()
        {
            foreach (GameObject go in getGameObjects())
            {
                go.finish(this);
            }
        }


        /// <summary>
        /// Retorna o n�mero de GameObjects no cen�rio
        /// </summary>
        /// <returns>N�mero de GameObjects</returns>
        public int gameObjectCount()
        {
            return mListObjects.Count;
        }


        /// <summary>
        /// Retorna todos os GameObjetos contidos em uma regi�o
        /// </summary>
        /// <param name="center">O centro da regi�o</param>
        /// <param name="range">O raio da regi�o (c�rculo)</param>
        /// <param name="collItems">O container que receber� os GameObjects</param>
        public abstract void getGameObjectInRegion(Vector2 center, float range, ICollection<ICollised> collItems);


    }
}

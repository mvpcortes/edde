using System;
using System.Collections.Generic;
using System.Text;
using edde.physicalmanager;

namespace edde
{
    public class Wall : edde.physicalmanager.ICollised
    {
        private RectFloat mRect;

        Wall(RectFloat rect)
        {
            mRect = rect;
        }

        Wall(float top, float left, float right, float bottom):this(new RectFloat(top, left, right, bottom)){}

        static public void insertRoomWalls(ICollection<Wall> collWall, RectFloat rect)
        { 

            collWall.Add(new Wall(rect.top,  rect.left, rect.right, rect.top+10));
            collWall.Add(new Wall(rect.top,  rect.left, rect.left+10, rect.bottom));
            collWall.Add(new Wall(rect.top,  rect.right-10, rect.right, rect.bottom));
            collWall.Add(new Wall(rect.bottom-10, rect.left, rect.right, rect.bottom));
        }

        #region ICollised Member;

        public COLLISION_TYPE getTypeCollision()
        {
            return COLLISION_TYPE.WALL;
        }

        public RectFloat getRectBase()
        {
            return mRect;
        }

        #endregion

        #region ICollised Members


        public bool intersects(RectFloat rect)
        {
            return Collision.intersects(mRect, rect);
        }

        #endregion
    }

}

using System;
using System.Collections.Generic;
using System.Text;
using edde;
using Microsoft.Xna.Framework;

namespace edde.components
{
    public class RandomMoviment: SoftComponent
    {
        private GameObject mParent;

        private bool mEnabled = true;

        private TimeSpan mTimeChangeDirection, mTimeLastChange;

        float mIncValue;

        private const float MAX_TIME_CHANGE_DIRECTION = 5f;

        /// <summary>
        /// velocidade
        /// </summary>
        public float incValue
        {
            get { return mIncValue; }
            set
            {
                mIncValue = value;
            }
        }

        public RandomMoviment(float incValue/*, TimeSpan timeChange*/)
        {
            mTimeLastChange = TimeSpan.Zero;
            mIncValue = incValue;
            mTimeChangeDirection = TimeSpan.FromSeconds(MyMath.getRandom(MAX_TIME_CHANGE_DIRECTION));
        }

        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Rectangle rectWindow)
        {
            //faznada
        }

        public void update(Microsoft.Xna.Framework.GameTime time)
        {
            Vector2 vec;
            if (time.TotalGameTime - mTimeLastChange > mTimeChangeDirection)
            {
                mTimeLastChange = time.TotalGameTime;

                vec = generateDir();
                mParent.setDirection(vec);
                mTimeChangeDirection = TimeSpan.FromSeconds(MyMath.getRandom(MAX_TIME_CHANGE_DIRECTION));
            }
            vec = mParent.getDirection();
            vec *= mIncValue;
            mParent.incPosition(vec);
        }

        private Vector2 generateDir()
        {
            return MyMath.getRandomDir();
            //int arrow = MyMath.getRandom(mTotalFrequency);
            //int posVec = 0;
            //for (int i = 0; i <= arrow && posVec < mListFequencyDirection.Count;)
            //{
            //    i += mListFequencyDirection[posVec].frequency;
            //    posVec++;
            //}

            //if (posVec < mListFequencyDirection.Count)
            //{
            //    if (mListFequencyDirection[posVec].direction != Vector2.Zero)
            //        return mListFequencyDirection[posVec].direction;
            //    else
            //        return MyMath.getRandomDir();
            //}
            //else
            //{
            //    return MyMath.getRandomDir();
            //}

        }

        public bool init(GameObject parent)
        {
            mParent = parent;
            mParent.moving = enable;
            return true;
        }

        public void ejected()
        {
            //throw new Exception("The method or operation is not implemented.");
        }

        public bool enable
        {
            get 
            {
                return mEnabled;
            }
            set 
            { 
                mEnabled = value;
                if (!ReferenceEquals(mParent, null)) mParent.moving = value;
            }
        }
        #endregion

        #region SoftComponent Members


        public bool ejectMe()
        {
            return false;
        }

        #endregion
    }
}

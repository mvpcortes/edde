using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
namespace edde.components
{
    /// <summary>
    /// Componente que representa um Timer gen�rico
    /// <remarks>
    /// Esta classe � bem gen�rica, e serve para qualquer situa��o que precise-se detectar um tempo futuro e realizar uma a��o
    /// </remarks>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Timer<T>: SoftComponent where T: GameObject
    {
        /// <summary>
        /// Determina um Delegate (function) que � acionada durante o evento do tempo
        /// </summary>
        /// <param name="timeParam"></param>
        /// <param name="parent"></param>
        public delegate void TimeHandle(TimeParam timeParam, T parent);

        /// <summary>
        /// Classe que representa a configura��o de um gatilho para o tempo
        /// </summary>
        public class TimeParam: IComparable<TimeParam>
        {
            public bool loop = false;
            public TimeSpan delay;
            public TimeSpan init;
            public TimeSpan realFinal;
            public TimeSpan forSeenFinal
            {
                get { return init + delay; }
            }
            /// <summary>
            /// Par�metro do evento de tempo
            /// </summary>
            private object mParam; 

            /// <summary>
            /// Objeto que parametriza a mensagem de t�rmino do tempo
            /// </summary>
            public object objParam
            {
                get{return mParam;}
            }
            /// <summary>
            /// evento disparado quando o timer acaba
            /// </summary>
            private event TimeHandle handle;

            /// <summary>
            /// Insere um handle para evento de tempo
            /// </summary>
            /// <param name="_handle"></param>
            internal void addHandle(TimeHandle _handle)
            {
                handle+= _handle;
            }

            /// <summary>
            /// Realiza o disparo de um evento
            /// </summary>
            /// <param name="param"></param>
            /// <param name="parent"></param>
            internal void handleIt(TimeParam param, T parent)
            {
                handle(param, parent);
            }

            /// <summary>
            /// retorna os handles associados
            /// </summary>
            /// <returns></returns>
            internal TimeHandle[] getHandles()
            {
                return (TimeHandle[])handle.GetInvocationList();
            }


            /// <summary>
            /// Construtor de um Rime param
            /// </summary>
            /// <param name="_delay">O tempo futuro que ser� disparado o evento a partir de um tempo inicial _init</param>
            /// <param name="objParam">O objeto de par�metro do evento</param>
            /// <param name="_init">O tempo inicial de contagem do timer</param>
            internal TimeParam(ref TimeSpan _delay, object objParam, ref TimeSpan _init)
            {
                delay = _delay;
                init  = _init;
                mParam = objParam;
                realFinal = TimeSpan.MaxValue;
            }

            #region IComparable<TimeParam> Members

            public int CompareTo(TimeParam other)
            {
                return forSeenFinal.CompareTo(other.forSeenFinal);
            }

            #endregion
        }



        #region fields
        
        private util.PriorityList<TimeParam> mListTimer = new util.PriorityList<TimeParam>();
        
        private TimeSpan NextValue
        {
            get 
            {
                if (mListTimer.Count > 0)
                    return mListTimer.First().forSeenFinal;
                else
                    return TimeSpan.MaxValue;
                }
        }

        private T mParent = default(T);

        public void addTimer(TimeSpan delay, params TimeHandle[] handle)
        {
            addTimer(delay, TimeSpan.Zero, handle);
        }

        public void addTimer(TimeSpan delay, TimeSpan init, params TimeHandle[] handle)
        {
            addTimer(delay, init, null, handle);
        }

        public void addTimer(TimeSpan delay, TimeSpan init, object param, params TimeHandle[] handle)
        {
            TimeParam time = new TimeParam(ref delay, param, ref init);

            foreach(TimeHandle han in handle)
            {
                time.addHandle(han);
            }
            mListTimer.Add(time);

        }

        public void addTimerLoop(TimeSpan delay, params TimeHandle[] handle)
        {
            addTimerLoop(delay, TimeSpan.Zero, handle);
        }

        public void addTimerLoop(TimeSpan delay, TimeSpan init, params TimeHandle[] handle)
        {
            addTimerLoop(delay, init, null, handle);
        }

        public void addTimerLoop(TimeSpan delay, TimeSpan init, object param, params TimeHandle[] handle)
        {
            TimeParam time = new TimeParam(ref delay, param, ref init);
            time.loop = true;

            foreach (TimeHandle han in handle)
            {
                time.addHandle(han);
            }
            mListTimer.Add(time);

        }
        #endregion

        #region SoftComponent Members

        public void update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            TimeSpan time = gameTime.TotalGameTime;
            if (mListTimer.Count <= 0) return;

            while (mListTimer.First().init.Equals(TimeSpan.Zero))
            {
                if (time.Equals(TimeSpan.Zero))
                    time = TimeSpan.FromTicks(1);
                else
                {
                    TimeParam tp = mListTimer.PopFirst();
                    tp.init = time;
                    mListTimer.Add(tp);
                }
            }


            while((mListTimer.Count > 0) && (NextValue < time))
            {
                TimeParam timeParam = mListTimer.PopFirst();

                timeParam.realFinal = time;
                timeParam.handleIt(timeParam, mParent);

                if (timeParam.loop)
                {
                    timeParam.init = timeParam.realFinal;
                    timeParam.realFinal = TimeSpan.MaxValue;

                    mListTimer.Add(timeParam);
                }
            }
        }

        public bool init(GameObject parent)
        {
            mParent = (T)parent;
            return true;
           //faznada
        }

        public void ejected()
        {
            //evento de ejetado
        }

        public bool ejectMe()
        {
            return false;
        }

        public bool enable
        {
            get
            {
                return true;
            }
          
        }

     

        public void draw(GameTime time, ref Rectangle rectWindow)
        {
            //faznada
        }

        #endregion
    }
}

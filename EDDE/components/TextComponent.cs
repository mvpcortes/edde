﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace edde.components
{
    /// <summary>
    /// Componente que exibe um texto relativo a um GameObject
    /// </summary>
    public class TextComponent: SoftComponent
    {
        #region fields
        SpriteBatch mSpriteBatch;

        /// <summary>
        /// GameObject que contém o componente
        /// </summary>
        GameObject mParent;

        /// <summary>
        /// Fonte
        /// </summary>
        private SpriteFont mFont;

        /// <summary>
        /// Nome da fonte
        /// </summary>
        private string mStrFont;

        /// <summary>
        /// O texto a ser exibido
        /// </summary>
        private string mStrText = "";

        /// <summary>
        /// Cor de alteração da fonte.
        /// </summary>
        private Color mColor = Color.White;

        /// <summary>
        /// Determina a escala/zoom da fonte
        /// </summary>
        private float mScale = 1f;

        /// <summary>
        /// Coordenadas relativas a origem do GameObject;
        /// </summary>
        private Vector2 mPosCoor = Vector2.Zero;

        #endregion

        #region properties

        public Vector2 posCoor
        {
            get { return mPosCoor; }
            set { mPosCoor = value; }

            
        }
        private SpriteBatch spriteBatch
        {
            get { return mSpriteBatch; }
        }

        public string strFont
        {
            set
            {
                mStrFont = value;
                mFont = Resource.getFont(mStrFont);
            }
        }

        public string text
        {
            get { return mStrText; }
            set { mStrText = value; }
        }

        public Color color
        {
            get { return mColor; }
            set { mColor = value; }
        }

        public float scale
        {
            get { return mScale; }
            set { mScale = value; }
        }

        protected GameObject parent
        {
            get{return mParent;}
        }

        #endregion

        public TextComponent(string strFont)
        {
            mStrFont = strFont;
        }

        
        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
        {
            Vector2 pos = posCoor + parent.position - new Vector2(rectWindow.X, rectWindow.Y);

            spriteBatch.DrawString(mFont, text, pos, color, 0, Vector2.Zero, scale, SpriteEffects.None, 0);
        }

        public virtual void update(Microsoft.Xna.Framework.GameTime time)
        {
        }

        public virtual bool init(GameObject parent)
        {
            mParent = parent;
            strFont = mStrFont;
            mSpriteBatch = RPG.me.spriteBatch;
            return true;
        }

        public void ejected()
        {
           
        }

        public bool ejectMe()
        {
            return false;
        }

        public bool enable
        {
            get { return true; }
        }

        #endregion
    }
}

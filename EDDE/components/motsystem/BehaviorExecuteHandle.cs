using System;
using System.Collections.Generic;
using System.Text;

namespace edde.components.motsystem
{
    public class BehaviorExecuteHandle
    {
        
        /// <summary>
        /// Nome do Comportamento a ser buscado para ser atribuido a vari�vel.
        /// </summary>
        private string mName;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_name">o nome do Comportamento a ser buscado</param>
        public BehaviorExecuteHandle(string _name)
        {
            mName = _name;
        }

        public string name
        {
            get{return mName;}
            set{mName = value;}
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace edde.components
{
    /// <summary>
    /// Exce��es geradas por SoftComponent
    /// </summary>
    public class SoftComponentException: eddeException
    {

        /// <summary>
        /// SoftComponent de origem
        /// </summary>
        public SoftComponent mSource;


        /// <summary>
        /// SoftComponent de origem
        /// </summary>
        public SoftComponent source
        {
            get
            {
                return mSource;
            }

        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="problem">Descri��o do problema</param>
        /// <param name="sc">SoftComponent de origem</param>
        /// <param name="region">Qual regi�o do GameLoop gerou o problema</param>
        public SoftComponentException(string problem, SoftComponent sc, REGION_LOOP region):base(problem, region)
        {
            mSource = sc;
        }


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="problem">Descri��o do problema</param>
        /// <param name="sc">SoftComponent de origem</param>
        public SoftComponentException(string problem, SoftComponent sc)
            : base(problem, REGION_LOOP.OTHER)
        {
            mSource = sc;
        }

        public override string ToString()
        {
            return "Componente de origem: " + source.ToString() + "\n" + base.ToString();
        }
    }
}

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using edde.maps;

namespace edde.components
{
    /// <summary>
    /// Classe componente que gera movimentos em um GameObject
    /// </summary>
    public class MoverComponent: SoftComponent
    {

        public delegate void EndAllMove();

        public delegate void StartFirstMove(ref Vector2 posDest, ref float velocity);

        public delegate void EndMove();

        public delegate void StartMove(ref Vector2 posDest, ref float velocity);

        public event EndAllMove OnEndAllMove;

        public event StartFirstMove OnStartFirstMove;

        public event EndMove OnEndMove;

        public event StartMove OnStartMove;

        /// <summary>
        /// N� usado para armazenar os movimentos
        /// </summary>
        private struct NodeMove
        {
            /// <summary>
            /// posi��o destino relativo ao cen�rio
            /// </summary>
            private Vector2     mPosDestiny;

            /// <summary>
            /// Velocidade do movimento
            /// </summary>
            private float mVelocity;

            public Vector2 posDestiny
            {
                get { return mPosDestiny; }
                set { mPosDestiny = value; }
            }

            public float velocity
            {
                get { return mVelocity; }
                set { mVelocity = value; }
            }

            public NodeMove(Vector2 pos, float velocity)
            {
                mPosDestiny = pos;
                mVelocity = velocity;
            }         
        };
      

        /// <summary>
        /// Determina se est� habilitado ou n�o.
        /// </summary>
        private bool mEnabled = true;


        /// <summary>
        /// Limite�de dist�ncia para poder ignorar um movimento. Em pixels.
        /// </summary>
        private const float MOVE_LIMIT_DISTANCE= 1;

        private GameObject m_parent = null;

        private LinkedList<NodeMove> m_listMovement = new LinkedList<NodeMove>();

        private float m_lastDistance = 0;

        private bool hasNextMove()
        {
            return m_listMovement.Count > 0;
        }

        public GameObject parent
        {
            get{ return m_parent;}
        }

        private NodeMove nextMove
        {
            get { return m_listMovement.First.Value; }
        }

        private void setNextMove(Vector2 pos, float velo)
        {
            m_listMovement.RemoveFirst();
            m_listMovement.AddFirst(new NodeMove(pos, velo));
        }


        public float lastDistance
        {
            get{return m_lastDistance;}
            set
            {
                m_lastDistance = value;
                parent.moving = (!stayStopped() && enable);
            }
        }

        public bool stayStopped()
        {
            return m_lastDistance == 0f;
        }

        public bool enable
        {
            get { return mEnabled; }
            set
            {
                mEnabled = value;
                parent.moving =  (value && !stayStopped());
            }
        }

        public Vector2 direction
        {
            get { return parent.getDirection(); }
        }

        private void popNextValue()
        {
            m_listMovement.RemoveFirst();
        }

        #region addMove

        /// <summary>
        /// adiciona um novo ponto de movimento
        /// </summary>
        /// <param name="newPos">nova posi��o destino, absoluta</param>
        /// <param name="velocity">a velocidade do movimento em pixels/milisegundos</param>
        public void addMove(Vector2 newPos, float velocity)
        {
             NodeMove node = new NodeMove(newPos, velocity/100);
             m_listMovement.AddFirst(node);
        }

        public void addMove(Vector2 newPos, TimeSpan delay)
        {
            NodeMove node = new NodeMove(newPos, (newPos - parent.position).Length()/(100*delay.Milliseconds));
            m_listMovement.AddFirst(node);
        }
        #endregion

        /// <summary>
        /// retorna um valor entre 0 e 1 indicando a porcentagem de efetua��o do movimento atual
        /// </summary>
        /// <returns></returns>
        public float accomplishActualMoviment()
        {
            if (!stayStopped())
            {
                Vector2 dist = nextMove.posDestiny - parent.position;
                float accom = dist.Length() / lastDistance;
                return Math.Max(0f, accom);
            }
            else
                return 0;
        }

        public void stopAll()
        {
            m_listMovement.Clear();
            lastDistance = 0;
        }

        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Rectangle rectWindow)
        {
            //faz nada
        }

        public void update(Microsoft.Xna.Framework.GameTime time)
        {
            if (hasNextMove())
            {
                Vector2 difVec;
                if (stayStopped()) //ainda n�o iniciou o movimento
                {
                    float velo = nextMove.velocity;
                    Vector2 pos = nextMove.posDestiny;
                    if(!ReferenceEquals(OnStartFirstMove, null))
                        OnStartFirstMove(ref pos, ref velo);
                    setNextMove(pos, velo);
                    
                    difVec = (nextMove.posDestiny - parent.position);
                    lastDistance = difVec.Length();
                }
                else
                    difVec =  (nextMove.posDestiny - parent.position);

                Vector2 direction = difVec;
                direction.Normalize();

                parent.setDirection(direction);
                direction *= nextMove.velocity;

                if (direction.Length() > difVec.Length()) // o movimento vai potencialmente extrapolar a posi��o destino)
                    direction = difVec;

                parent.incPosition(direction);

                if (MyMath.equal(parent.position, nextMove.posDestiny, MOVE_LIMIT_DISTANCE))
                {
                    if (!ReferenceEquals(OnEndMove, null))
                    OnEndMove();

                    popNextValue();
                    if (!hasNextMove())
                    {
                        lastDistance = 0f;
                        if (!ReferenceEquals(OnEndAllMove, null))
                            OnEndAllMove();
                    }
                    else
                    {
                        NodeMove node = nextMove;
                        float velo = nextMove.velocity;
                        Vector2 pos = nextMove.posDestiny;
                        if (!ReferenceEquals(OnStartMove, null))
                            OnStartMove(ref pos, ref velo);
                        setNextMove(pos, velo);

                        difVec = (nextMove.posDestiny - parent.position);
                        lastDistance = difVec.Length();
                    }
                }
            }
            else if(!stayStopped())
            {
                lastDistance = 0;
            }
        }
      
    public bool  init(GameObject myParent)
    {
        m_parent = myParent;
        return !ReferenceEquals(parent, null);
    }

    public void  ejected()
    {
        stopAll();
    }

    public bool  ejectMe()
    {
        return false;
    }

#endregion
}

    /// <summary>
    /// Este Mover ainda � burro e gera colis�o. Depois � preciso implementar um mover que use o gerente de f�sica para achar caminhos.
    /// ele n�o considera as c�lulas e move continuamente.
    /// </summary>
    //public class MoverT : SoftComponent
    //{

    //    public delegate void EndAllMoviment();

    //    public delegate void StartMoviment(ref Vector2 posDest, ref TimeSpan timeFinal, object owner);

    //    public event EndAllMoviment OnEndAllMoviment;

    //    public event StartMoviment OnStartMoviment;
        
    //    /// <summary>
    //    /// Determina se est� habilitado ou n�o.
    //    /// </summary>
    //    private bool mEnabled = true;

    //    /// <summary>
    //    /// Limite�de dist�ncia para poder ignorar um movimento
    //    /// </summary>
    //    public const float MOVE_LIMIT_DISTANCE= 1;

    //    public readonly Vector2 OBJECT_POS = new Vector2(-1, -1);

    //    #region itens usados para verificar a posi��o previa de um movimento

    //    private Vector2 mPosInit = Vector2.Zero;
        
    //    private TimeSpan mTimeActual = TimeSpan.Zero;

    //    private TimeSpan mTimeInit = TimeSpan.Zero;
    //    #endregion

    //    private GameObject  m_objectParent = null;
 
    //    private LinkedList<NodeMove> m_listDestiny = new LinkedList<NodeMove>();

    //    /// <summary>
    //    /// N� usado para armazenar os movimentos
    //    /// </summary>
    //    private struct NodeMove
    //    {
    //        private Vector2     mPosDest;
    //        private TimeSpan    mTimeFinal;
    //        private object      mMovimentOwner;
    //        //private object mDataMoviment;

    //        public Vector2 posDest
    //        {
    //            get{return mPosDest;}
    //            set { mPosDest = value; }
    //        }

    //        public TimeSpan timeFinal
    //        {
    //            get{return mTimeFinal;}
    //            set { mTimeFinal = value; }
    //        }

    //        public object owner
    //        {
    //            get { return mMovimentOwner; }
    //            set { mMovimentOwner = value; }
    //        }

    //        public NodeMove(Vector2 posDest, TimeSpan timeFinal, object owner)
    //        {
    //            mPosDest = (posDest);
    //            mTimeFinal = (timeFinal);
    //            mMovimentOwner = owner;
    //        }

    //        public NodeMove (Vector2 posDest, Vector2 posIni, TimeSpan timeInit, float velocity, object owner)
    //        {
    //            mPosDest = posDest;
    //            mTimeFinal = timeInit + TimeSpan.FromSeconds((posDest - posIni).Length() / velocity);
    //            mMovimentOwner = owner;
    //        }
    //    }


    //    //public Mover()
    //    //{
    //    //}

    //    private bool hasNextMove()
    //    {
    //        return m_listDestiny.Count > 0;
    //    }

    //    public bool moving()
    //    {
    //        return m_listDestiny.Count > 0;
    //    }

    //    private NodeMove nextMove
    //    {
    //        get { return m_listDestiny.First.Value; }
    //        set { m_listDestiny.First.Value = value; }
    //    }

    //    public object actualMoveOwner
    //    {
    //        get
    //        {
    //            if (hasNextMove())
    //                return nextMove.owner;
    //            else
    //                return null;
    //        }
    //    }

    //    public Vector2 direction
    //    {
    //        get{return parent.getDirection();}
    //        set{parent.setDirection(value);}
    //    }
    //    public void setDirection(Vector2 vec)
    //    {
    //        parent.setDirection(vec);
    //    }

    //    private void popNextMove()
    //    {
    //        m_listDestiny.RemoveFirst();
    //    }

    //    #region addMove
    //    public void addMove(Vector2 posDest, TimeSpan timeFinal)
    //    {
    //        addMove(posDest, timeFinal, null);
    //    }

    //    public void addMove(Vector2 posDest, TimeSpan timeFinal, object owner)
    //    {
    //        moving = true;
    //        m_listDestiny.AddLast(new NodeMove(posDest, timeFinal, owner));
    //    }

    //    public void addMove(Vector2 posDest, float velocity)
    //    {
    //        addMove(posDest, velocity, null);
    //    }

    //    public void addMove(Vector2 posDest, float velocity, object owner)
    //    {
    //        moving = true;

    //        Vector2 posInit = hasNextMove() ? nextMove.posDest : m_objectParent.position;   
    //        TimeSpan initTime   = hasNextMove()?nextMove.timeFinal:mTimeActual;

    //        m_listDestiny.AddLast(new NodeMove(posDest, posInit, initTime, velocity, owner));
    //    }

    //    public void addMove(TimeSpan deltaTime, float velocity)
    //    {
    //        addMove(deltaTime, velocity, null);
    //    }

    //    public void addMove(TimeSpan deltaTime, float velocity, object owner)
    //    {
    //       Vector2 vecVelocity = parent.getDirection() *  velocity;
    //       Vector2 vecPos = vecVelocity * (float)deltaTime.TotalSeconds;

    //       if (hasNextMove())
    //           vecPos += mPosInit;
    //       else
    //           vecPos += parent.position;

    //       addMove(vecPos, deltaTime + mTimeActual, owner);
    //    }
    //    #endregion

    //    #region addMovePath
        
    //    /// <summary>
    //    /// adiciona um movimento usando o sistema de PathFind
    //    /// </summary>
    //    /// <param name="deltaTime">O tempo usado para o movimento. Cada movimento unit�rio levar� deltaTime/n para ocorrer.</param>
    //    /// <param name="posDest">Posi��o destino</param>
    //    public bool addMovePath(TimeSpan deltaTime, Vector2 posDest)
    //    {
    //        if (RPG.me.sceneManager.hasScene())
    //        {
    //            Map map = RPG.me.sceneManager.topScene as Map;
    //            List<Vector2> vecPath = new List<Vector2>();
    //            if (!map.collisionManager.findPathTo(parent, posDest, vecPath)) return false;

    //            TimeSpan incTime = TimeSpan.FromSeconds(deltaTime.TotalSeconds / vecPath.Count);
    //            TimeSpan timeMove = incTime + mTimeActual;
    //            foreach (Vector2 vec in vecPath)
    //            {
    //                addMove(vec, timeMove);
    //                timeMove += incTime;
    //            }

    //            return true;
    //        }
    //        return false;
    //    }
    //    #endregion 
    //    /// <summary>
    //    /// Adiciona um movimento falso que deixa o GameObject parado. Isto � interessante para simular tempo em comportamentos parados.
    //    /// </summary>
    //    /// <param name="owner">Dono do moimento</param>
    //    public void addWaitMove(TimeSpan timeFinal, object owner)
    //    {
    //        moving = true;
    //        m_listDestiny.AddLast(new NodeMove(parent.position, timeFinal, owner));
    //    }

    //    /// <summary>
    //    /// retorna um valor entre 0 e 1 indicando a porcentagem de efetua��o do movimento atual
    //    /// </summary>
    //    /// <returns></returns>
    //    public float accomplishActualMoviment()
    //    {
    //        if (hasNextMove())
    //        {
    //            return Math.Max(0f, (nextMove.posDest - mPosInit).Length() / (float)(nextMove.timeFinal - mTimeInit).TotalSeconds);
    //        }
    //        else
    //            return 0;
    //    }
             
    //    public void stopAll()
    //    {
    //        m_listDestiny.Clear();
    //        if(OnEndAllMoviment != null) OnEndAllMoviment();

    //        parent.moving = false;
    //    }
    //    private GameObject parent
    //    {
    //        get { return m_objectParent; }
    //    }


    //    #region SoftComponent Members

    //    public void draw(Microsoft.Xna.Framework.GameTime time, ref Rectangle rectWindow)
    //    {
    //        //faz nada
    //    }

    //    public void update(Microsoft.Xna.Framework.GameTime time)
    //    {
    //        TimeSpan timePrev = mTimeActual;
    //        mTimeActual = time.TotalGameTime;

    //        if(hasNextMove())
    //        {
    //            if(mTimeInit == TimeSpan.Zero)
    //            {
    //                mTimeInit = mTimeActual;
    //                mPosInit = parent.position;
    //                if (OnStartMoviment != null)
    //                {
    //                    Vector2 posDest = nextMove.posDest;
    //                    TimeSpan timeFinal = nextMove.timeFinal;
    //                    OnStartMoviment(ref posDest, ref timeFinal, nextMove.owner);
    //                    nextMove = new NodeMove(posDest, timeFinal, nextMove.owner);
    //                }
    //            }

    //            if(nextMove.timeFinal <= mTimeActual)
    //            {
    //                NodeMove node = nextMove; popNextMove();
    //                //parent.setPosition(node.posDest);
    //                if(!hasNextMove())
    //                {
    //                    if(OnEndAllMoviment != null)
    //                        OnEndAllMoviment();
    //                    mPosInit = Vector2.Zero;
    //                    mTimeInit = TimeSpan.Zero;
    //                    parent.moving = false;
    //                }else
    //                {
    //                    mTimeInit   = mTimeActual;
    //                    mPosInit = parent.position;
    //                    if (OnStartMoviment != null)
    //                    {
    //                        Vector2 posDest = nextMove.posDest;
    //                        TimeSpan timeFinal = nextMove.timeFinal;
    //                        OnStartMoviment(ref posDest, ref timeFinal, nextMove.owner);
    //                        nextMove = new NodeMove(posDest, timeFinal, nextMove.owner);
    //                    }

    //                }
    //            }else
    //            {
    //                Vector2 nextPos = (nextMove.posDest - mPosInit)/(float)(nextMove.timeFinal - mTimeInit).TotalSeconds; //velocidade
    //                nextPos = nextPos * (float)(mTimeActual - timePrev).TotalSeconds;
    //                parent.incPosition(nextPos); 
    //            }
    //        }
       
    //    }

    //    public bool init(GameObject parent)
    //    {

    //        m_objectParent = (parent);
    //        return true;
    //    }

    //    public void ejected()
    //    {
    //    }
    //    public bool ejectMe()
    //    {
    //        return false;
    //    }

    //    public bool enable
    //    {
    //        get { return mEnabled; }
    //        set { mEnabled = true; }
    //    }

    //    #endregion
    //}
}

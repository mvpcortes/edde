using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using edde.physicalmanager;

namespace edde.components
{
    public class RotationCharSet: CharSetComponent
    {
        private float mRotation = (float)Math.PI/2f;

        /// <summary>
        /// Determina qual a c�lula de dire��o ser� usada do tileset.
        /// </summary>
        private int mDirectionID = 0;

        public RotationCharSet(string nameResource)
            : base(nameResource)
        {


        }
        public RotationCharSet(string nameResource, PATTERN pattern):base(nameResource, pattern){}

        public RotationCharSet(string nameResource, PATTERN pattern, float rot):base(nameResource, pattern){rotation = rot;}

        SpriteBatch mSpriteBatch = null;

        /// <summary>
        /// Rota��o da imagem em rela��o ao vetor de origem. em radianos
        /// </summary>
        public float rotation
        {
            get { return mRotation; }
            set { mRotation = value; }
        }

        /// <summary>
        /// determina qual c�lula de dire��o ser� usada do tileset
        /// </summary>
        public int directionID
        {
            get { return mDirectionID; }
            set { mDirectionID = value;}
        }

        public void setRotation(Vector2 vec)
        {
            vec.Normalize();
            rotation = (float)Math.Acos(Vector2.Dot(vec, Vector2.UnitX));
            
        }

        public override bool init(GameObject parent)
        {
            mSpriteBatch = RPG.me.spriteBatch;
            base.init(parent);
            return true;
        }

        public override void draw(Microsoft.Xna.Framework.GameTime time, ref Rectangle rectWindow)
        {
            // base.draw(time, offSet);
            RectFloat rectDest = new RectFloat((mParent.position - new Vector2(centerPoint.X, centerPoint.Y)), new Vector2(getTileWidth(), getTileHeight()));

            if (!Collision.intersects(rectDest, RectFloat.FromRectangle(rectWindow))) return;

            rectDest.top -= rectWindow.Y;
            rectDest.left -= rectWindow.X;
            Rectangle rectSource = getCharSetRect(directionID, getFrame());
            mSpriteBatch.Draw(mTexture, rectDest.getCenter(), rectSource, colorShandow, rotation + mPattern.ANGLE_SOURCE,
                new Vector2(rectDest.width/2, rectDest.height/2),
                1,
                SpriteEffects.None, 0);  
           // base.draw(time, offSet);
        }
    }
}

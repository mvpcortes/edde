using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using edde;

namespace edde.components
{
    /// <summary>
    /// Componente que exibe uma textura. Serve apenas para testes
    /// </summary>
    class ComponentStub:SoftComponent
    {
        private Texture2D mTexture;
        private GameObject mParent;

        public ComponentStub(string resource)
        {
            mTexture = Resource.getTexture2D(resource);
        }

        #region SoftComponent Members

        public void draw(GameTime time, ref Rectangle rectWindow)
        {
            RectFloat rect = mParent.getRectBase();
            rect.setBase(rect.getBase() + new Vector2(rectWindow.X, rectWindow.Y));

            SpriteBatch sb = RPG.me.spriteBatch;
            sb.Draw(mTexture, rect.toRectangle(), Color.White);  
        }

        public void update(GameTime time)
        {

        }

        public bool init(GameObject parent)
        {
            mParent = parent;
            return true;
        }

        public void ejected()
        {
            //todo
        }
   

        public bool ejectMe()
        {
            return false;
        }

        public bool enable
        {
            get { return true; }
            set { }//nothing }
        }

        #endregion
    }
}

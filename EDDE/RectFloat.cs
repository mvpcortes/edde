using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using edde;

namespace edde
{
    /// <summary>
    /// Estrutura elementar para representar um ret�ngulo.
    /// <remarks>
    /// Ela � voltada para a representa��o de retangulos em ponto flutuante. Para ponto fixo use a Rectangle do XNA.
    /// </remarks>
    /// </summary>
    public struct RectFloat
    {

        /// <summary>
        /// Vetor indicando a origem
        /// </summary>
        private Vector2 mSource;

        /// <summary>
        /// Vetor representando o tamanho
        /// </summary>
        private Vector2 mSize;

        /// <summary>
        /// Construtor usando quatro posi��es
        /// </summary>
        /// <param name="top">Valor do topo</param>
        /// <param name="left">Valor direito</param>
        /// <param name="right">Valor esquerdo</param>
        /// <param name="bottom">Valor da base</param>
        public RectFloat(float top, float left, float right, float bottom)
        {
            mSource = new Vector2(left, top);
            mSize = new Vector2(right - left, bottom - top );
        }

        /// <summary>
        /// Constr�i um Rect baseado em outros, gerando uma regi�o que os engloba
        /// </summary>
        /// <param name="regions"></param>
        public RectFloat(params RectFloat[] regions)
        {
            mSource = new Vector2(float.MaxValue, float.MaxValue);
            mSize   = new Vector2(float.MinValue, float.MinValue);

            foreach (RectFloat rect in regions)
            {
                this.mSource.X  = Math.Min(rect.mSource.X   , mSource.X);
                this.mSource.Y  = Math.Min(rect.mSource.Y   , mSource.Y);

                this.mSize.X    = Math.Max(rect.mSize.X     , mSize.X);
                this.mSize.Y    = Math.Max(rect.mSize.Y     , mSize.Y);
            }
        }

        /// <summary>
        /// Construtor usando dois vetores. Um de origem e outro de tamanho
        /// </summary>
        /// <param name="source">Vetor de origem</param>
        /// <param name="size">Vetor de tamanho</param>
        public RectFloat(Vector2 source, Vector2 size)
        {
            mSource = source;
            mSize = size;
        }

        public Vector2 getSource()  { return mSource;}

  
        public Vector2 getSize()    { return mSize;}

        public Vector2 getBase()    { return mSource + mSize; }

        public float top
        {
            get { return mSource.Y;}

            set { mSource.Y = value;}
        }

        public float left
        {
            get { return mSource.X; }

            set { mSource.X = value; }
        }

        public float bottom
        {
            get { return mSource.Y + mSize.Y; }

            set { mSize.Y = value - mSource.Y; }
        }

        public float right
        {
            get { return mSource.X + mSize.X; }

            set { mSize.X = value - mSource.X; }
        }

        public float width
        {
            get { return mSize.X; }
        }

        public float height
        {
            get { return mSize.Y; }
        }

        public float length()
        {
            return mSize.Length();
        }


        public void setSource(float x, float y) { mSource.X  = x; mSource.Y  = y; }
        public void setSource(Vector2 vec)      { mSource = vec; }
        public void setSize(float x, float y)   { mSize.X = x; mSize.Y = y; }
        public void setSize  (Vector2 vec)      { mSize = vec;}
        public void setBase(Vector2 vec)        { mSize = vec - mSource;}

        //public void setTop(float top)
        //{
        //}

        /// <summary>
        /// Transforma em um Retangle
        /// </summary>
        /// <returns></returns>
        public Rectangle toRectangle()
        {
            return new Rectangle(
                MyMath.round(this.mSource.X),
                MyMath.round(this.mSource.Y),
                MyMath.round(this.mSize.X),
                MyMath.round(this.mSize.Y)
            );
        }


        /// <summary>
        /// Gera um RectFloat baseado em um Rectangle
        /// </summary>
        /// <param name="rect">Rectangle de origem</param>
        /// <returns>RectFloat de destino</returns>
        static public RectFloat FromRectangle(Rectangle rect)
        {
            return new RectFloat((float)rect.Top, (float)rect.Left, (float)rect.Right, (float)rect.Bottom);

        }


        /// <summary>
        /// Expande o Rectangle usando uma escala
        /// </summary>
        /// <param name="scale">A escala usada</param>
        public void expandFromScale(float scale)
        {
            float addX = ((mSize.X * scale) - mSize.X);
            float addY = ((mSize.Y * scale) - mSize.Y);

            mSource.X -= addX/2;
            mSource.Y -= addY/2;
            mSize.X   += addX;
            mSize.Y   += addY;
        }


        /// <summary>
        /// Expande o Rectagle usando valor de incremento
        /// </summary>
        /// <param name="value">O valor de incremento</param>
        public void expand(float value)
        {
            mSource.X -= value;
            mSource.Y -= value;
            mSize.X += value*2;
            mSize.Y += value*2;
        }

        /// <summary>
        /// Retorna o centro do Rectangle
        /// </summary>
        /// <returns></returns>
        public Vector2 getCenter()
        {
            return mSource + mSize / 2;
        }


        /// <summary>
        /// Transforma em uma String
        /// </summary>
        /// <returns>A string de retorno</returns>
        public override string ToString()
        {
            return string.Format("{0:g}; {1:g}; {2:g}; {3:g}", mSource.X, mSource.Y, mSize.X, mSize.Y);
        }

        /// <summary>
        /// Gera um RectFloat quadrado a partir de um ponto central e o tamanho do lado.
        /// </summary>
        /// <param name="position">A posi��o central</param>
        /// <param name="side">O tamanho do lado</param>
        /// <returns>O novo RectFloat gerado</returns>
        public static RectFloat FromCenter(Vector2 position, float side)
        {
            RectFloat rect = new RectFloat(position - new Vector2(side / 2f, side / 2f), new Vector2(side, side));
            return rect;
        }
    }
}


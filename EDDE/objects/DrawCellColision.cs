﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde.components;
using edde.maps;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using edde.physicalmanager;

namespace edde.objects
{
    /// <summary>
    /// Desenha no mapa as células de colisão
    /// </summary>
    public class DrawCellColision : GameObject, edde.physicalmanager.IVisionObserver
    {

        /// <summary>
        /// Instância de uma grid para representar as células<br/> Este item também pinta as células de visão de outros GameObject contidos no mapa
        /// </summary>
        [Injection()]
        GameGridComponent mGameGrid;


        private static readonly Color DEFAULT_BG_CELL_COLOR = Color.TransparentBlack;

        /// <summary>
        /// Construtor
        /// </summary>
        public DrawCellColision()
        {
        }

        /// <summary>
        /// inicializa o GameObject, pegando dados do cenário e criando a Grid.
        /// </summary>
        /// <param name="scene"></param>
        /// <returns></returns>
        public override bool init(Scene scene)
        {
            Map map = scene as Map;
            CellCollisionManager cellColl = map.collisionManager as CellCollisionManager;
            float side = cellColl.getCellSize();
            mGameGrid  = new GameGridComponent((int)(cellColl.getHeight() / side), (int)(cellColl.getWidth() / side));
            mGameGrid.defaultCell.mBgColor = DEFAULT_BG_CELL_COLOR;
            mGameGrid.fixScreen = true;

            for (int i = 0; i < mGameGrid.colCount; i++)
            {
                mGameGrid.setColWidth(i, (int)side);
                mGameGrid[0,i].mText = i.ToString();
            }

            for (int i = 0; i < mGameGrid.rowCount; i++)
            {
                mGameGrid.setRowHeight(i, (int)side);
                mGameGrid[i,0].mText = i.ToString();
            }
            
            if (!base.init(scene)) return false;
            
            this.position = new Vector2(-20, -20);
            this.setTypeCollision(COLLISION_TYPE.VOID);

            return true;
        }

        /// <summary>
        /// Atualiza a grid tornando a cor das células transparente (DEFAULT_BG_CELL_COLOR)
        /// </summary>
        /// <param name="time"></param>
        public override void update(Microsoft.Xna.Framework.GameTime time)
        {
            for (int row = 0; row < mGameGrid.rowCount; row++)
                for (int col = 0; col < mGameGrid.colCount; col++)
            {
                mGameGrid[row, col].mBgColor = DEFAULT_BG_CELL_COLOR;
            }
            base.update(time);
        }

        #region IVisionObserver Members

        /// <summary>
        /// Atualiza a visão do objeto monitorado e a pinta na grid.
        /// </summary>
        /// <param name="collised">O objeto monitorado</param>
        /// <param name="coll">As células de visão</param>
        public void itemViewIt(edde.physicalmanager.ICollised collised, ICollection<Point> coll)
        {
            foreach (Point p in coll)
            {
                mGameGrid[p.Y, p.X].mBgColor = new Color(Color.Pink, 100);
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Microsoft.Xna.Framework;
using System.Xml;
using util;

namespace edde.objects
{
    /// <summary>
    /// Classe de fábrica de GameObjects.
    /// <remarks>
    /// Isto é usado para facilitar a criação de qq GameObject desenvolido através de arquivos XML (string)
    /// </remarks>
    /// </summary>
    class GameObjectFactory
    {
        /// <summary>
        /// Execção de contrução dinamica.
        /// </summary>
        public class DynamicCreatorGameObjectException : edde.eddeException
        {
            public DynamicCreatorGameObjectException(string msg, Exception source)
                : base("Erro durante a criação dinâmica de um GameObject: " + msg, source, REGION_LOOP.LOAD)
            {
            }
        }


        /// <summary>
        /// Nome do obje
        /// </summary>
        private string mName = "noname";

        /// <summary>
        /// O nome do objeto
        /// </summary>
        private string mTipo = "GameObject";

        /// <summary>
        /// Dicionário TODO
        /// </summary>
        private Dictionary<string, string> mDicProperties = new Dictionary<string,string>();
       
        /// <summary>
        /// Região padrão do GameObject
        /// </summary>
        private RectFloat mRectRegion = GameObject.DEFAULT_ICOLLISED_REGION;

        /// <summary>
        /// Posição central do GameObject
        /// </summary>
        private Vector2 mPosition = Vector2.Zero;

        /// <summary>
        /// Construtor
        /// </summary>
        public GameObjectFactory()
        {
        }

        /// <summary>
        /// Especifica o nome  do GameObject  que será gerado pela Fábrica. Este nome especifica o GameObject dentro da Engine.
        /// </summary>
        public string gameObjectName
        {
            get { return mName; }
            set { mName = value;}
        }

        /// <summary>
        /// Especifica o tipo do GameObject que será gerado pela fábrica.
        /// </summary>
        public string gameObjectType
        {
            get { return mTipo; }
            set { mTipo = value; }
        }

        /// <summary>
        /// Determina a região ocupada pelo GameObject criado. (a região relativa, não especifica sua posição no mapa)
        /// </summary>
        public RectFloat rectRegion
        {
            get { return mRectRegion; }
        }


        /// <summary>
        /// Atribui uma região levando em conta sua posição em relação a origem do cenário. Alterando sua posição inicial.
        /// </summary>
        /// <param name="region"></param>
        public void setPosAndRectByRegion(RectFloat region)
        {
            Vector2 media = region.getSize()/2;
            mPosition = region.getSource() + media;
            mRectRegion = new RectFloat(-media, region.getSize());
        }

        /// <summary>
        /// Determina a posição do GameObject
        /// </summary>
        public Vector2 position
        {
            get { return mPosition; }
        }


        /// <summary>
        /// Reinicia a fábrica
        /// </summary>
        public void reset()
        {
            gameObjectName = "noname";
            gameObjectType = "GameObject";
            mDicProperties.Clear();

            mRectRegion = GameObject.DEFAULT_ICOLLISED_REGION;

            mPosition = Vector2.Zero;
        }

        /// <summary>
        /// Copia os dados de dicProperties para o dicionário de propriedades interno
        /// </summary>
        /// <param name="dicProperties">Os dados de origem</param>
        public void setProperties(Dictionary<string, string> dicProperties)
        {
            mDicProperties.Clear();
            foreach (KeyValuePair<string, string> key in dicProperties)
                mDicProperties[key.Key] = key.Value;
        }

        /// <summary>
        /// Insere uma nova propriedade no dicionário
        /// </summary>
        /// <remarks>
        /// As propriedades são especificadas e serão usadas para alterar as propriedades (get/set) do objeto gerado
        /// </remarks>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void addProperty(string name, string value)
        {
            mDicProperties.Add(name, value);
        }

        /// <summary>
        /// Cria um objeto baseado na configuração da fábrica corrente.
        /// </summary>
        /// <returns>O objeto criado</returns>
        public GameObject createGameObject()
        {
            object oo = null;
            try
            {
                oo = Assembly.GetEntryAssembly().CreateInstance(gameObjectType, true);
                if (oo == null)
                    oo = Assembly.GetExecutingAssembly().CreateInstance(gameObjectType, true);
            }
            catch (Exception e)
            {
                if (e.GetBaseException() != null)
                    e = e.GetBaseException();
                throw new DynamicCreatorGameObjectException("não foi possível criar a classe \"" + gameObjectType + "\" adequadamente devido a:\n" + e.Message, e);
            }

            GameObject go = oo as GameObject;

            if (go == null) throw new DynamicCreatorGameObjectException("tipo \"" + gameObjectType + "\" não encontrado", null);

            go.setRectBase(rectRegion);
            go.setPosition(position);
            go.setDirection(MyMath.VEC_UP);

            foreach (KeyValuePair<string, string> key in mDicProperties)
            {
                Type type = go.GetType();
                PropertyInfo prop = type.GetProperty(key.Key);
                if (prop == null) throw new DynamicCreatorGameObjectException("Propriedade \"" + key.Key + "\" não existe.", null);
                try
                {
                    object objValue = convertData(prop.PropertyType, key.Value);
                    prop.SetValue(go, objValue, null);
                }
                catch (Exception e)
                {
                    throw new DynamicCreatorGameObjectException("Propriedade \"" + key.Key + "\" não pode ser alterada.", e);
                }
            }

            //verifica se o GameObject possui uma string name e tenta setá-la
            try
            {
                Type type = go.GetType();
                PropertyInfo prop = type.GetProperty("name");
                if (prop != null && prop.CanWrite)
                {
                    prop.SetValue(go, this.gameObjectName, null);
                }
            }
            catch (Exception)
            {
                //faz nada pq não deve ligar se não conseguir atribuir um nome
            }

            return go;
        }


        /// <summary>
        /// Converte um dado em string para um tipo default do XNA.
        /// </summary>
        /// <remarks>
        /// Este método evita gerar exceções.
        /// </remarks>
        /// <param name="type">O tipo a ser convertido</param>
        /// <param name="value">O valor de origem em string</param>
        /// <returns>O objeto criado</returns>
        private object convertData(Type type, string value)
        {
            if (type == typeof(int))
            {
                int a;
                if (int.TryParse(value, out a))
                    return a;
                else
                    return 0;
            }
            else if (type == typeof(bool))
            {
                if (value == "false")
                    return false;
                else if (value == "true")
                    return true;
                else
                {
                    int f = (int)StrTools.toFloat(value);
                    return f != 0;
                }
            }
            else if (type == typeof(float))
            {
                float a;
                if (float.TryParse(value, out a))
                    return a;
                else
                    return 0;
            }
            else if (type == typeof(string))
            {
                return value;
            }
            else if (type == typeof(RectFloat))
            {
                float[] vec = StrTools.toVecFloat(value);
                return new RectFloat(vec[0], vec[1], vec[2] - vec[1], vec[3] - vec[0]);
            }
            else if (type == typeof(Rectangle))
            {
                float[] vec = StrTools.toVecFloat(value);
                return new Rectangle((int)vec[0], (int)vec[1], (int)vec[2], (int)vec[3]);
            }
            else if (type == typeof(Vector2))
            {
                float[] vec = StrTools.toVecFloat(value);
                return new Vector2(vec[0], vec[1]);
            }
            else if (type == typeof(Point))
            {
                float[] vec = StrTools.toVecFloat(value);
                return new Point((int)vec[0], (int)vec[1]);
            }
            else
                return null;
        }
    }
}

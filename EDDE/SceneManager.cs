﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace edde
{
    /// <summary>
    /// Gerente de transição de mapas (singletons). O RPG o usa para plotar o mapa corrente.
    /// </summary>
    public class SceneManager : DrawableGameComponent
    {
        /// <summary>
        /// Tempo gasto em jogo em um cenário
        /// </summary>
        TimeSpan mTimePlay = TimeSpan.Zero;

        SpriteBatch mSpriteBatch;
     
        /// <summary>
        /// Tamanho máximo de arquivos que podem ser carregados previamente.
        /// </summary>
        //private static int LOAD_MAP_SIZE_BUFFER = 20;

        /// <summary>
        /// Pilha de cenas pecorridas pelo RPG
        /// </summary>
        private LinkedList<Scene> mStackScene = new LinkedList<Scene>();

        /// <summary>
        /// List de GameObjects esperando para entrar em runtime no mapa.
        /// </summary>
        private LinkedList<GameObject> mListSafeInsertGameObject = new LinkedList<GameObject>();

        private LinkedList<GameObject> mListSafeRemoveGameObject = new LinkedList<GameObject>();

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="rpg">referência ao Game</param>
        public SceneManager(RPG rpg):base(rpg)
        {

        }

        /// <summary>
        /// Insere seguramente um item em um cenário em tempo de execução
        /// </summary>
        /// <param name="go"></param>
        public void insertSafeGameObject(GameObject go)
        {
            if (ReferenceEquals(go, null)) throw new edde.eddeException("Não é possível adicionar uma referência para null");
            mListSafeInsertGameObject.AddLast(go);
        }

        public void removeSafeGameObject(GameObject go)
        {
            if (ReferenceEquals(go, null)) throw new edde.eddeException("Não é possível remover uma referência para null");
            mListSafeRemoveGameObject.AddLast(go);
        }

        internal TimeSpan timePlay
        {
            get{return mTimePlay;}
        }
        /// <summary>
        /// Verify if has some scene at stack top.
        /// </summary>
        /// <returns></returns>
        public bool hasScene()
        {
            return mStackScene.Count > 0;
        }

        /// <summary>
        /// Retorna a cena no topo da pilha (a cena corrente). Gera uma exceção caso não exista nada.
        /// </summary>
        /// <returns></returns>
        public Scene topScene
        {
            get { return mStackScene.First.Value; }
        }

        // <summary>
        // Entra em um scene (empilha)
        // </summary>
        // <param name="name">o nome do scene a ser acessado</param>
        // <returns> Um ponteiro para o scene carregado e entrado.</returns>
        // <remarks>
        // <para>Esta função trata do sistema de pilha de cenas. O jogador pode ir entrando em cenas e mais cenas, mas as anteriores ficam carregadas e memórias para acesso na volta. Isto é implementado como uma pilha.</para>
        // <para>Mesmo que uma instância deste mesmo mapa esteja na pilha. Será carregada uma nova instância desta cena independente da outra.</para>
        // </remarks>
        public Scene enterScene(string name)
        {
            Scene newScene = findScene(name);
            if (newScene == null) throw new edde.eddeException("Não foi possível encontrar o mapa especificado", eddeException.REGION_LOOP.UPDATE);
            return enterScene(newScene);            
        }

        public Scene enterScene(Scene newScene)
        {
            mStackScene.AddFirst(newScene);
            return mStackScene.First.Value;
        }

        /// <summary>
        /// Retira a atual cena do topo e a retorna
        /// </summary>
        /// <returns>Cena que foi removida do topo.</returns>
        public Scene exitScene()
        {
            Scene oldScene = mStackScene.First.Value;
            mStackScene.RemoveFirst();
            return oldScene;
        }

        /// <summary>
        /// Retira a cena no topo da pilha e a troca pela especificada pelo name. Retorna a Cena removida.
        /// </summary>
        /// <param name="name"></param>
        public Scene swapScene(string name)
        {
            Scene newScene = findScene(name);
            if (newScene == null) throw new edde.eddeException("Não foi possível encontrar o mapa especificado", eddeException.REGION_LOOP.UPDATE);
            return swapScene(newScene);
           
        }

        public Scene swapScene(Scene newScene)
        {
            Scene oldScene = null;
            if (hasScene())
            {
                oldScene = mStackScene.First.Value;
                mStackScene.RemoveFirst();
            }

            mStackScene.AddFirst(newScene);
            return oldScene;
        }

        private Scene findScene(string name)
        {
            maps.ReadXMLTileSetLayer reader = new edde.maps.ReadXMLTileSetLayer(name);
            maps.Map map = reader.generateNewMap();

            //inicializa o mapa
            map.init();
            return map;
        }

        protected sealed override void LoadContent()
        {
            base.LoadContent();
            mSpriteBatch = RPG.me.spriteBatch;
        }

        public sealed override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            if (hasScene())
            {
                //inicia o spriteBath.
                mSpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.None);
                
                topScene.Draw(gameTime);

                //finaliza o spriteBatch
                mSpriteBatch.End();
            }

        }

        public sealed override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            //
            if (hasScene())
            {
                while(mListSafeInsertGameObject.Count > 0)
                {
                    GameObject go = mListSafeInsertGameObject.First.Value;
                    mListSafeInsertGameObject.RemoveFirst();
                    go.init(topScene);
                    topScene.addObject(go);
                }
          
                Scene scene = topScene;
                scene.Update(gameTime);

                //faz atualização do tempo de play útil
                if (scene.countPlayTime())
                   mTimePlay += gameTime.ElapsedGameTime;

                while (mListSafeRemoveGameObject.Count > 0)
                {
                    GameObject go = mListSafeRemoveGameObject.First.Value;
                    mListSafeRemoveGameObject.RemoveFirst();
                    topScene.removeGameObject(go);
                }
          
            }
        }

        /// <summary>
        /// finaliza todos as scenas sem salvar sua informação
        /// </summary>
        internal void finishAllScenes()
        {
            foreach (Scene scene in mStackScene)
            {
                scene.finish();
            }
        }
    }
}

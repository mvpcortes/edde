﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace edde.PhysicalManager
{
    /// <summary>
    /// Classe que representa um nó no sistema de PathFind A*
    /// </summary>
    class NodePathFind: IEquatable<NodePathFind>
    {
        /// <summary>
        /// Posição relativa do nó
        /// </summary>
        private Point mPosition;

        /// <summary>
        /// valor de custo de Start até este nó. (g)
        /// </summary>
        private int mStartCust = 0;

        /// <summary>
        /// valor de custo estimado do End até este nó (h)
        /// </summary>
        private int mEndCust = 0;

        /// <summary>
        /// Referência ao nó pai
        /// </summary>
        private NodePathFind mParent = null;


        #region properties
        public NodePathFind parent
        {
            get { return mParent; }
            set { mParent = value; }
        }

        public Point position
        {
            get { return mPosition; }
        }

        public int fValue
        {
            get { return mStartCust + mEndCust; }
        }

        public int startCust
        {
            get { return mStartCust; }
            set { mStartCust = value; }
        }

        public int endCust
        {
            get { return mEndCust; }
            set { mEndCust = value;}
        }
        #endregion

        public NodePathFind(Point _position)
        {
            mPosition = _position;
        }

        public NodePathFind(NodePathFind otherNode): this(otherNode.position)
        {

        }

        public NodePathFind(NodePathFind otherNode, int deltaX, int deltaY):this(otherNode.position)
        {
            mPosition.X += deltaX;
            mPosition.Y += deltaY;
        }

        public NodePathFind(int x, int y):this(new Point(x, y))
        { 
        }


        public override string ToString()
        {
            return position.ToString();
        }

        public override int GetHashCode()
        {
            return position.GetHashCode();
        }

        #region IEquatable<NodePathFind> Members

        public bool Equals(NodePathFind other)
        {
            return this.position == other.position;
        }

        #endregion
    }
}

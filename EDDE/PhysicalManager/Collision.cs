using System;
using System.Collections.Generic;
using System.Text;

namespace edde.physicalmanager
{

    /// <summary>
    /// Tipos de estados de colis�o poss�veis
    /// </summary>
    public enum COLLISION_TYPE
    {
        VOID = -1,       //n�o colide com nada
        CLOUD,      //� um objeto, causa colis�o mas permite estar junto com outros objetos
        SOLID,      //� um objeto mas n�o permite estar junto com outros objetos
        WALL,       //� um objeto de estrutura do mapa, uma parede
    }

    /// <summary>
    /// Especifica algumas fun��es de colis�o entre estruturas comuns como ret�ngulos e c�rculos
    /// </summary>
    public static class Collision
    {
        public const float TOUCH_LIMIT_ESCALE   = 1.5f; //limite para considerar contato entre dois objetos.

        public const float VISION_DISTANCE      = 800.0f;

        public const float VISION_ANGLE         = (float)Math.PI / 4f;

        static public bool isCollisionType(COLLISION_TYPE type)
        {
            return (type == COLLISION_TYPE.SOLID) || (type == COLLISION_TYPE.WALL);
        }

        static public bool isTouchType(COLLISION_TYPE type)
        {
            return isCollisionType(type) || (type == COLLISION_TYPE.CLOUD);
        }
       
        /// <summary>
        /// interse��o entre dois retangulos
        /// </summary>
        /// <param name="a">O primeiro ret�ngulo</param>
        /// <param name="b">O segundo ret�ngulo</param>
        /// <returns>Se h� interse��o ou n�o</returns>
        static public bool intersects(RectFloat a, RectFloat b)
        {
           // Microsoft.Xna.Framework.GameServiceContainer.
            if (a.right     < b.left)   return false;
            if (a.left      > b.right)  return false;
            if (a.bottom    < b.top)    return false;
            if (a.top       > b.bottom) return false;
            return true;
        }


    }
}

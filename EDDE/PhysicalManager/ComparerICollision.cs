﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace edde.physicalmanager
{
    /// <summary>
    /// Compara e ver qual dos dois colliseds estão mais próximos de uma origem
    /// </summary>
    public class ComparerICollision: IComparer<ICollised>
    {
        private Vector2 mSource;
        public ComparerICollision(ICollised source)
        {
            mSource = source.getRectBase().getCenter();
        }

        public ComparerICollision(Vector2 vecSource)
        {
            mSource = vecSource;
        }

        #region IComparer<ICollised> Members

        //faz a comparação
        public int Compare(ICollised x, ICollised y)
        {
            if (ReferenceEquals(x, null))
            {
                if (ReferenceEquals(y, null))
                    return 0;
                else
                    return +1;
            }
            else
            {
                if (ReferenceEquals(y, null))
                    return -1;
            }

            float distX = (x.getRectBase().getCenter() - mSource).Length() ;
            float distY = (y.getRectBase().getCenter() - mSource).Length();

            if (distX < distY) return -1;
            else return +1;
        }

        #endregion
    }
}

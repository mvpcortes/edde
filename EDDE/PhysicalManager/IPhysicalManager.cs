using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace edde.physicalmanager
{
    /// <summary>
    /// Interface de um gerente de colis�o gen�rico para Scenes estilo Map.
    /// <remarks>
    /// A principal motiva��o de criar um gerente de colis�o e n�o aclopar a colis�o em outra entidade � permitir a implementa��o de otimiza��es
    /// para a colis�o.<br/>
    /// Assim, um Map tem uma entidade de ICollisionManager que � implementada por alguma forma de otimiza��o.<br/>
    /// Um gerente de colis�o � de certa forma independente de um Map pois ele precisa somente dos GameObjects que est�o sujeitos a colis�o assim como
    /// dos Walls (paredes, barreiras) do Map. O tamanho do Map tamb�m � desej�vel.
    /// </remarks>
    /// </summary>
    public interface IPhysicalManager
    {

        /// <summary>
        /// Define a regi�o onde existe os itens a serem checados por colis�o
        /// </summary>
        /// <param name="w">A largura, em pirxels</param>
        /// <param name="h">A altura, em pirxels</param>
        void setSizeRegion(int w, int h);

        /// <summary>
        /// Adiciona um GameObject ao sistema
        /// </summary>
        /// <param name="go">O objeto a ser inserido</param>
        void addObject(ICollised go);

        /// <summary>
        /// Remove um GameObject do sistema
        /// </summary>
        /// <param name="go">O objeto a ser removido</param>
        void removeObject(ICollised go);

        /// <summary>
        /// Verifica a colis�o e o toque em um objeto.
        /// <remarks>
        /// Por otimiza��o, a interface define que quando verificar colis�o tamb�m deve-se verificar toque entre dois GameObjects. Isto for�a haver apenas um �nico loop para estas tarefas.
        /// </remarks>
        /// </summary>
        /// <param name="obj">Objeto a ser testado usando seu rectBase</param>
        /// <param name="collCollision">container contendo os outros GameObjects que tiveram colis�o</param>
        /// <param name="collTouch">container contendo os outros GameObjects que tiveram toque</param>
        void testColision(GameObject obj, ICollection<ICollised> collCollision, ICollection<ICollised> collTouch);

        /// <summary>
        /// Verifica a vis�o do objeto obj e adiciona os objetos vistos em collVisible.
        /// </summary>
        /// <param name="obj">Objeto que ser� testada a vis�o</param>
        /// <param name="collVisible">Container que receber� os objetos vistos</param>
        void testVision(GameObject obj, ICollection<ICollised> collVisible);

        /// <summary>
        /// Gera um caminho (path) de um ponto source para um ponto destiny
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destiny"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        bool findPathTo(GameObject source, Vector2 destiny, ICollection<Vector2> path);

        /// <summary>
        /// Retorna todos os objetos contidos na regi�o especificada
        /// </summary>
        /// <param name="centerPoint">Centro da regi�o</param>
        /// <param name="range">Raio da regi�o (c�rculo)</param>
        /// <param name="collObjRegion">Container que receber� os itens</param>
        void getObjectRegion(Vector2 centerPoint, float range, ICollection<ICollised> collObjRegion);
    }
}

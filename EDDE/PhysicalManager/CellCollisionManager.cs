using System.Collections.Generic;
using System;
using util;
using Microsoft.Xna.Framework;
using edde.PhysicalManager;

namespace edde.physicalmanager
{
    /// <summary>
    /// Esta � uma implementa��o de um gerente de Colis�o que divide o espa�o �til do mapa em c�lulas de tamanho igual.<br/>
    /// O sistema se aproveita desta propriedade para otimizar a verifica��o de colis�o apenas testando os gameObjects existentes nas c�lulas onde o objeto a ser testado reside.
    /// </summary>
    public class CellCollisionManager: IPhysicalManager
    {

        /// <summary>
        /// Classe criada para permitir a cria��o de listas durante o manuseio da Matriz
        /// </summary>
        private class CreateList : Matrix<ICollection<ICollised>>.CreateIt
        {

            #region CreateIt Members

            public ICollection<ICollised> DoCreateIt()
            {
                return new LinkedList<ICollised>();
            }

            #endregion
        }

        /// <summary>
        /// Tamanho da c�lula
        /// </summary>
        private int mSizeCell;

        /// <summary>
        /// Tamanho default da c�lula
        /// </summary>
        static private int DEFAULT_SIZE_CELL = 32;

        /// <summary>
        /// Tamanho da grade usada no pathfind
        /// </summary>
        private const int PATHFIND_GRID_SIZE = 20;

        /// <summary>
        /// Custo necess�rio para mover uma c�lula do mapa
        /// </summary>
        private const int PATHFIND_MOVE_CUST = 1;


        /// <summary>
        /// Matriz que conter� as c�lulas.
        /// </summary>
        private Matrix<ICollection<ICollised>> mMatrixCollised;

        //private int mWidth, mHeight;
        private int mCountObject;

        /// <summary>
        /// por testes, � preciso verificar tratar quando um ICollised viu uma c�lula e quem ele viu.
        /// </summary>
        private Dictionary<ICollised, IVisionObserver> mDicVisionObserver;

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Retorna a largura da �rea �til de colis�o em pirxels</returns>
        public int getWidth()
        {
            return this.mMatrixCollised.Width * getCellSize();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Retorna a altura da �rea �til de colis�o em pirxels</returns>
        public int getHeight()
        {
            return mMatrixCollised.Height * getCellSize();
        }

        /// <summary>
        /// retorna o lado das c�lulas
        /// </summary>
        /// <returns>Retorna o tamanho do lado da c�lula usado para divis�o do espa�o �til.</returns>
        public int getCellSize() { return mSizeCell; }

        /// <summary>
        /// Atribui um novo tamanho de c�lula �til para o sistema
        /// </summary>
        /// <param name="size">o novo tamanho (lado) da c�lula</param>
        public void setCellSize(int size)
        {
            int w = getWidth() * getCellSize();
            int h = getHeight() * getCellSize();
            mSizeCell = Math.Max(1, Math.Min(size, Math.Min(w, h)));
            setSizeRegion(w, h);
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="w">Tamanho da largura �til em pixels</param>
        /// <param name="h">Tamanho da altura �til em pixels</param>
        public CellCollisionManager(int w, int h)
        {
            mCountObject = 0;
            this.mSizeCell = DEFAULT_SIZE_CELL;
            this.mMatrixCollised = new Matrix<ICollection<ICollised>>(1, 1, new CreateList());
            mDicVisionObserver = new Dictionary<ICollised, IVisionObserver>();
            setSizeRegion(w, h);
        }

        /// <summary>
        /// Retorna o n�mero de objetos dentro do sistema
        /// </summary>
        /// <returns>O n�mero de objetos</returns>
        private int getCountObject()
        {
            return mCountObject;
        }


        /// <summary>
        /// Dado um RectFloat, retorna quais c�lulas ele ocupa dentro do sistema
        /// </summary>
        /// <param name="rect">O RectFloat (retangulo) a ser testado</param>
        /// <returns>Um Rectangle onde suas posi��es indicam quais as c�lulas inicial e final da regi�o onde o rect reside.<br/>
        ///Isto �, (return.Top, return.Left) indicam qual a c�lula inicial da regi�o e (return.bottom, return.right) a c�lula final.
        ///</returns>
        private Rectangle getCellsContains(RectFloat rect)
        {
            int x = (int)rect.left / getCellSize();
            int y = (int)rect.top / getCellSize();
            int xfim = (int)rect.right / getCellSize();
            int yfim = (int)rect.bottom / getCellSize();

            x = Math.Max(x, 0);
            y = Math.Max(y, 0);

            if (xfim >= mMatrixCollised.Width)
            {
                xfim = mMatrixCollised.Width - 1;
                x = Math.Min(x, xfim);
            }

            if (yfim >= mMatrixCollised.Height)
            {
                yfim = mMatrixCollised.Height - 1;
                y = Math.Min(y, yfim);
            }

            return new Rectangle(x, y, xfim - x, yfim - y);
        }

        /// <summary>
        /// fun��o de heuristica para usar no pathfind (A*)
        /// </summary>
        /// <param name="node"></param>
        /// <param name="nodeEnd"></param>
        /// <returns></returns>
        private static int heuristicFunction(Point node, Point nodeEnd)
        {
            return Math.Abs(node.X - nodeEnd.X) + Math.Abs(node.Y - nodeEnd.Y);
        }

        #region ICollisionManager Members

        public void setSizeRegion(int w, int h)
        {
            Dictionary<ICollised, int> mapTemp = new Dictionary<ICollised, int>(getCountObject());
            for (int i = 0; i < mMatrixCollised.Width; i++)
                for (int j = 0; j < mMatrixCollised.Height; j++)
                {
                    foreach (ICollised go in mMatrixCollised[i, j])
                    {
                        mapTemp[go] = 0;
                    }
                    mMatrixCollised[i, j].Clear();
                }

            w = Math.Max(w, getCellSize());
            h = Math.Max(h, getCellSize());


            mMatrixCollised.setSize(w / getCellSize(), h / getCellSize(), new CreateList());

            foreach (ICollised go in mapTemp.Keys)
            {
                Rectangle rectCells = getCellsContains(go.getRectBase());
                for (int i = rectCells.Left; i <= rectCells.Right; i++)
                    for (int j = rectCells.Top; j <= rectCells.Bottom; j++)
                        mMatrixCollised[i, j].Add(go);
            }
        }

        public void addObject(ICollised go)
        {
            Rectangle rectCells = getCellsContains(go.getRectBase());
            for (int i = rectCells.Left; i <= rectCells.Right; i++)
                for (int j = rectCells.Top; j <= rectCells.Bottom; j++)
                    mMatrixCollised[i, j].Add(go);
            mCountObject++;
        }

        public void removeObject(ICollised go)
        {
            Rectangle rectCells = getCellsContains(go.getRectBase());

            for (int i = rectCells.Left; i <= rectCells.Right; i++)
                for (int j = rectCells.Top; j <= rectCells.Bottom; j++)
                    mMatrixCollised[i, j].Remove(go);

            mCountObject--;
        }

        public void testColision(GameObject obj, ICollection<ICollised> collCollision, ICollection<ICollised> collTouch)
        {
           //if (!Collision.isCollisionType(obj.getTypeCollision())) return;
           if (!Collision.isTouchType(obj.getTypeCollision())) return;

            RectFloat rectObj = obj.getRectBase();

            RectFloat rectObjExpand = rectObj;
            rectObjExpand.expandFromScale(Collision.TOUCH_LIMIT_ESCALE);

            Rectangle rectCells = getCellsContains(rectObjExpand);

            for (int i = rectCells.Left; i <= rectCells.Right; i++)
                for (int j = rectCells.Top; j <= rectCells.Bottom; j++)
            {
                foreach (ICollised other in mMatrixCollised[i, j])
                {
                    if (!ReferenceEquals(obj, other))
                    {
                       
                        //if (Collision.intersects(otherRect, rectObj) && Collision.isCollisionType(other.getTypeCollision()))
                        if(other.intersects(rectObj) && Collision.isCollisionType(other.getTypeCollision()))
                        {
                            if (!collCollision.Contains(other))
                            {
                                collCollision.Add(other);

                                if (!collTouch.Contains(other))
                                    collTouch.Add(other);//(se ele colidiu ele tbm tocou...
                            }
                        }
                        else
                        {
                            if (other.intersects(rectObjExpand) && Collision.isTouchType(other.getTypeCollision()))
                            {
                                if (!collTouch.Contains(other))
                                    collTouch.Add(other);
                            }
                        }
                    }
                }
            }
        }

        private void validPoint(ref Point p)
        {
            p.X = Math.Min(Math.Max(0, p.X), mMatrixCollised.Width-1);
            p.Y = Math.Min(Math.Max(0, p.Y), mMatrixCollised.Height-1);
        }

        private Point addPointsOfCell(GameObject excludeIt, Vector2 origem, Dictionary<ICollised, int> collVisible)
        {
            Point pos = new Point((int)origem.X / getCellSize(), (int)origem.Y / getCellSize());
            //validPoint(ref pos);

            if ((pos.X < 0 || pos.X >= mMatrixCollised.Width)
                ||
                (pos.Y < 0 || pos.Y >= mMatrixCollised.Height)
                )
                return new Point(-1, -1);

            ICollection<ICollised> coll = mMatrixCollised[pos.X, pos.Y];
            foreach (ICollised collised in coll)
            {
                if (!ReferenceEquals(collised, excludeIt))
                    collVisible[collised] = 1;
            }

            return pos;
        }

        private Point addPointsOfCell(GameObject excludeIt, Vector2 origem, Dictionary<Point, int> dicPoints, Dictionary<ICollised, int> collVisible)
        {
            Point p = addPointsOfCell(excludeIt, origem, collVisible);
            if((p.X > -1) && (p.Y > -1))
                dicPoints[p] = 1;

            return p;
        }

        public void testVision(GameObject obj, ICollection<ICollised> collVisible)
        {
            Dictionary<ICollised, int>dicTemp = new Dictionary<ICollised, int>(getCountObject() / 4);
            IVisionObserver observer = null;
            collVisible.Clear();

            Vector2 origem = obj.position;
            Vector2 vecVision = Vector2.Normalize(obj.getDirection());
            Vector2 vecPerp = new Vector2(-vecVision.Y, vecVision.X);//cria vetor perpendicular
            float halfAngle = Collision.VISION_ANGLE / 2;

            //observador
            if (mDicVisionObserver.TryGetValue((ICollised)obj, out observer))
            {
                Dictionary<Point, int> dicPoint = new Dictionary<Point, int>(getCountObject() / 4);

                addPointsOfCell(obj, origem, dicPoint, dicTemp);

                float inc = getCellSize() / ((float)Math.Sqrt(2));

                for (float tValue = inc; tValue < Collision.VISION_DISTANCE + inc; tValue += inc)
                {
                    Vector2 posCenter = (tValue * vecVision) + origem;//pega ponto na reta da vis�o
                    addPointsOfCell(obj, posCenter, dicPoint, dicTemp);
                    float prof = ((float)(Math.Tan(halfAngle)) * (tValue * vecVision)).Length();

                    float tProf = inc;
                    for (; tProf < prof; tProf += inc)
                    {
                        Vector2 posPerp1 = tProf * vecPerp + posCenter;
                        Vector2 posPerp2 = -tProf * vecPerp + posCenter;
                        addPointsOfCell(obj, posPerp1, dicPoint, dicTemp);
                        addPointsOfCell(obj, posPerp2, dicPoint, dicTemp);
                    }
                }

                if (!ReferenceEquals(dicPoint, null))
                    observer.itemViewIt((ICollised)obj, dicPoint.Keys);

            }
            else
            {
                addPointsOfCell(obj, origem, dicTemp);

                float inc = getCellSize() / ((float)Math.Sqrt(2));

                for (float tValue = inc; tValue < Collision.VISION_DISTANCE + inc; tValue += inc)
                {
                    Vector2 posCenter = (tValue * vecVision) + origem;//pega ponto na reta da vis�o
                    addPointsOfCell(obj, posCenter, dicTemp);
                    float prof = ((float)(Math.Tan(halfAngle)) * (tValue * vecVision)).Length();

                    float tProf = inc;
                    for (; tProf < prof; tProf += inc)
                    {
                        Vector2 posPerp1 = tProf * vecPerp + posCenter;
                        Vector2 posPerp2 = -tProf * vecPerp + posCenter;
                        addPointsOfCell(obj, posPerp1, dicTemp);
                        addPointsOfCell(obj, posPerp2, dicTemp);
                    }
                }
            }

            foreach (KeyValuePair<ICollised, int> kv in dicTemp)
            {
                collVisible.Add(kv.Key);
            }
        }

        /// <summary>
        /// Verifica se existe colis�o em uma data c�lula.
        /// </summary>
        /// <param name="p">A c�lula a ser verificada X, Y</param>
        /// <returns>Se existem item de colis�o ou n�o</returns>
        private bool pathCollision(Point p)
        {
            ICollection<ICollised> collection = mMatrixCollised[p.X, p.Y];
            RectFloat rect = new RectFloat(p.X * DEFAULT_SIZE_CELL, p.Y * DEFAULT_SIZE_CELL, DEFAULT_SIZE_CELL, DEFAULT_SIZE_CELL);
            foreach(ICollised collised in collection)
            {
                if (Collision.isCollisionType(collised.getTypeCollision()) && collised.intersects(rect))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Usa o A* para achar um caminho entre source e um destiny.
        /// Esta vers�o � otimizada para usar a grid do CellCollisionManager. Entretanto classes usadas no A* gen�rico tamb�m s�o usadas aqui.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destiny"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool findPathTo(GameObject source, Vector2 destiny, ICollection<Vector2> path)
        {
            LinkedList<NodePathFind> openedNodes = new LinkedList<NodePathFind>();
            Dictionary<NodePathFind, int> closedNodes = new Dictionary<NodePathFind, int>();

            NodePathFind start = new NodePathFind((int)(source.position.X / getCellSize()), (int)(source.position.Y / getCellSize()));
            NodePathFind end = new NodePathFind((int)(destiny.X / getCellSize()), (int)(destiny.Y / getCellSize()));
            
            {
                start.startCust = 0;
                start.endCust = heuristicFunction(start.position, end.position);
                openedNodes.AddLast(start);
            }

            Rectangle region = new Rectangle(start.position.X - PATHFIND_GRID_SIZE / 2, start.position.Y - PATHFIND_GRID_SIZE / 2, PATHFIND_GRID_SIZE, PATHFIND_GRID_SIZE);
            
            //crop valid region 
            region.X = Math.Max(0, region.X);
            region.Y = Math.Max(0, region.Y);
            region.Width  = Math.Min(region.Width , mMatrixCollised.Width  - region.X -1);
            region.Height = Math.Min(region.Height, mMatrixCollised.Height - region.Y -1);

            while (openedNodes.Count > 0)
            {
                NodePathFind current = null;
                {
                    LinkedListNode<NodePathFind> nodeLL = openedNodes.First;
                    LinkedListNode<NodePathFind> minNodeLL = nodeLL;
                    while (nodeLL != null)
                    {
                        if (nodeLL.Value.fValue < minNodeLL.Value.fValue)
                        {
                            minNodeLL = nodeLL;
                        }
                        nodeLL = nodeLL.Next;
                    }
                    current = minNodeLL.Value;
                    openedNodes.Remove(minNodeLL);
                    closedNodes[current] = 0;
                }

                //faz vizinhos
                NodePathFind[] nodeVizinho = new NodePathFind[]
                {
                    new NodePathFind(current, -1, -1),
                    new NodePathFind(current, -1,  0),
                    new NodePathFind(current, -1, +1),
                    new NodePathFind(current,  0, -1),
                    new NodePathFind(current,  0, +1),
                    new NodePathFind(current, +1, -1),
                    new NodePathFind(current, +1,  0),
                    new NodePathFind(current, +1, +0)
                };

                foreach (NodePathFind node in nodeVizinho)
                {
                    //Est� na regi�o e ainda n�o foi fechado
                    if (region.Contains(node.position) && !closedNodes.ContainsKey(node) && !pathCollision(node.position))
                    {
                        NodePathFind realNode = null;
                        foreach (NodePathFind nodeTemp in openedNodes)
                        {
                            if (nodeTemp.Equals(node))
                            {
                                realNode = nodeTemp;
                                break;
                            }
                        }

                        if (realNode != null)
                        {
                            if (realNode.startCust > current.startCust + PATHFIND_MOVE_CUST)
                            {
                                realNode.startCust = current.startCust + PATHFIND_MOVE_CUST;
                                realNode.parent = current;
                            }
                        }
                        else
                        {
                            node.parent = current;
                            node.startCust = current.startCust + PATHFIND_MOVE_CUST;
                            node.endCust = heuristicFunction(node.position, end.position);
                            openedNodes.AddLast(node);

                            if (node.Equals(end))//chegou no alvo
                            {
                                end = node;
                                openedNodes.Clear();//for�a limpar para sair do loop do container
                                break;
                            }
                        }
                    }
                }
            }//end while

            path.Clear();
            List<Vector2> listTemp = new List<Vector2>();

            //v� se achou o pai do final
            if (end.parent != null)
            {
                while (end != null)
                {

                    listTemp.Add(new Vector2(end.position.X * DEFAULT_SIZE_CELL, end.position.Y * DEFAULT_SIZE_CELL));
                    end = end.parent;
                }
            }
            else
            {
                NodePathFind nodeNear = closedNodes.GetEnumerator().Current.Key;
                foreach (KeyValuePair<NodePathFind, int> kv in closedNodes)
                {
                    NodePathFind node = kv.Key;
                    if (heuristicFunction(node.position, end.position) < heuristicFunction(node.position, end.position))
                        nodeNear = node;
                }
                
                while (nodeNear!= null)
                {                    
                    listTemp.Add(new Vector2(nodeNear.position.X * DEFAULT_SIZE_CELL, nodeNear.position.Y * DEFAULT_SIZE_CELL));
                    nodeNear = nodeNear.parent;
                }
            }
            for (int i = listTemp.Count - 1; i >= 0; i--)
            {
                path.Add(listTemp[i]);
            }

            return path.Count > 0;

            //PATHFIND_GRID_SIZE

            //Point pos;
            //{
            //    Vector2 tempPos = source.getPosition();
            //    pos = new Point(tempPos.X / CellCollisionManager.DEFAULT_SIZE_CELL, tempPos.Y / CellCollisionManager.DEFAULT_SIZE_CELL);
            //}


        }

        public void addVisionObserver(IVisionObserver observer, ICollised who)
        {
            mDicVisionObserver[who] = observer;
        }


        void IPhysicalManager.getObjectRegion(Vector2 centerPoint, float range, ICollection<ICollised> collObjRegion)
        {
            collObjRegion.Clear();

            range /= 2f;
            RectFloat rect = new RectFloat(centerPoint.Y - range, centerPoint.X - range, centerPoint.X + range, centerPoint.Y + range);
            Rectangle rectCells = this.getCellsContains(rect);

             for (int i = rectCells.Left; i <= rectCells.Right; i++)
                 for (int j = rectCells.Top; j <= rectCells.Bottom; j++)
                 {
                     foreach (ICollised other in mMatrixCollised[i, j])
                     {
                         if ((other.getRectBase().getCenter() - centerPoint).Length() < range)
                         {
                             if (!collObjRegion.Contains(other))
                                 collObjRegion.Add(other);
                         }
                     }
                 }
        }
        #endregion
    }
}

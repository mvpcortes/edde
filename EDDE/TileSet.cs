using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace edde
{
    /// <summary>
    /// Cria uma abstra��o para acesso a tilesets existentes em uma imagem
    /// </summary>
    public class TileSet
    {
        /// <summary>
        /// Tamanhos default para um tileset
        /// </summary>
        static public int DEFAULT_WIDTH = 16, DEFAULT_HEIGHT = 16;

        /// <summary>
        /// Valor de tileset inv�lido, usado para retorno do sistema indicando que n�o foi poss�vel achar dado tileset
        /// </summary>
        public const int INVALID_TILESET = -1;


        /// <summary>
        /// Largura em pixels de um tileset para este objeto
        /// </summary>
        protected int mTileWidth;

        /// <summary>
        /// Altura em pixels de um tileset para este objeto
        /// </summary>
        protected int mTileHeight;
    	 
        /// <summary>
        /// A textura contendo os tilesets
        /// </summary>
	    protected Texture2D mTexture;

        /// <summary>
        /// Quantos tiles existem em uma linha do texture2D
        /// </summary>
        protected int mTileCellCount; 

        /// <summary>
        /// Construtor
        /// </summary>
        public TileSet()
        {
            mTileCellCount = 0;
            mTexture = null;
            mTileWidth = DEFAULT_WIDTH;
            mTileHeight = DEFAULT_HEIGHT;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="name">Nome do recurso contendo o tileset</param>
        public TileSet(string name):this(name, DEFAULT_WIDTH, DEFAULT_HEIGHT, true)
        {
            
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="name">Nome do recurso contendo o tileset</param>
        /// <param name="tileWidth">Largura do tileset</param>
        /// <param name="tileHeight">Altura do tileset</param>
        public TileSet(string name, int tileWidth, int tileHeight):this(name, tileWidth, tileHeight, true)
        {
        }

        public TileSet(string name, int tileWidth, int tileHeight, bool useFirstColorTransparent)
        {
            if (useFirstColorTransparent)
                mTexture = Resource.getTexture2DFirstColorTransparent(Resource.PATH_TILESET + name);
            else
                mTexture = Resource.getTexture2D(Resource.PATH_TILESET + name);

            mTileWidth  = Math.Max(tileWidth, 1);
            mTileHeight = Math.Max(tileHeight, 1);
            mTileCellCount = getTileXCount();            
        }
    	 
        /// <summary>
        /// </summary>
        /// <returns>Retorna a largura dos tilesets</returns>
	    public int getTileWidth() {
		    return mTileWidth;
	    }
    	 
        /// <summary>
        /// </summary>
        /// <returns>Retorna a altura dos tilesets</returns>
	    public int getTileHeight() {
		    return mTileHeight;
	    }
    	 
        /// <summary>
        /// Atribui uma nova largura aos tilesets
        /// </summary>
        /// <param name="w"></param>
	    public void setTileWidth(int w) {
            mTileWidth = Math.Max(w, 1);
	    }
    	 
        /// <summary>
        /// atribui uma nova altura aos tilesets
        /// </summary>
        /// <param name="h"></param>
	    public void setTileHeight(int h) {
            mTileHeight = Math.Max(h, 1);
	    }
    	 
        /// <summary>
        /// Retorna o n�mero de tilesets em uma linha (fileira) do recurso.
        /// </summary>
        /// <remarks>
        /// Um recurso usado neste sistema � como uma "matriz" contendo tilesets que s�o acessados em suas posi��es x,y. Este m�todo retorna quantos tilesets existem em uma linha da matriz.
        /// </remarks>
        /// <returns></returns>
	    public int getTileXCount() {
		    return mTexture.Width/mTileWidth;
        }

        /// <summary>
        /// Retorna o n�mero de tilesets em uma coluna do recurso.
        /// </summary>
        /// <remarks>
        /// Um recurso usado neste sistema � como uma "matriz" contendo tilesets que s�o acessados em suas posi��es x,y. Este m�todo retorna quantos tilesets existem em uma coluna da matriz.
        /// </remarks>
        /// <returns></returns>
	    public int getTileYCount() {
            return mTexture.Height / mTileHeight;
	    }

        /// <summary>
        /// retorna qual id de um recurso dada sua posi��o x e y.
        /// </summary>
        /// <remarks>
        /// um tileset tamb�m pode ser identificado como um ID. Cada ID possui um valor X e Y correspondente a posi��o do tileset no recurso. E cada posi��o X e Y possuem um ID tamb�m.<br/>
        /// Este valor de ID � importante para armazenar um mapa em arquivo.
        /// </remarks>
        /// <param name="x">coordenada X</param>
        /// <param name="y">coordenada Y</param>
        /// <returns></returns>
	    public int getTileIDCount(int x, int y) {
		    return x + mTileWidth*y;
	    }
    	 

        /// <summary>
        /// retorna um Rectanglecontendo o tileset idenficado pela posi��o X e Y.
        /// </summary>
        /// <param name="x">Cooordenada X do tileset que se deseja resgatar o rect</param>
        /// <param name="y">Cooordenada Y do tileset que se deseja resgatar o rect</param>
        /// <remarks>
        /// Para resgatar dados dentro do recurso, usa-se uma regi�o dentro dele. Esta regi�o � um Rectangle. Isto � normalmente usado para pintar um rectangle na tela.
        /// </remarks>
        /// <returns>O Rectangle contendo o tileset desejado dentro do recurso</returns>
	    public Rectangle getRect(int x, int y) {
		    Rectangle rect;
            rect.X      = x * getTileWidth();
            rect.Y      = y * getTileHeight();
            rect.Width  = getTileWidth();
            rect.Height = getTileHeight();
            return rect;
	    }

        /// <summary>
        /// retorna um Rectanglecontendo o tileset idenficado pelo ID do TileSet
        /// </summary>
        /// <param name="id">ID do tileset que se deseja resgatar o rect</param>
        /// <remarks>
        /// Para resgatar dados dentro do recurso, usa-se uma regi�o dentro dele. Esta regi�o � um Rectangle. Isto � normalmente usado para pintar um rectangle na tela.
        /// </remarks>
        /// <returns>O Rectangle contendo o tileset desejado dentro do recurso</returns>
        public Rectangle getRect(int id)
        {
            int x = id % mTileCellCount;
            int y = id / mTileCellCount;
            return getRect(x, y);
        }


        /// <summary>
        /// Retorna o recurso (Texture2D) contendo os tilesets
        /// </summary>
        /// <returns>A textura</returns>
        public Texture2D getTexture() { return mTexture; }
    }
}

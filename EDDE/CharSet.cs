using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using edde;

namespace edde
{

    /// <summary>
    /// Classe que implementa acesso a um recurso como um CharSet de RPG Maker
    /// </summary>
    public class CharSet : TileSet 
    {
        public struct PATTERN
        {
            public readonly int CHAR_WIDTH;
            public readonly int CHAR_HEIGHT;
            public readonly int MIDDLE_FRAME;
            public readonly int FRAME_COUNT;
            public readonly int DIR_COUNT;
            public readonly Vector2 VECTOR_SOURCE_ANGLE;
            public readonly float ANGLE_SOURCE;//angulo de origem em rela��o ao vetor (1, 0);
            public readonly int[] VEC_ANIMATION;
            public readonly RectFloat RECT_BASE;

            private PATTERN(int cw, int ch, int mf, int fc, int dc, Vector2 sa, int[] vecAnim)
            {
                CHAR_WIDTH=(cw);
                CHAR_HEIGHT=(ch);
                MIDDLE_FRAME=(mf);
                FRAME_COUNT=(fc);
                DIR_COUNT=(dc);
                VECTOR_SOURCE_ANGLE=(sa);
                VEC_ANIMATION = vecAnim;
                float min = Math.Min(CHAR_WIDTH, CHAR_HEIGHT)/2;
                RECT_BASE = new RectFloat(Vector2.One * (-min / 2), Vector2.One * (min));
                ANGLE_SOURCE = (float)Math.Acos(Vector2.Dot(Vector2.UnitX, VECTOR_SOURCE_ANGLE));
            }

            private float acos(float p)
            {
                throw new Exception("The method or operation is not implemented.");
            }

            public static PATTERN PATTERN_RPG_2K        = new PATTERN( 24, 32, 1, 3, 4, MyMath.VEC_UP, new int[4]  { 0, 1, 2, 1 }   );
            public static PATTERN PATTERN_RPG_XP        = new PATTERN( 24, 32, 1, 3, 4, MyMath.VEC_UP, new int[4]  { 0, 1, 2, 3 }   );
            public static PATTERN PATTERN_RENAN         = new PATTERN(160,160, 8,15, 4, MyMath.VEC_UP, new int[15] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            public static PATTERN PATTERN_64_4          = new PATTERN( 64, 64, 1, 4, 4, MyMath.VEC_UP, new int[4]  { 0, 1, 2, 3 }   );
            public static PATTERN PATTERN_FIVE_FRAME    = new PATTERN( 24, 32, 0, 5, 4, MyMath.VEC_UP, new int[5] { 0, 1, 2, 3, 4 });
            public static PATTERN PATTERN_FIVE_FRAME_LOOP = new PATTERN(24, 32, 0, 5, 4, MyMath.VEC_UP, new int[8] { 0, 1, 2, 3, 4, 3, 2, 1 });
            public static PATTERN PATTERN_CELULA        = new PATTERN(64, 64, 0, 1, 1, MyMath.VEC_UP, new int[1] {0});
            public static PATTERN PATTERN_SMALL_CELULA  = new PATTERN(16, 16, 0, 1, 1, MyMath.VEC_UP, new int[1] { 0 });
            public static PATTERN PATTERN_INVALID       = new PATTERN(0, 0, 0, 0, 0, Vector2.Zero, null);
            public static PATTERN PATTERN_DEFAULT = PATTERN_RPG_2K;

            public static PATTERN byName(string name)
            {
                if (string.Compare(name, "RPG_2K", false) == 0)
                    return PATTERN_RPG_2K;
                else if (string.Compare(name, "RPG_XP", false) == 0)
                    return PATTERN_RPG_2K;
                else if (string.Compare(name, "RENAN", false) == 0)
                    return PATTERN_RENAN;
                else if (string.Compare(name, "DEFAULT", false) == 0)
                    return PATTERN_DEFAULT;
                else if (string.Compare(name, "64_4", false) == 0)
                    return PATTERN_64_4;
                else
                    return PATTERN_INVALID;
            }

            //public static bool operator !=(PATTERN A, PATTERN B)
            //{
            //    return !ReferenceEquals(A, B);
            //}

            //public static bool operator ==(PATTERN A, PATTERN B)
            //{
            //    return ReferenceEquals(A, B);
            //}

            public override bool Equals(object obj)
            {
                return ReferenceEquals(this, obj);
            }

            public override int GetHashCode()
            {
                return 
                (
                    (
                        this.ANGLE_SOURCE+
                        this.CHAR_HEIGHT+
                        this.CHAR_WIDTH+
                        this.DIR_COUNT+
                        this.FRAME_COUNT+
                        this.MIDDLE_FRAME
                     )                      .GetHashCode()+
                    this.RECT_BASE          .GetHashCode()+
                    this.VEC_ANIMATION      .GetHashCode()+
                    this.VECTOR_SOURCE_ANGLE.GetHashCode()
                ).GetHashCode();
            }


        }

        /*/// <summary>
        /// Largura de um charset
        /// </summary>
        public static int CHAR_WIDTH = 320;//24;

        /// <summary>
        /// Altura de um charset
        /// </summary>
        public static int CHAR_HEIGHT = 320; //= 32;

        /// <summary>
        /// Frame do meio do CharSet. Ele � importante pois define o frame default, onde n�o h� movimento 
        /// </summary>
        public static int MIDDLE_FRAME  = 2;


        /// <summary>
        /// N�mero de frames em uma dire��o do CharSet
        /// </summary>
        public static int FRAME_COUNT   = 5;
        
        /// <summary>
        /// N�mero de dire��es em um CharSet
        /// </summary>
        public static int DIR_COUNT     = 4;


        /// <summary>
        /// Primeira dire��o de uma fileira de CharSet. A partir dele os outros s�o dispostos no sentido do rel�gio.
        /// </summary>
        public static Vector2 VECTOR_SOURCE_ANGLE = MyMath.VEC_UP;//new Vector2(0, -1);*/

        /// <summary>
        /// determina modelo do tileset
        /// </summary>
        public PATTERN mPattern;

        /// <summary>
        /// Especifica qual CharSet est� sendo especificado dentro do Recurso. Ele � identificado em sua posi��o X, Y
        /// </summary>
        /// <remarks>
        /// Um recurso possui v�rios CharSets. Um CharSet � composto por um conjunto de TileSets em forma de matriz da forma FRAME_COUNT*DIR_COUNT.
        /// </remarks>
        private Point mPointCharSet; 

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="name">Nome do recurso que cont�m o CharSet</param>
        public CharSet(string name) : this(name, PATTERN.PATTERN_DEFAULT)
        { 
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="name">Nome do recurso que cont�m o charset</param>
        /// <param name="width">Tamanho de um TileSet em pixels</param>
        /// <param name="height">Tamanho de um TileSet em pirxels</param>
        public CharSet(string name, PATTERN pattern)
        {
            mPointCharSet = new Point();
            mTexture = Resource.getTexture2DFirstColorTransparent(Resource.PATH_CHARSET + name);
            mPattern = pattern;
            mTileWidth = mPattern.CHAR_WIDTH;
            mTileHeight = mPattern.CHAR_HEIGHT;
        }

        public virtual void setPattern(PATTERN pattern)
        {
            mPattern = pattern;
            mTileHeight = (pattern.CHAR_HEIGHT);
            mTileWidth  = (pattern.CHAR_WIDTH);
        }

        public virtual void setStrPattern(string name)
        {
            PATTERN p = PATTERN.byName(name);

            if (PATTERN.PATTERN_INVALID.Equals(p))
            {
                setPattern(p);
            }
        }

        /// <summary>
        /// Especifica qual o CharSet referenciado pela classe
        /// </summary>
        /// <param name="x">a posi��o em X</param>
        /// <param name="y">a posi��o em Y</param>
        public void setPointCharSet(int x, int y)
        {
            mPointCharSet.X = x;
            mPointCharSet.Y = y;
        }

        /// <summary>
        /// Retorna a posi��o X e Y de um CharSet
        /// </summary>
        /// <returns></returns>
        public Point getPointCharSet() { return mPointCharSet; }


        /// <summary>
        /// Retorna um rect onde est� contido o TileSet de um CharSet especificado pela sua dire��o (id da c�lula) e o frame
        /// </summary>
        /// <param name="dirFrame">o id de dire��o</param>
        /// <param name="frame"> o frame do TileSet</param>
        /// <returns></returns>
        public Rectangle getCharSetRect(int dir_frame, int frame)
        {
            frame = frame % mPattern.FRAME_COUNT;

            Rectangle rect;
            rect.X = frame * mPattern.CHAR_WIDTH + mPointCharSet.X * (mPattern.CHAR_WIDTH * mPattern.FRAME_COUNT);//mCharSet.X * CHAR_WIDTH + 
            rect.Y = dir_frame * mPattern.CHAR_HEIGHT + mPointCharSet.Y * (mPattern.CHAR_HEIGHT * mPattern.DIR_COUNT);
            rect.Width = mPattern.CHAR_WIDTH;
            rect.Height = mPattern.CHAR_HEIGHT;
            return rect;
        }

        /// <summary>
        /// Retorna um Rectangle onde est� contido o TileSet de um CharSet especificado pela sua dire��o e o frame
        /// </summary>
        /// <param name="dir">A dire��o do TileSet</param>
        /// <param name="frame">O frame do TileSet</param>
        /// <returns>O retangle</returns>
        public Rectangle getCharSetRect(Vector2 dir, int frame)
        {
            float angle = MyMath.angle(mPattern.VECTOR_SOURCE_ANGLE, dir);
            if (dir.X < 0) //pq o angulo s� � detectado de 0 a 180�. Maiores do que isto devem ser tratados aqui.
                angle = (float)(2.0 * Math.PI - angle);

            float fdir_frame;
            if (float.IsNaN(angle) || float.IsInfinity(angle))
                fdir_frame = mPattern.MIDDLE_FRAME;
            else
                fdir_frame = angle * ((float)mPattern.DIR_COUNT) / (2f * (float)Math.PI);
            
            int dir_frame = MyMath.round(fdir_frame) % mPattern.DIR_COUNT;
            return getCharSetRect(dir_frame, frame);           
        }
    }
}

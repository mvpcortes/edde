using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using System.Collections;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MotivationalSystem;

namespace edde
{
    /// <summary>
    /// Classe que implementa o jogo em si.
    /// </summary>
    public class RPG: Game
    {

        //public SoundManager mSoundManager;

        #region GameManagers

        /// <summary>
        /// Controle de cenas do sistema
        /// </summary>
        private SceneManager mSceneManager;

        /// <summary>
        /// Retorna o controlador de cenas
        /// </summary>
        public SceneManager sceneManager { get { return mSceneManager; } }

      //  public
        /// <summary>
        /// Controle mestre do sistema
        /// </summary>
        private InputControl mInputControl;

        /// <summary>
        /// Retorna o controlador de controle
        /// </summary>
        public InputControl inputControl { get { return mInputControl; } }

        #endregion

        /// <summary>
        /// Manager obrigat�rio pelo XNA
        /// </summary>
        private GraphicsDeviceManager mGraphics;

        /// <summary>
        /// SpriteBatch obrigat�rio pelo XNA
        /// </summary>
        private SpriteBatch mSpriteBatch;

        //=============================================================
        /// <summary>
        /// Refer�ncia a inst�ncia do singletons
        /// </summary>    
        private static RPG msMe = new RPG();


        /// <summary>
        /// Tempo total de jogo
        /// </summary>
	    private TimeSpan mTime;

        /// <summary>
        /// Tempo jogado de jogo. (tempo em que o usu�rio controlou o personagem, fora de menus e coisas do g�nero.
        /// </summary>
        private TimeSpan mTimePlay;
   
        /// <summary>
        /// Tamanho padr�o de largura de tela
        /// </summary>
        static public readonly int DEFAULT_SCREEN_WIDTH = 800;

        /// <summary>
        /// Tamanho padr�o de altura de tela
        /// </summary>
        static public readonly int DEFAULT_SCREEN_HEIGHT = 600;

   
        /// <summary>
        /// Construtor. � protegido pq � um singletons.
        /// </summary>
        /// <remarks>
        /// Cria o Gerente de Cena (Scene Manager) e o gerente de input (InputControl)
        /// </remarks>
        protected RPG():base()
        {
            msMe = this;
            mGraphics = new GraphicsDeviceManager(this);
            mSceneManager = new SceneManager(this);
            Content.RootDirectory = "Content";

            //
            mTime = new TimeSpan(0);
            mTimePlay = mTime;

            mInputControl = new InputControl(this);

            mSceneManager = new SceneManager(this);

        }

        /// <summary>
        /// N�o faz nada muito importante. Apenas faz o handle do evento de sa�da do sistema e cria o sistema de sprites
        /// </summary>
        protected override void Initialize()
        {
            //game framerate
            //Do not synch our Draw method with the Vertical Retrace of our monitor
            mGraphics.SynchronizeWithVerticalRetrace = false;
            
            //Call our Update method at the default rate of 1/60 of a second.
            IsFixedTimeStep = false;


            //
            mGraphics.PreferredBackBufferWidth = RPG.DEFAULT_SCREEN_WIDTH;
            mGraphics.PreferredBackBufferHeight = RPG.DEFAULT_SCREEN_HEIGHT;
          

            //Apply the changes made to the device
            mGraphics.ApplyChanges();
            
            mSpriteBatch = new SpriteBatch(GraphicsDevice);// � preciso contruir aqui pois o GraphicsDevice precisa estar inicializado.

            Components.Add(mInputControl);
            Components.Add(mSceneManager);

            Exiting += exitHandle;
            base.Initialize();
        }

      
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            GraphicsDevice device = mGraphics.GraphicsDevice;
 
            base.LoadContent();
        }



        /// <summary>
        /// Retorna uma inst�ncia do SpriteBatch.
        /// </summary>
        public SpriteBatch spriteBatch
        {
            get { return mSpriteBatch; }
        }

        /// <summary>
        /// Retorna o tempo total de jogo
        /// </summary>
        /// <remarks>
        /// Este fun��o retorna o tempo real de jogo, a partir do seu in�cio
        /// </remarks>
        /// <returns>O tempo total de jogo</returns>
	    public TimeSpan getTimeTotal()
        {
		    return mTime;
	    }

        /// <summary>
        /// Retorna o tempo de jogo corrido do sistema.
        /// </summary>
        /// <remarks>
        /// O sistema tamb�m guarda o tempo de jogo real. Este valor s� � incrementado por determinadas scenes, em geral as de <c>map</c> (mapa). A de menu, por exemplo, n�o contabiliza tempo de jogo.
        /// </remarks>
        /// <returns></returns>
        public TimeSpan timePlay
        {
            get { return mSceneManager.timePlay; }
        }
    	 

        /// <summary>
        /// Retorna a inst�ncia do singletons
        /// </summary>
        /// <returns>O singletons de RPG</returns>
	    public static RPG me
        {     
            get{return msMe;}
	    }

        /// <summary>
        /// handle do exit do Game
        /// </summary>
        private void exitHandle(object sender, EventArgs e)
        {
            exit(false);
        }
        /// <summary>
        /// Sai do RPG
        /// </summary>
        /// <param name="save">Especifica se deve salvar o jogo ou n�o.</param>
	    public virtual void exit(bool save) {
            sceneManager.finishAllScenes();
	    }
    	 
        /// <summary>
        /// Salva o jogo
        /// </summary>
        /// <param name="filename">O nome do container (arquivo) l�gico a ser salvo o jogo.</param>
        /// <returns></returns>
	    public bool save(string filename) {
            //TODO faz algo aqui
		    return false;
	    }
    	 
        /// <summary>
        /// Carrega um jogo salvo
        /// </summary>
        /// <param name="filename">O nome de um contaienr (arquivo) l�gico para carregar o jogo.</param>
	    public void load(string filename) {
            //TODO faz algo aqui
	    }
    	 
	    protected override void Update(GameTime time) {
            base.Update(time);
	    }
        
        /// <summary>
        /// Apaga o fundo da tela
        /// </summary>
        /// <param name="time"></param>
        protected override void Draw(GameTime time)
        {
            mGraphics.GraphicsDevice.Clear(Color.Black);
            base.Draw(time);
            
	    }
     

        //TODO ver como faz para pegar o tamanho width da janela
        public int screenWidth { get { return mGraphics.PreferredBackBufferWidth; } }

        //TODO ver como faz para pegar o tamanho height da janela
        public int screenHeight { get { return mGraphics.PreferredBackBufferHeight; } }
      
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Celulas
{
    class GeneCelula: Gene
    {
        enum PARTITION_POS
        {
            VELOCITY    = 0,
            REPRODUTION = 1,
            TIMELIFE    = 2
        };


        const float MAX_VELOCITY    = 0.6f;

        const float MAX_REPRODUTION = 6f;

        const float MAX_TIMELIFE    = 5f*60f;
        
        const float VELOCITY_RATE    = MAX_VELOCITY     / (float)MAX_VALUE_PARTITION;

        const float REPRODUTION_RATE = MAX_REPRODUTION  / (float)MAX_VALUE_PARTITION;

        const float TIMELIFE_RATE    = MAX_TIMELIFE     / (float)MAX_VALUE_PARTITION;

        public GeneCelula():base()
        { }
        public GeneCelula(Gene a, Gene b)
            : base(a, b)
        {
        }
        public float velocity
        {
            get 
            { 
                return VELOCITY_RATE*(float)get((int)PARTITION_POS.TIMELIFE);
            }
            set 
            { 
                set((int)PARTITION_POS.TIMELIFE, value/VELOCITY_RATE);
            }
        }

        public int reprodution
        {
            get 
            {
                return (int)(REPRODUTION_RATE*(float)get((int)PARTITION_POS.REPRODUTION));
            }
            set 
            {
                set((int)PARTITION_POS.REPRODUTION, (float)(value)/REPRODUTION_RATE);
            }
        }

        public TimeSpan timeLife
        {
            get
            {
                return TimeSpan.FromSeconds(TIMELIFE_RATE*(float)get((int)PARTITION_POS.TIMELIFE)); 
            }
            set
            {
                set((int)PARTITION_POS.TIMELIFE, (float)value.TotalSeconds /TIMELIFE_RATE); 
            }
        }
    }
}

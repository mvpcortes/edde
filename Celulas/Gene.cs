﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Numeric;
using edde;

namespace Celulas
{
    public class Gene
    {
        private const int NUM_PARTICOES = 3;
        private const int TAM_PARTICAO = 12;
        private const int TAM_CROMOSSOMO = TAM_PARTICAO * NUM_PARTICOES;

        protected const double MAX_VALUE_PARTITION = 4095.0;//2^36-1//(TAM_PARTICAO-1 >= 0)?Math.Pow(2,TAM_PARTICAO-1):0.0;

        private bool[] mVecCromossomo = new bool[TAM_CROMOSSOMO];

        private float mOptimalValue = -1f;
        public Gene()
        {
            randomizeGene();
            generateOptimalValue();
        }

        public Gene(Gene pai, Gene mae)
        {
            Gene.crossover(pai, mae, this);
            this.mutacao();
            generateOptimalValue();
        }

        public double get(int nIndice)
        {
            if ((nIndice < 0) || (nIndice >= NUM_PARTICOES))
            {
                Debug.Assert(false);
                return -1;
            }

            double dValor = 0;
            int nPotencia = 0;
            for (int i = nIndice * TAM_PARTICAO; i < (nIndice + 1) * TAM_PARTICAO; ++i)
            {
                if (mVecCromossomo[i] == true)
                    dValor += Math.Pow(2, nPotencia);

                ++nPotencia;
            }

            return dValor;
        }

        public void set(int nIndice, double dValor)
        {
            if ((nIndice < 0) || (nIndice >= NUM_PARTICOES))
                return;


            double dValorTemp = dValor;
            int nPotencia = TAM_PARTICAO - 1;
            double dValorPotencia = Math.Pow(2, nPotencia);
            for (int i = (nIndice+1)*TAM_PARTICAO -1; i >= nIndice*TAM_PARTICAO; --i)
            {
                double nBit = dValorTemp / dValorPotencia;
                if (nBit >= 1)
                {
                    mVecCromossomo[i] = true;
                    dValorTemp -= dValorPotencia;
                }
                else
                    mVecCromossomo[i] = false;

                --nPotencia;
                dValorPotencia = Math.Pow(2, nPotencia);
            }
        }

        public bool getBit(int i)
        {
            return mVecCromossomo[i];
        }

        public void setBit(int i, bool v)
        {
            mVecCromossomo[i] = v;
        }

        public static Gene crossover(Gene gene1, Gene gene2, Gene filho)
        {
            int k = MyMath.getRandom(TAM_CROMOSSOMO);
            for (int i = 0; i < k; i++)
            {
                filho.setBit(i, gene1.getBit(i));
            }

            for (int i = k; i < TAM_CROMOSSOMO; i++)
            {
                filho.setBit(i, gene2.getBit(i));
            }

            return filho;
        }

        public void mutacao()
        {
            if (MyMath.getRandom(10) == 0)
                return;

            int nIndice = MyMath.getRandom(TAM_CROMOSSOMO);
            mVecCromossomo[nIndice] = !mVecCromossomo[nIndice];
        }

        public void randomizeGene()
        {
            for (int i = 0; i < TAM_CROMOSSOMO; i++)
            {
                setBit(i, MyMath.getRandom(5) == 0);
            }
        }

        public override string ToString()
        {
            string temp = "(";
            for (int i = 0; i < TAM_CROMOSSOMO; i++)
            {
                temp += getBit(i) ? "1" : "0";
            }
            return temp + ")";
        }

        public void generateOptimalValue()
        {
            float valueInc = 0;
            for (int i = 0; i < TAM_CROMOSSOMO; i++)
            {
                valueInc += getBit(i) ? 1f : 0f;
            }

            mOptimalValue = 100f * (
                            valueInc / (float)TAM_CROMOSSOMO);
        }


        /// <summary>
        /// Retorna o valor de otimicidade em porcentos
        /// </summary>
        /// <returns></returns>
        public int getOptimalValue()
        {
            double acum = 0.0;
            //return (int)mOptimalValue;
            for (int i = 0; i < NUM_PARTICOES; i++)
            {
                acum += get(i);
            }

            return (int)(acum*100.0 / (MAX_VALUE_PARTITION * NUM_PARTICOES));
           
        }
    }
}

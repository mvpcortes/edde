﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using edde.physicalmanager;
using Celulas.reputation;
using ReputationSystem;
using Celulas.components;

namespace Celulas
{
    class Celula: GameObjectReputable
    {
        #region injection  itens
        [Injection]
        edde.components.RotationCharSet mCharSet = new edde.components.RotationCharSet("small_celula");

        [Injection]
        edde.components.RandomMoviment mRandomMoviment = new edde.components.RandomMoviment(VELOCITY);

       // [Injection]
       // edde.components.RectBaseComponent mRectBaseComponent = new edde.components.RectBaseComponent();

        [Injection]
        FindAndFollowComponent mFindFollow = new FindAndFollowComponent();

        [Injection]
        components.CoupleComponent mCouple = new Celulas.components.CoupleComponent();

        [Injection]
        components.CelulaController mController = new Celulas.components.CelulaController();

        [Injection]
        edde.components.TextComponent mText = new edde.components.TextComponent("arial_12_b");
        #endregion

        #region fields

        /// <summary>
        /// Velocidade default da célula
        /// </summary>
        private const float VELOCITY = 0.3f;

        /// <summary>
        /// Tempo passado desde a ultima vez que atualizou a direção
        /// </summary>
        private TimeSpan mLastTimeUpdateDirection = TimeSpan.Zero;

        /// <summary>
        /// Delay de uma atualização de direção e outra
        /// </summary>
        private readonly TimeSpan mDelayTimeUpdateDirection = TimeSpan.FromSeconds(0.2f);
  
        /// <summary>
        /// Determina se o giro é para a esquerda ou direita
        /// </summary>
        private bool mIsRightRotation = true;

        /// <summary>
        /// Gene usado para atribuir os valores da célula
        /// </summary>
        private GeneCelula mGene = null;

        private bool mEjected = false;

        private readonly Color[] mVecColors = new Color[] 
        {
            Color.Red,
            Color.Blue,
            Color.Green,
            Color.Yellow,
            Color.Violet,
            Color.Tomato,
            Color.Lime,
            Color.Orange,
            Color.Gray,
            Color.Gainsboro
        };
        #endregion

        #region properties

        /// <summary>
        /// Cor usada para exibir a célula
        /// </summary>
        public Color color
        {
            get { return mCharSet.colorShandow; }
            set { mCharSet.colorShandow = value; }
        }

        public Celula otherTouch
        {
            get { return mCouple.otherTouch; }
            set { mCouple.otherTouch = value as Celula; }
        }

        /// <summary>
        /// Tempo de vida da célula
        /// </summary>
        public TimeSpan timeLife
        {
            get { return mController.timeLife; }
            set { mController.timeLife = value; }
        }

        public GeneCelula gene
        {
            get { return mGene; }
        }

        public components.CelulaController.STATE state
        {
            get { return mController.state; }
        }

        public bool ejected
        {
            get { return mEjected; }
            set { mEjected = value; }
        }
        #endregion

        public Celula(string _name, GeneCelula _gene)
        { 
            name = _name;
            mGene = _gene;
            refreshGene();
            CelulaLog.Instance.add(name, this.ToString(), mGene.ToString());
        }

        public Celula(string _name):this(_name, new GeneCelula())
        {
        }

        public Celula(string _name, Celula pai, Celula mae)
        {
            name = _name;
            mGene = new GeneCelula(pai.gene, mae.gene);
            refreshGene();

        }

        public void refreshGene()
        {
            mCouple.childMakeCount  = gene.reprodution;

            timeLife                = TimeSpan.FromSeconds(10) + gene.timeLife;

            mRandomMoviment.incValue= gene.velocity;

            mFindFollow.velocity    = gene.velocity;
        }

        #region GameObject Methods
        public override bool init(Scene scene)
        {
            bool ok = base.init(scene);
            if (ok)
            {
                mCharSet.setPattern(edde.CharSet.PATTERN.PATTERN_SMALL_CELULA);
                mCharSet.centerPoint = new Point(edde.CharSet.PATTERN.PATTERN_SMALL_CELULA.CHAR_WIDTH / 2, edde.CharSet.PATTERN.PATTERN_SMALL_CELULA.CHAR_HEIGHT / 2);
                setTypeCollision(edde.physicalmanager.COLLISION_TYPE.CLOUD);
                RectFloat rect = getRectBase();
                //setRectBase(new Rectangle);

                //
                setDirection(MyMath.getRandomDir());
                mIsRightRotation = MyMath.getRandomBool();
             
                //
                mFindFollow.velocity = VELOCITY;
                mFindFollow.parentScene = scene;
                mController.state = Celulas.components.CelulaController.STATE.RANDOM;

                //
                mText.color = Color.SpringGreen;
                mText.posCoor = new Vector2(10, 10);
            }

            color =  mVecColors[MyMath.getRandom(mVecColors.Length)];

            return ok;
        }

        public override void update(GameTime time)
        {
            base.update(time);

            TimeSpan actualTime = time.TotalGameTime;
            if (actualTime - mLastTimeUpdateDirection > mDelayTimeUpdateDirection)
            {
                mLastTimeUpdateDirection = actualTime;
                if(mIsRightRotation)
                    mCharSet.rotation += (float)Math.PI / 20f;
                else
                    mCharSet.rotation -= (float)Math.PI / 20f;


                if (MyMath.getRandom(10) == 0)
                {
                    this.mIsRightRotation = MyMath.getRandomBool();
                }
            }

            mText.text =  string.Format("{0}:{1}:{2}", this.name, CelulaController.toString(this.state), gene.getOptimalValue());
        }

        public override void draw(GameTime time, ref Rectangle rectWindow)
        {
        
            base.draw(time, ref rectWindow);
        }

        public override bool testVision(GameTime gameTime)
        {
            return false;
        }

        public override void onTouch(ICollection<ICollised> collObject)
        {
            base.onTouch(collObject);
            otherTouch = null;
            util.PriorityList<IReputable> priorityList = new util.PriorityList<IReputable>
                (new ReputationSystem.ReputationCenter.ComparableReputable<IReputable>(false));
            foreach (ICollised collised in collObject)
            {
                if (collised is IReputable)
                    priorityList.Add(collised as IReputable);
            }

            foreach (IReputable reputable in priorityList)
            {
                GameObjectReputable go = reputable as GameObjectReputable;
                if (!ReferenceEquals(go, null))
                {
                    otherTouch = go as Celula;
                    break;
                }
            }
        }

        public override string ToString()
        {
            return gene.getOptimalValue().ToString();
        }
        #endregion


        internal bool canGetGene(Celula other)
        {
            float reputationMe = 0f;
            float reputationIt = 0f;
            if (!ReputationCenter.Instance.getReputation(other, out reputationIt))
                reputationIt = float.MaxValue;
            if (!ReputationCenter.Instance.getReputation(this, out reputationMe))
                reputationMe = 0;

            if (reputationIt < reputationMe/2f)
            {
                int i = 0;
            }
            return (reputationIt >= reputationMe);
        }
    }
}

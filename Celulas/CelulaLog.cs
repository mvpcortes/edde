﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Celulas
{
    class CelulaLog
    {
        private static CelulaLog mInstance;
        private StreamWriter mDestinyFile = new StreamWriter("C:\\Celula\\celulas.log");
        private StreamWriter mScoreFile = new StreamWriter("C:\\Celula\\scores.log");

        /// <summary>
        /// Construtor
        /// </summary>
        public CelulaLog()
        {
        }

        /// <summary>
        /// Retorna a intância do singletons
        /// </summary>
        public static CelulaLog Instance
        {
            get
            {
                if (mInstance == null)
                    mInstance = new CelulaLog();
                
                return mInstance;
            }
        }

        public void add(string str1, string str2, string str3)
        {
            /*mDestinyFile.WriteLine(str1 + " - " + str2);
            mDestinyFile.WriteLine(str3);
            mDestinyFile.WriteLine("");*/
            int cellCount  = 0;
            if (CelulasGame.me.sceneManager.hasScene())
            {
                cellCount = CelulasGame.me.sceneManager.topScene.gameObjectCount() - 1;
            }
            int reputableCount = ReputationSystem.ReputationCenter.Instance.memoryCount();
            float ratio = (float)reputableCount/(float)cellCount;
            mDestinyFile.WriteLine(cellCount.ToString() + ", " + reputableCount.ToString() + ", " + ((int)(ratio * 100)).ToString());

            mDestinyFile.Flush();

            mScoreFile.WriteLine(str2);
            mScoreFile.Flush();
        }
    }
}

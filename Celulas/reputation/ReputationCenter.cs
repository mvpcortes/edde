﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace ReputationSystem
{
    /// <summary>
    /// Componente responsável por armazenar a reputação de outros agentes.
    /// Usando reputaçãcentralizada
    /// Ele é um SoftComponent para que participe do sistema de injeção
    /// </summary>
    public class ReputationCenter/*: IComparer<IReputable>*/
    {
        public class ComparableReputable<T> : IComparer<T> where T : IReputable
        {
            private bool m_bCrescenteOrder = false;

            public void setCrescenteSortOrder()
            {
                m_bCrescenteOrder = true;
            }

            public void setDecrescenteSortOrder()
            {
                m_bCrescenteOrder = false;
            }

            private int minRelativeValue()
            {
                return m_bCrescenteOrder ? -1 : +1;
            }

            private int maxRelativeValue()
            {
                return m_bCrescenteOrder ? -1 : +1;
            }


            public ComparableReputable(bool crescenteOrder)
            {
                m_bCrescenteOrder = crescenteOrder;
            }

            #region IComparer<T> Members

            public int Compare(T x, T y)
            {
                if (ReferenceEquals(x, null))
                {
                    if (ReferenceEquals(y, null))
                        return 0;
                    else
                        return minRelativeValue();
                }
                else
                {
                    if (ReferenceEquals(y, null))
                        return maxRelativeValue();
                }

                float reputationA, reputationB;
                ReputationCenter.Instance.getReputation(x, out reputationA);
                ReputationCenter.Instance.getReputation(y, out reputationB);
                if (reputationA < reputationB)
                    return minRelativeValue();
                else if (reputationA > reputationB)
                    return maxRelativeValue();
                else
                    return 0;
            }

            #endregion
        }
        /// <summary>
        /// Instância do singletons
        /// </summary>
        private static ReputationCenter mInstance = new ReputationCenter();

        /// <summary>
        /// Valores de multiplicativo da função de rateio (valor prévio)
        /// </summary>
        private const float PREV_VALUE_RATE = 0.3f;

        /// <summary>
        /// Valores de multiplicativo da função de rateio (valor prévio)
        /// </summary>
        private const float NEW_VALUE_RATE = 0.7f;
        
        /// <summary>
        /// Dicionário que associa um 
        /// </summary>
        private Dictionary<IReputable, float> mDicAgentReputation = new Dictionary<IReputable, float>();

        /// <summary>
        /// Construtor
        /// </summary>
        public ReputationCenter()
        {
        }

        /// <summary>
        /// Retorna a intância do singletons
        /// </summary>
        public static ReputationCenter Instance
        {
            get
            {
                return mInstance;
            }
        }

        #region container methods
        /// <summary>
        /// Adiciona uma reputação a um agente em um estado
        /// </summary>
        /// <param name="go">O agente a receber a reputação</param>
        /// <param name="state">O estado do agente</param>
        /// <param name="reputation">A reputação</param>
        public void addReputation(IReputable go, float reputation)
        {
            if (ReferenceEquals(go, null))
                return;

            float prevComp = 0;
            if(mDicAgentReputation.TryGetValue(go, out prevComp))
            {
                mDicAgentReputation[go] = PREV_VALUE_RATE*prevComp + NEW_VALUE_RATE*reputation;
            }else
            {
                mDicAgentReputation[go] = reputation;
            }
           
        }

        /// <summary>
        /// Retorna a reputação de um Agente
        /// </summary>
        /// <param name="go">O agente a receber a reputação</param>
        /// <returns>O valor da reputação</returns>
        public bool getReputation(IReputable go, out float reputation)
        {
            if ((mDicAgentReputation.TryGetValue(go, out reputation)))
                return true;
            
            return false;
        }

        ///// <summary>
        ///// Busca dentro de um container de Agentes qual o agente com maior reputação
        ///// </summary>
        ///// <param name="objects">Um vetor de agentes</param>
        ///// <param name="states">Um vetor de States. Caso nenhum agente exista dentro da base de dados, ele usa o estado deles para determinar o com maior reputação</param>
        ///// <returns>Um enumarator apontando pro item selecionado</returns>
        //public IEnumerator<IReputable> getTheHighReputation(ICollection<IReputable> coll)
        //{
        //    float maxReputation = float.MinValue;
        //    IEnumerator<IReputable> enumMax = null;
        //    IEnumerator<IReputable> enumIt = coll.GetEnumerator();
        //    while (enumIt.MoveNext())
        //    {
        //        float reputation = float.MinValue;
        //        if (getReputation(enumIt.Current, out reputation))
        //        {
        //            if (maxReputation < reputation)
        //            {
        //                enumMax = enumIt;
        //            }
        //        }
        //        else if (getReputation(enumIt.Current.state, out reputation))
        //        {
        //            if (maxReputation < reputation)
        //            {
        //                enumMax = enumIt;
        //            }
        //        }
        //    }

        //    return enumMax;
        //}

        #endregion

        //public bool m_bCrescenteOrder = false;
        
        //public void setCrescenteSortOrder()
        //{
        //    m_bCrescenteOrder = true;
        //}

        //public void setDecrescenteSortOrder()
        //{
        //    m_bCrescenteOrder = false ;
        //}

        //private int minRelativeValue()
        //{
        //    return m_bCrescenteOrder ? -1 : +1;
        //}

        //private int maxRelativeValue()
        //{
        //    return m_bCrescenteOrder ? -1 : +1;
        //}

        //#region IComparer<IReputable> Members
        //public int Compare(IReputable x, IReputable y)
        //{
        //    if (ReferenceEquals(x, null))
        //    {
        //        if (ReferenceEquals(y, null))
        //            return 0;
        //        else
        //            return minRelativeValue();
        //    }
        //    else
        //    {
        //        if (ReferenceEquals(y, null))
        //            return maxRelativeValue();
        //    }

        //    float reputationA, reputationB;
        //    getReputation(x, out reputationA);
        //    getReputation(y, out reputationB);
        //    if (reputationA < reputationB)
        //        return minRelativeValue();
        //    else if (reputationA > reputationB)
        //        return maxRelativeValue();
        //    else
        //        return 0;

        //}
        //#endregion 

        /// <summary>
        /// número de elementos reputados
        /// </summary>
        /// <returns></returns>
        public int memoryCount() { return mDicAgentReputation.Count; }

        internal void removeAgent(IReputable parent)
        {
            mDicAgentReputation.Remove(parent);
        }
    }
}

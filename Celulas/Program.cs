using System;

namespace Celulas
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (CelulasGame game = new CelulasGame())
            {
                game.Run();
            }
        }
    }
}


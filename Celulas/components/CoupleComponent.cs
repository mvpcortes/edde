﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using Microsoft.Xna.Framework;
using edde.maps;
using edde.physicalmanager;
using System.Diagnostics;
using Celulas.reputation;

namespace Celulas.components
{
    class CoupleComponent: SoftComponent
    {
        private bool mEnable = false;

        private Celula mParent;

        private Celula mOtherTouch = null;

        private TimeSpan mLastTimeTouch = TimeSpan.Zero;

       // public readonly TimeSpan DELAY_COUPLE = TimeSpan.FromSeconds(1f);

        /// <summary>
        /// número de filhos que podem ser feitos em uma cópula
        /// </summary>
        private int mChildMakeCount = 1;

        /// <summary>
        /// Quantos filhos foram feitos até o momento durante uma cópula
        /// </summary>
        private int mChildHasMade = 0;

        #region properties
        public Celula parent
        {
            get{return mParent;}
        }

        public Celula otherTouch
        {
            get{return mOtherTouch;}
            set{mOtherTouch = value;}
        }

        ///// <summary>
        ///// !Retorna o deltaTempo para gerar um filho
        ///// </summary>
        //public TimeSpan timeForBornChild
        //{
        //    get{
        //        if (mChildMakeCount > 0)
        //        {
        //            return TimeSpan.FromSeconds(DELAY_COUPLE.TotalSeconds / (float)mChildMakeCount);
        //        }
        //        else
        //        {
        //            return TimeSpan.FromSeconds(DELAY_COUPLE.TotalSeconds*2.0);
        //        }

        //    }
        //}

        /// <summary>
        /// Retorna o número de filhos a serem gerados em um acasalamento
        /// </summary>
        public int childMakeCount
        {
            get{return mChildMakeCount;}
            set{mChildMakeCount = value;}
        }

        /// <summary>
        /// Retorna quantos filhos feitos até agora durante esta cópula
        /// </summary>
        public int childHasMade
        {
            get { return mChildHasMade; }
            set { 
                mChildHasMade = Math.Max(0, Math.Min(value, childMakeCount-1));
            }
        }

        public void incChildHasMade() { mChildHasMade++; }
        #endregion


        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
        {
            return;
        }

        public void update(Microsoft.Xna.Framework.GameTime time)
        {
            if(enable)
            {
                if (!ReferenceEquals(otherTouch, null))
                {
                    if (otherTouch.canGetGene(parent))
                    {
                        //cria filho em algum lugar
                        int childCount = (int)Math.Round((otherTouch.gene.reprodution + parent.gene.reprodution + 0.1)/2f) +1;
                       int performance = createChilds(childCount);
                       //performance = otherTouch.gene.getOptimalValue();
                       ReputationSystem.ReputationCenter.Instance.addReputation(otherTouch, performance);
                    }
                    else
                    {
                        //mLastTimeTouch = time.TotalGameTime - DELAY_COUPLE;
                    }
                    enable = false;

                }
                //if(!ReferenceEquals(otherTouch, null))
                //{
                //    if (mLastTimeTouch == TimeSpan.Zero)
                //    {
                //        mLastTimeTouch = time.TotalGameTime;
                //    }
                //    else
                //    {
                //        if (
                //            (time.TotalGameTime - mLastTimeTouch > DELAY_COUPLE)
                //            ||
                //            (childHasMade >= childMakeCount)
                //           )
                //        {
                //            if (!ReferenceEquals(otherTouch, null))
                //            {
                //                int performance = childHasMade;
                //                ReputationSystem.ReputationCenter.Instance.addReputation(otherTouch, (float)performance);
                //                childHasMade = 0;
                //                this.enable = false;
                //            }
                //        }

                //        if ( 
                //            (time.TotalGameTime - mLastTimeTouch > TimeSpan.FromSeconds(timeForBornChild.TotalSeconds * childHasMade))
                //           )
                //        {
                //            if (otherTouch.canGetGene(parent))
                //            {
                //                //cria filho em algum lugar
                //                if (createChilds(1) > 0)
                //                    incChildHasMade();
                //            }
                //            else
                //            {
                //                enable = false;
                //                mLastTimeTouch = time.TotalGameTime - DELAY_COUPLE;
                //            }
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Insere filhos no mapa
        /// </summary>
        /// <param name="qtd">a quantidade desejada de filhos</param>
        /// <returns>a quantidade real de filhos inseridos</returns>
        private int createChilds( int qtd)
        {
            Map map = RPG.me.sceneManager.topScene as Map;
            if (ReferenceEquals(map, null)) return 0;
            IPhysicalManager phisicalManager = map.collisionManager;

            //tenta criar um filho em volta do casal.
            Celula couple = this.otherTouch;
            RectFloat rect = new RectFloat(couple.getRectBase(), parent.getRectBase());
            float originalRange = 0.7f * (float)Math.Sqrt(rect.width * rect.width + rect.height * rect.height);//é 0.6 para almentar um pouco o range
            float range = originalRange;
            Vector2 posCenter = rect.getCenter();
            Vector2 posRotate = MyMath.VEC_UP;
            int incRotate = 1; //só pode rotacionar 12 vezes, senão deve aumentar o raio
            LinkedList<ICollised> listCollision = new LinkedList<ICollised>();
            LinkedList<ICollised> listTouch     = new LinkedList<ICollised>();
            LinkedList<ICollised> listInsert    = new LinkedList<ICollised>();

            for (int qtdInserida = 0; qtdInserida < qtd; qtdInserida++)
            {
                bool posicionou = false;
                GeneCelula gene = new GeneCelula(parent.gene, couple.gene);

                Celula celula = new Celula(string.Format("Cell {0}", CelulasGame.cellCreateCount), gene);
                do
                {
                    celula.position = posCenter + posRotate*range;
                    listCollision.Clear();
                    listTouch.Clear();
                    phisicalManager.testColision(celula, listCollision, listTouch);
                    //GameObject goCollised = null;

                    foreach (ICollised collised in listInsert)
                    {
                        if (celula.intersects(collised.getRectBase()))
                            listCollision.AddLast(collised);
                    }

                    ////acha alguma colisão
                    //foreach (ICollised collised in listCollision)
                    //{
                    //    goCollised = collised as GameObject;
                    //    if (!ReferenceEquals(goCollised, null))
                    //        break;
                    //}

                    if (listCollision.Count == 0)
                        posicionou = true;
                    else
                    {
                        if (incRotate > 200)
                        {
                            break;
                        }
                        if (incRotate % 12 == 0)
                            range += originalRange;//incrementa o raio;
                        else
                            posRotate = MyMath.rotate(posRotate, (float)Math.PI / 6f);

                        incRotate++;
                    }
                } while (!posicionou);

                if (!posicionou) return qtdInserida;

                RPG.me.sceneManager.insertSafeGameObject(celula);
                listInsert.AddLast(celula);
            }
           return qtd;
        }

        public bool init(GameObject myParent)
        {
            mParent = myParent as Celula;
            return !ReferenceEquals(mParent, null);
        }

        public void ejected()
        {
        }

        public bool ejectMe()
        {
            return false;
        }

        public bool enable
        {
            get { return mEnable; }
            set 
            {
                mEnable = value;
                if (value == false)
                {
                    mLastTimeTouch = TimeSpan.Zero;
                }
            }
        }

        #endregion
    }
}
